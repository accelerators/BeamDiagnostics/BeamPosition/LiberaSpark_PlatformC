/*
 * Copyright (c) 2014 Instrumentation Technologies
 * All Rights Reserved.
 *
 * $Id: libera_device_class.cpp 20559 2014-06-02 07:57:53Z tomaz.beltram $
 */

#include <istd/log.h>

#include "libera_attr.h"

#include "libera_device_server.h"

template<mci::NodeValType_e NodeValType>
LiberaAttr<NodeValType>::LiberaAttr(const LiberaMapping& a_mapping) :
        Tango::Attr(a_mapping.TangoName(),
        NodeValTraits<NodeValType>::TTypeEnumVal,
        a_mapping.TangoAccess()),
        m_mapping(a_mapping)
{
    istd_FTRC();
    istd_TRC(istd::eTrcMed, "Created Attr: "
        << a_mapping.IregName() << ", " << a_mapping.TangoName()
        << ", " << a_mapping.TangoAccess() << ", " << NodeValType
        << ", " << NodeValTraits<NodeValType>::TTypeEnumVal);
}

template<mci::NodeValType_e NodeValType>
void LiberaAttr<NodeValType>::read(Tango::DeviceImpl *a_dev,
    Tango::Attribute &a_attr)
{
    istd_FTRC();

    LiberaDeviceServer* dev = dynamic_cast<LiberaDeviceServer *>(a_dev);
    std::string path = m_mapping.FullName(dev->GetInstanceName());

    try {
        mci::Node &root = dev->GetRoot(m_mapping.IsPlatform());
        root.GetNode(mci::Tokenize(path)).Get(m_lval);
    } catch (istd::Exception& ie) {
        Tango::Except::throw_exception(
            "istd::Exception", ie.what(), __PRETTY_FUNCTION__);
    }

    FromLiberaType();
    a_attr.set_value(&m_tval);
    istd_TRC(istd::eTrcDetail, "read node: " << path
        << "\n  lval: " << m_lval << "\n  tval: " << m_tval);
}

template<mci::NodeValType_e NodeValType>
void LiberaAttr<NodeValType>::write(Tango::DeviceImpl *a_dev,
    Tango::WAttribute &a_attr)
{
    istd_FTRC();

    LiberaDeviceServer* dev = dynamic_cast<LiberaDeviceServer *>(a_dev);
    std::string path = m_mapping.FullName(dev->GetInstanceName());

    a_attr.get_write_value(m_tval);

    ToLiberaType();
    try {
        mci::Node &root = dev->GetRoot(m_mapping.IsPlatform());
        root.GetNode(mci::Tokenize(path)).Set(m_lval);
    } catch (istd::Exception& ie) {
        Tango::Except::throw_exception(
            "istd::Exception", ie.what(), __PRETTY_FUNCTION__);
    }
    istd_TRC(istd::eTrcDetail, "write node: " << path
        << "\n  tval: " << m_tval << "\n  lval: " << m_lval);
}

template<mci::NodeValType_e NodeValType>
void LiberaAttr<NodeValType>::writeDefaultValue(Tango::DeviceImpl *a_dev)
{
    istd_FTRC();

    if( m_mapping.HasDefaultValue() ) {

        LiberaDeviceServer *dev = dynamic_cast<LiberaDeviceServer *>(a_dev);
        std::string path = m_mapping.FullName(dev->GetInstanceName());

        std::string d;
        m_mapping.GetDefaultValue(d);
        long v = std::stol(d);
        m_lval = v;

        try {
            mci::Node &root = dev->GetRoot(m_mapping.IsPlatform());
            root.GetNode(mci::Tokenize(path)).Set(m_lval);
        } catch (istd::Exception &ie) {
            Tango::Except::throw_exception(
                    "istd::Exception", ie.what(), __PRETTY_FUNCTION__);
        }
        istd_TRC(istd::eTrcDetail, "write default node: " << path << " lval: " << m_lval);

    }
}

template<mci::NodeValType_e NodeValType>
bool LiberaAttr<NodeValType>::is_allowed(Tango::DeviceImpl *a_dev,
    Tango::AttReqType a_type)
{
    istd_FTRC();

    LiberaDeviceServer* dev = dynamic_cast<LiberaDeviceServer *>(a_dev);
    std::string path = m_mapping.FullName(dev->GetInstanceName());

    bool isValid = false;
    try {
        mci::Node &root = dev->GetRoot(m_mapping.IsPlatform());
        root.GetNode(mci::Tokenize(path)).GetValueType();
        isValid = true;
    } catch (istd::Exception& ie) {
        istd_TRC(istd::eTrcMed, "Node is not allowed: " << path);
    }
    return isValid;
}

template<>
void LiberaAttr<mci::eNvString>::FromLiberaType()
{
    // Cast away constness and hope Tango will not abuse it.
    m_tval = const_cast<NodeValTraits<mci::eNvString>::TType>(m_lval.c_str());
}

/**
 * Explicit template instantiations for supported classes so these can be
 * used in other compilation units.
 */
template class LiberaAttr<mci::eNvBool>;
template class LiberaAttr<mci::eNvLong>;
template class LiberaAttr<mci::eNvULong>;
template class LiberaAttr<mci::eNvLongLong>;
template class LiberaAttr<mci::eNvULongLong>;
template class LiberaAttr<mci::eNvDouble>;
template class LiberaAttr<mci::eNvFloat>;
template class LiberaAttr<mci::eNvString>;
template class LiberaAttr<mci::eNvEnumeration>;
