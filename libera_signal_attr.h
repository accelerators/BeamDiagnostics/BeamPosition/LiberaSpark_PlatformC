/*
 * Copyright (c) 2014 Instrumentation Technologies
 * All Rights Reserved.
 *
 * $Id: libera_signal_attr.h 22552 2015-10-27 14:54:34Z luka.rahne $
 */

#pragma once

#include <istd/log.h>
#include <mci/mci.h>

#include "libera_mapping.h"
#include <type_traits>

template<isig::AtomType_e SignalAtomType,typename Base = Tango::SpectrumAttr>
class LiberaSignalAttr : public Base
{
public:
   template<typename B=Base>
   LiberaSignalAttr(const LiberaMapping& a_mapping,
           typename std::enable_if<std::is_same<B,Tango::SpectrumAttr>::value, Base>::type * dummy = 0 ) :
        Base(a_mapping.TangoName(),
        SignalAtomTraits<SignalAtomType>::TTypeEnumVal,
        a_mapping.TangoAccess(),4194304),
        m_mapping(a_mapping)
    {
        istd_FTRC();
        istd_TRC(istd::eTrcMed, "Created SpectrumAttr for signal: "
            << m_mapping.IregName() << ", " << SignalAtomType
            << ", " << m_mapping.TangoName()
            << ", " << SignalAtomTraits<SignalAtomType>::TTypeEnumVal
            << ", " << a_mapping.TangoAccess()
            << ", " << m_mapping.GetSize());
   }
   template<typename B=Base>
   LiberaSignalAttr(const LiberaMapping& a_mapping,
           typename std::enable_if<std::is_same<B,Tango::Attr>::value, Base>::type * dummy = 0 ) :
        Base(a_mapping.TangoName(),
        SignalAtomTraits<SignalAtomType>::TTypeEnumVal,
        a_mapping.TangoAccess()),
        m_mapping(a_mapping)
    {
        istd_FTRC();
        istd_TRC(istd::eTrcMed, "Created Scalar for signal: "
            << m_mapping.IregName() << ", " << SignalAtomType
            << ", " << m_mapping.TangoName()
            << ", " << SignalAtomTraits<SignalAtomType>::TTypeEnumVal
            << ", " << a_mapping.TangoAccess()
            << ", " << m_mapping.GetSize());
    }

private:
    typedef typename SignalAtomTraits<SignalAtomType>::LType LiberaType;
    typedef typename SignalAtomTraits<SignalAtomType>::TType TangoType;

    /**
     * Inherited methods implemented for Libera node access.
     */
    virtual void read(Tango::DeviceImpl *a_dev, Tango::Attribute &a_attr);
    virtual void write(Tango::DeviceImpl *a_dev, Tango::WAttribute &a_attr);
    virtual bool is_allowed(Tango::DeviceImpl *a_dev, Tango::AttReqType a_type);

    LiberaMapping     m_mapping;
};
