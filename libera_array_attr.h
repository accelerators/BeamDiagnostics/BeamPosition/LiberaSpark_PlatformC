/*
 * Copyright (c) 2014 Instrumentation Technologies
 * All Rights Reserved.
 *
 * $Id: libera_array_attr.h 20854 2014-07-30 13:38:14Z tomaz.beltram $
 */

#pragma once

#include <mci/mci.h>

#include "libera_mapping.h"

template<mci::NodeValType_e NodeValType>
class LiberaArrayAttr : public Tango::SpectrumAttr
{
public:
    LiberaArrayAttr(const LiberaMapping& a_mapping);

private:
    typedef typename NodeValTraits<NodeValType>::LType LiberaType;
    typedef typename NodeValTraits<NodeValType>::TCType TangoCType;
    typedef typename NodeValTraits<NodeValType>::TType TangoType;
    typedef typename std::vector<LiberaType> LiberaTypeVec;
    typedef typename std_vector<TangoType>::Type TangoTypeVec;

    /**
     * Inherited methods implemented for Libera node access.
     */
    virtual void read(Tango::DeviceImpl *a_dev, Tango::Attribute &a_attr);
    virtual void write(Tango::DeviceImpl *a_dev, Tango::WAttribute &a_attr);
    virtual bool is_allowed(Tango::DeviceImpl *a_dev, Tango::AttReqType a_type);

    /**
     * Copy and conversion methods.
     */
    void FromLiberaType();
    void ToLiberaType(const TangoCType * a_tval);

    LiberaMapping     m_mapping;
    LiberaTypeVec     m_lval;
    TangoTypeVec      m_tval;
};

// Disable redundant declaration warning for template specialization
// caused by gcc bug fixed only in 4.7.0 and above.
#pragma GCC diagnostic ignored "-Wredundant-decls"

/**
 * Specialized template class function for std::string to char* conversion.
 */
template<>
void LiberaArrayAttr<mci::eNvString>::FromLiberaType();
/*
template<>
void LiberaArrayAttr<mci::eNvString>::ToLiberaType(
    std::vector<NodeValTraits<mci::eNvString>::LType>& a_dst,
    const NodeValTraits<mci::eNvString>::TType*&  a_src);
*/
#pragma GCC diagnostic warning "-Wredundant-decls"
