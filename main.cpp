/*
 * Copyright (c) 2014 Instrumentation Technologies
 * All Rights Reserved.
 *
 * $Id: main.cpp 20974 2014-08-21 12:13:29Z tomaz.beltram $
 */

#include <istd/log.h>
#include <mci/mci.h>
#include <istd/enum_cast.h>

#include "libera_device_class.h"

/**
 * Base device server class method used as a callback to instanitate
 * device class instance.
 */
void Tango::DServer::class_factory()
{
    add_class(LiberaDeviceClass::Instance());
}

/**
 * Main function for initializing Tango framework.
 */
int main(int argc, char* argv[])
{

    if(argc==1) {
      cout << "Use [--debug level] to set up debug level 0=off,1=Low,2=Med,3=High,4=Detail" << endl;
    }

    if (argc >= 3) {
        if (std::string("--debug") == argv[argc-2]) {
            // Output to stdout
            cout << "Debug level:" << atoi(argv[argc-1]) << endl;
            istd::TraceInit("", "/var/opt/libera/log/");
            istd::TraceStart(istd::EnumCast<istd::TraceLevel_e>(atoi(argv[argc-1])));
            argc -= 2;
        }
    }
    istd::Log::Init("/var/opt/libera/log/libera-ds.log");
    
    // need to initialize CORBA from mci before Tango does it
    mci::Init(argc, argv);

    Tango::Util *tg = NULL;
    try
    {
        // Initialise the device server
        //----------------------------------------
        tg = Tango::Util::init(argc,argv);

        // Create the device server singleton
        //	which will create everything
        //----------------------------------------
        tg->server_init(false);

        // Run the endless loop
        //----------------------------------------
        cout << "Ready to accept request" << endl;
        tg->server_run();
    }
    catch (bad_alloc)
    {
        cout << "Can't allocate memory to store device object !!!" << endl;
        cout << "Exiting" << endl;
    }
    catch (CORBA::Exception &e)
    {
        Tango::Except::print_exception(e);

        cout << "Received a CORBA_Exception" << endl;
        cout << "Exiting" << endl;
    }
    if (tg!=NULL)
        tg->server_cleanup();

    return 0;
}
