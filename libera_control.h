/*
 * Copyright (c) 2014 Instrumentation Technologies
 * All Rights Reserved.
 *
 * $Id: libera_device_class.cpp 20559 2014-06-02 07:57:53Z tomaz.beltram $
 */

#pragma once

#include <mci/mci.h>

#include "libera_mapping.h"

template<Control_e ControlType>
class LiberaControlAttr : public Tango::Attr
{
public:
    LiberaControlAttr(const LiberaMapping& a_mapping);
    virtual void writeDefaultValue(Tango::DeviceImpl *a_dev);

private:
    /**
     * Inherited methods implemented for Libera node control.
     */
    virtual void read(Tango::DeviceImpl *a_dev, Tango::Attribute &a_attr);
    virtual void write(Tango::DeviceImpl *a_dev, Tango::WAttribute &a_attr);
    virtual bool is_allowed(Tango::DeviceImpl *a_dev, Tango::AttReqType a_type);

    LiberaMapping     m_mapping;
};
