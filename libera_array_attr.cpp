/*
 * Copyright (c) 2014 Instrumentation Technologies
 * All Rights Reserved.
 *
 * $Id: libera_array_attr.cpp 20854 2014-07-30 13:38:14Z tomaz.beltram $
 */

#include <istd/log.h>

#include "libera_array_attr.h"

#include "libera_device_server.h"

template<mci::NodeValType_e NodeValType>
LiberaArrayAttr<NodeValType>::LiberaArrayAttr(const LiberaMapping& a_mapping) :
    Tango::SpectrumAttr(a_mapping.TangoName(),
        NodeValTraits<NodeValType>::TTypeEnumVal,
        a_mapping.TangoAccess(), a_mapping.GetSize()),
    m_mapping(a_mapping), m_lval(a_mapping.GetSize()),
    m_tval(a_mapping.GetSize())
{
    istd_FTRC();
    istd_TRC(istd::eTrcMed, "Created SpectrumAttr: "
        << m_mapping.IregName() << ", " << NodeValType
        << ", " << m_mapping.TangoName()
        << ", " << NodeValTraits<NodeValType>::TTypeEnumVal
        << ", " << a_mapping.TangoAccess()
        << ", " << m_mapping.GetSize());
}

template<mci::NodeValType_e NodeValType>
void LiberaArrayAttr<NodeValType>::read(Tango::DeviceImpl *a_dev,
    Tango::Attribute &a_attr)
{
    istd_FTRC();

    LiberaDeviceServer* dev = dynamic_cast<LiberaDeviceServer *>(a_dev);
    std::string path = m_mapping.FullName(dev->GetInstanceName());

    try {
        mci::Node &root = dev->GetRoot(m_mapping.IsPlatform());
        root.GetNode(mci::Tokenize(path)).Get(m_lval);
    } catch (istd::Exception& ie) {
        Tango::Except::throw_exception(
            "istd::Exception", ie.what(), __PRETTY_FUNCTION__);
    }

    istd_TRC(istd::eTrcDetail, "read node: " << path);
    FromLiberaType();
    a_attr.set_value(reinterpret_cast<TangoType*>(&m_tval[0]),
        m_mapping.GetSize(), 0, false);
}

template<mci::NodeValType_e NodeValType>
void LiberaArrayAttr<NodeValType>::write(Tango::DeviceImpl *a_dev,
    Tango::WAttribute &a_attr)
{
    istd_FTRC();

    LiberaDeviceServer* dev = dynamic_cast<LiberaDeviceServer *>(a_dev);
    std::string path = m_mapping.FullName(dev->GetInstanceName());

    size_t len = a_attr.get_w_dim_x();
    if (len != m_mapping.GetSize()) {
        Tango::Except::throw_exception(
                "istd::Exception", "wrong attribute X dimension",
                __PRETTY_FUNCTION__);
    }

    const TangoCType *tval;
    a_attr.get_write_value(tval);

    ToLiberaType(tval);
    try {
        mci::Node &root = dev->GetRoot(m_mapping.IsPlatform());
        root.GetNode(mci::Tokenize(path)).Set(m_lval);
    } catch (istd::Exception& ie) {
        Tango::Except::throw_exception(
            "istd::Exception", ie.what(), __PRETTY_FUNCTION__);
    }
    istd_TRC(istd::eTrcDetail, "write node: " << path
        << "\n  tval: " << m_tval[0] << "\n  lval: " << m_lval[0]);
}

template<mci::NodeValType_e NodeValType>
bool LiberaArrayAttr<NodeValType>::is_allowed(Tango::DeviceImpl *a_dev,
    Tango::AttReqType a_type)
{
    istd_FTRC();

    LiberaDeviceServer* dev = dynamic_cast<LiberaDeviceServer *>(a_dev);
    std::string path = m_mapping.FullName(dev->GetInstanceName());

    bool isValid = false;
    try {
        mci::Node &root = dev->GetRoot(m_mapping.IsPlatform());
        root.GetNode(mci::Tokenize(path)).GetValueType();
        isValid = true;
    } catch (istd::Exception& ie) {
        istd_TRC(istd::eTrcMed, "Node is not allowed: " << path);
    }
    return isValid;
}


template<mci::NodeValType_e NodeValType>
void LiberaArrayAttr<NodeValType>::FromLiberaType()
{
    for (size_t i(0); i < m_mapping.GetSize(); ++i) {
        m_tval[i] = m_lval[i];
        istd_TRC(istd::eTrcDetail, "  " << m_lval[i] << ", " << m_tval[i]);
    }
}

template<mci::NodeValType_e NodeValType>
void LiberaArrayAttr<NodeValType>::ToLiberaType(const TangoCType * a_tval)
{
    for (size_t i(0); i < m_mapping.GetSize(); ++i) {
        m_lval[i] = a_tval[i];
    }
}

template<>
void LiberaArrayAttr<mci::eNvString>::FromLiberaType()
{
    // Cast away constness and hope Tango will not abuse it.
    for (size_t i(0); i < m_mapping.GetSize(); ++i) {
        m_tval[i] = const_cast<NodeValTraits<mci::eNvString>::TType>(m_lval[i].c_str());
    }
}
/*
template<>
void LiberaArrayAttr<mci::eNvString>::ToLiberaType(
    std::vector<NodeValTraits<mci::eNvString>::LType>& a_dst,
    const NodeValTraits<NodeValType>::TCType*&  a_src)
{
    for (size_t i(0); i < m_mapping.GetSize(); ++i) {
        a_dst[i] = a_src[i];
    }
}
*/

/**
 * Explicit template instantiations for supported classes so these can be
 * used in other compilation units.
 */

template class LiberaArrayAttr<mci::eNvBool>;
template class LiberaArrayAttr<mci::eNvLong>;
template class LiberaArrayAttr<mci::eNvULong>;
template class LiberaArrayAttr<mci::eNvLongLong>;
template class LiberaArrayAttr<mci::eNvULongLong>;
template class LiberaArrayAttr<mci::eNvDouble>;
template class LiberaArrayAttr<mci::eNvFloat>;
template class LiberaArrayAttr<mci::eNvString>;
template class LiberaArrayAttr<mci::eNvEnumeration>;
