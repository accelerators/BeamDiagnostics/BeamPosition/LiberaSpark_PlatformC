/*
 * Copyright (c) 2014 Instrumentation Technologies
 * All Rights Reserved.
 *
 * $Id: libera_config.h 20729 2014-06-26 12:00:56Z tomaz.beltram $
 */

#pragma once

#include <vector>

#include "libera_mapping.h"

class  LiberaConfig : public std::vector<LiberaMapping> {
public:

    LiberaConfig();
    void Init();
    bool HasBpm();
    vector<std::string> GetBPMNodes();
    bool hasZyncTemp;
    bool hasAGC;
    bool hasSAIncoherencyDrift;

private:

    void ReadTopFile(const std::string& a_fname, const bool a_platform);
    void ReadFile(const std::string& a_fname,
        const std::string& a_ireg_prefix, const std::string& a_tango_prefix,
        const bool a_platform, const bool a_instance);
    void ReadListenFile(const std::string& a_fname, const bool a_platform);
    void ReadBpmFile(const std::string& a_fname);
    void ReadFeaturesFile(const std::string& a_fname);

    vector<std::string> bpmNodes;
    static const std::string m_cfg_dir_name;
    static const std::string m_cfg_file_name;
    static const std::string m_cfg_pm_name;
    static const std::string m_cfg_bpm_ireg_name;
    static const std::string m_cfg_features_name;

};
