/*
 * Copyright (c) 2014 Instrumentation Technologies
 * All Rights Reserved.
 *
 * $Id: libera_signal_client.cpp 21366 2015-01-13 08:17:23Z tomaz.beltram $
 */

#include <chrono>

#include <istd/trace.h>
#include <mci/mci_util.h>

#include "libera_signal_client.h"
#include "libera_signal_client_base.h"

#define SATURATE(x,max) if( x>(size_t)max ) x=max;

time_t get_ticks() {

    static time_t tickStart = -1;
    if (tickStart < 0)
        tickStart = time(NULL);

    struct timeval tv;
    gettimeofday(&tv, NULL);
    return ((tv.tv_sec - tickStart) * 1000 + tv.tv_usec / 1000);

}

template<isig::AtomType_e SignalAtomType>
LiberaSignalClient<SignalAtomType>::LiberaSignalClient(
    Tango::DeviceImpl* a_dev, const size_t a_length, const LiberaMapping& a_lm)
    : LiberaSignalClientBase(a_lm),
      m_dev(a_dev),
      m_length(a_length),
      m_offset(0),
      m_mt(0),
      m_enabled(false),
      m_idle(true),
      m_updated(false),
      m_refreshPeriod(10),
      m_mode(isig::eModeDodOnEvent),
      m_averaging(false),
      m_averaging_length(10),
      m_signal(),
      m_running(false),
      m_thread()
{
    istd_FTRC();

    m_thread = std::thread(std::ref(*this));
    // safety check, wait that thread function has started
    while (!m_running) {
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
    };
}

template<isig::AtomType_e SignalAtomType>
LiberaSignalClient<SignalAtomType>::~LiberaSignalClient()
{
    m_running = false;
    if (m_thread.joinable()) {
        m_thread.join();
    }
}

/**
 * This is internal thread for continuous acquisition.
 */
template<isig::AtomType_e SignalAtomType>
void LiberaSignalClient<SignalAtomType>::operator()() {

  istd_FTRC();

  // thread function has started
  m_running = true;
  m_idle = true;

  while (m_running) {

    if (m_enabled) {

      m_idle = false;

      try {
        Update();
      } catch (istd::Exception &e) {
        istd_TRC(istd::eTrcLow,
                 "Exception caught updating data buffer.");
        // Try to reconnect
        m_enabled = false;
        Initialize();
        continue;
      }

      if (!m_enabled)
        continue;

      const std::map<std::string, _ATTR_T> &names = GetNames();

      for (auto i = names.begin(); i != names.end(); ++i) {

        //  send notification
        string attName = i->first;
        _ATTR_T att = i->second;

        if( att.hasEvent ) {

          try {

            if( att.mapToDouble ) {

              // Convert to double
              if (att.isSum) {

                Tango::DevDouble *sum = new Tango::DevDouble[m_tsum.size()];
                for(int j=0;j<(int)m_tsum.size();j++)
                  sum[j] = (Tango::DevDouble)m_tsum[j];
                m_dev->push_change_event(attName.c_str(), sum, m_tsum.size(), 0, true);

              } else if (att.isSumRow ) {

                Tango::DevDouble *sum = new Tango::DevDouble[m_tsumrow.size()];
                for(int j=0;j<(int)m_tsumrow.size();j++)
                  sum[j] = (Tango::DevDouble)m_tsumrow[j];
                m_dev->push_change_event(attName.c_str(), sum, m_tsumrow.size(), 0, true);

              } else {

                  Tango::DevDouble *vals = new Tango::DevDouble[m_tvals[att.column].size()];
                  for(int j=0;j<(int)m_tvals[att.column].size();j++)
                    vals[j] = (Tango::DevDouble)m_tvals[att.column][j];
                  m_dev->push_change_event(attName.c_str(),vals,m_tvals[att.column].size(),0,true);

              }


            } else {

              // Native mapping
              if (att.isSum) {

                m_dev->push_change_event(attName.c_str(), reinterpret_cast<TangoType *>(&m_tsum[0]), m_tsum.size());

              } else if (att.isSumRow) {

                m_dev->push_change_event(attName.c_str(), reinterpret_cast<TangoType *>(&m_tsumrow[0]), m_tsumrow.size());

              } else {

                m_dev->push_change_event(attName.c_str(), reinterpret_cast<TangoType *>(&m_tvals[att.column][0]), m_tvals[att.column].size());

              }

            }

            //istd_TRC(istd::eTrcHigh, "Sent event for: " << i->first
            //                                            << ", component: " << i->second);

          } catch (Tango::DevFailed &e) {
            istd_TRC(istd::eTrcHigh, "Failed to push event for: "
                    << i->first << ", " << e.errors[0].desc.in());
            break;
          }

        }

      }

      if ((m_signal->AccessType() == isig::eAccessDataOnDemand) &&
          (m_mode != isig::eModeDodOnEvent)) {
        // avoid busy loop for OnEvent DoD
        std::this_thread::sleep_for(std::chrono::milliseconds(m_refreshPeriod));
      }

    } else {

      m_idle = true;
      std::this_thread::sleep_for(std::chrono::milliseconds(100));

    }

  }
  m_idle = true;
  istd_TRC(istd::eTrcHigh, "Exit update thread for.");
}

template<isig::AtomType_e SignalAtomType>
void LiberaSignalClient<SignalAtomType>::Create(mci::Node &a_node,uint32_t refresh_period)
{
    istd_FTRC();
    // create signal just once when device server starts
    m_refreshPeriod = refresh_period;
    m_signal = mci::CreateRemoteSignal(a_node);
    istd_TRC(istd::eTrcMed, "Created signal for: " << a_node.GetName());

    m_tvals.resize(NumColumns());
    m_tsum.resize(NumColumns());
    m_tlastvals.resize(NumColumns());
    for (int i = 0; i < NumColumns(); ++i) {
        m_tvals[i].resize(m_length);
    }
    istd_TRC(istd::eTrcMed, "Created tango bufers for: " << a_node.GetName());
}

template<isig::AtomType_e SignalAtomType>
bool LiberaSignalClient<SignalAtomType>::IsEnabled()
{
    istd_FTRC();
    return m_enabled;
}

/**
 * look in isig/declatations.h for updated version of enum isig::AccessMode_e
 * eModeStream         = 0,    ///< Stream has just one access mode
 * eModeDodPosition    = 10,   ///< Data on demand: Absolute position in the input buffer in atom units
 * eModeDodNow         = 11,   ///< Data on demand: Best approximation of now
 * eModeDodLMT         = 12,   ///< Data on demand: Position of the data is defined by the specified LMT
 * eModeDodOnEvent     = 13,   ///< Data on demand: Position is defined by a series of events (for example triggers)
 * eModeDodSingleEvent = 14    ///< Data on demand: Position is defined by single next event (for example trigger)
 */
int32_t c_dodModeOffset(10); // DoD modes control attribute starts at zero.

template<isig::AtomType_e SignalAtomType>
void LiberaSignalClient<SignalAtomType>::SetMode(int32_t a_mode)
{
    istd_FTRC();
    if (!m_signal) {
        istd_LOGe("Signal doesn't exisit!");
        return;
    }
    if (!m_enabled) {
        int32_t mode = a_mode + c_dodModeOffset;
        switch (mode) {
        case isig::eModeDodPosition : // fall through
        case isig::eModeDodNow : // fall through
        case isig::eModeDodLMT : // fall through
        case isig::eModeDodSingleEvent :
        case isig::eModeDodOnEvent :
            m_mode = static_cast<isig::AccessMode_e>(mode);
            m_updated = false;
            break;
        default:
            istd_LOGe("Invalid mode for DoD signal: " << a_mode);
        }
    }
}

template<isig::AtomType_e SignalAtomType>
int32_t LiberaSignalClient<SignalAtomType>::GetMode()
{
    istd_FTRC();
    return static_cast<int32_t>(m_mode) - c_dodModeOffset;
}

template<isig::AtomType_e SignalAtomType>
void LiberaSignalClient<SignalAtomType>::OpenStream()
{
    istd_FTRC();
    cout << "LiberaSignalClient::OpenStream():" << m_signal->GetName() << endl;
    m_stream = std::dynamic_pointer_cast<RStream>(m_signal);
    if (m_streamClient && m_streamClient->IsOpen()) {
        istd_TRC(istd::eTrcHigh, "Close stream.");
        m_streamClient->Close();
    }
    m_length = m_stream->GroupSize();
    istd_TRC(istd::eTrcLow, "Adjusted client buffer to group size:"
        << m_length);
    m_streamClient = std::make_shared<StreamClient>(m_stream.get(), "stream_client");
    m_buf =  std::make_shared<ClientBuffer>(m_streamClient->CreateBuffer(m_length));
    istd_TRC(istd::eTrcHigh, "Open stream.");
    if (m_streamClient->Open() != isig::eSuccess) {
        throw istd::Exception("Failed to open stream!");
    }
}

template<isig::AtomType_e SignalAtomType>
void LiberaSignalClient<SignalAtomType>::OpenDoD()
{
    istd_FTRC();
    cout << "LiberaSignalClient::OpenDoD():" << m_signal->GetName() << endl;
    m_dod = std::dynamic_pointer_cast<RSource>(m_signal);
    if (m_dodClient && m_dodClient->IsOpen()) {
        m_dodClient->Close();
    }
    m_dodClient = std::make_shared<DodClient>(m_dod, "dod_client", m_dod->GetTraits());
    m_buf = std::make_shared<ClientBuffer>(m_dodClient->CreateBuffer(m_length));

    size_t offset_open(0);
    if ((m_mode == isig::eModeDodSingleEvent) ||
        (m_mode ==isig::eModeDodOnEvent)) {
        offset_open = m_offset;
    }

    istd_TRC(istd::eTrcHigh, "Open DoD.");
    if (m_dodClient->Open(m_mode, m_length, offset_open) != isig::eSuccess) {
        istd_TRC(istd::eTrcLow, "Error opening dod in "
            << m_mode << " mode.");
        m_enabled = false;
        throw istd::Exception("Failed to open dod!");
    }
}

template<isig::AtomType_e SignalAtomType>
int32_t LiberaSignalClient<SignalAtomType>::GetAccessType() {

  if(m_signal==NULL)
    return isig::eAccessUnknown;

  return m_signal->AccessType();

}

template<isig::AtomType_e SignalAtomType>
void LiberaSignalClient<SignalAtomType>::Initialize()
{
    istd_FTRC();
    if (!m_signal) {
        istd_LOGe("Signal doesn't exisit!");
        return;
    }
    if (!m_enabled) {
        if (m_signal->AccessType() == isig::eAccessStream) {
            OpenStream();
        } else if (m_signal->AccessType() == isig::eAccessDataOnDemand) {
            OpenDoD();
        } else {
            throw istd::Exception("Unsupported signal access mode.");
        }
        m_enabled = true;
        m_updated = false;
        istd_TRC(istd::eTrcLow, "Initialized signal: " << LM().IregName());
    }
}

template<isig::AtomType_e SignalAtomType>
void LiberaSignalClient<SignalAtomType>::Terminate()
{
    istd_FTRC();
    if (!m_signal) {
        istd_LOGe("Signal doesn't exisit!");
        return;
    }
    m_enabled = false;
    // close stream, stop notifications
    if (m_streamClient && m_streamClient->IsOpen()) {
        istd_TRC(istd::eTrcHigh, "Close stream.");
        m_streamClient->Close();
    }
    // stop updating DoD in trigger mode (freeze)
    if (m_dodClient && m_dodClient->IsOpen()) {
        istd_TRC(istd::eTrcHigh, "Close DoD.");
        m_dodClient->Close();
    }
    // Wait for end of thread
    while (!m_idle) {
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }
    m_updated = false;
    istd_TRC(istd::eTrcLow, "Terminated signal: " << LM().IregName());
}

template<isig::AtomType_e SignalAtomType>
void LiberaSignalClient<SignalAtomType>::Update()
{
    istd_FTRC();

    if (!m_signal) {
        istd_LOGe("Signal doesn't exist!");
        return;
    }

    if (m_signal->AccessType() == isig::eAccessStream) {

        // streamClient is opened in enable
        isig::SuccessCode_e res = m_streamClient->Read(*m_buf);
        if (res == isig::eSuccess) {
            Transpose();
        } else {
            // disable signal
            m_enabled = false;
            istd_LOGe("Failed to read stream, error: " << res);
        }

    } else if (m_signal->AccessType() == isig::eAccessDataOnDemand) {

        isig::SignalMeta signal_meta;
        size_t offset_read(0);
        if ((m_mode != isig::eModeDodSingleEvent) &&
            (m_mode !=isig::eModeDodOnEvent)) {
            offset_read = m_offset;
        }

        // Can be optimized using MetaBufferPtr if necessary.
        if (m_buf->GetLength() != m_length)
            m_buf->Resize(m_length);

        isig::SuccessCode_e res = isig::eSuccess;

        //time_t t0 = get_ticks();
        try {
          res = m_dodClient->Read(*m_buf, signal_meta, offset_read);
        } catch (istd::Exception &e) {
           cout << "m_dodClient->Read(" << LM().IregName() << "): Exception:" << e.what() << endl;
          throw e;
        }
        //time_t t1 = get_ticks();
        //cout << LM().IregName() << ":" << (t1-t0) << "ms" << endl;

        if (res == isig::eSuccess) {

            auto mt = signal_meta.find(isig::eMetaMT);
            if (mt != signal_meta.end()) {
                m_mt = mt->second;
            }
            else {
                m_mt = 0;
            }
            Transpose();

        } else {

            if(!m_dodClient->IsOpen()){
                m_enabled = false;
                istd_TRC(istd::eTrcLow,
                    "Signal closed while reading dod in position mode");
            } else {
                istd_LOGe("Error reading dod in position mode, error: " << res);
            }

        }

    } else {

        m_enabled = false;
        throw istd::Exception("Unsupported signal access mode.");

    }

}

template<isig::AtomType_e SignalAtomType>
bool LiberaSignalClient<SignalAtomType>::IsAveraging() {
  return m_averaging;
}

template<isig::AtomType_e SignalAtomType>
bool LiberaSignalClient<SignalAtomType>::SetAveraging(bool averaging) {
  std::lock_guard<std::mutex> l(m_data_x);
  m_averaging = averaging;
  m_averaging_number = 0;
  for(int i=0;i<NumColumns();i++)
    m_tlastvals[i].resize(m_averaging_length);
}

template<isig::AtomType_e SignalAtomType>
size_t LiberaSignalClient<SignalAtomType>::GetAveragingLength() {
  return m_averaging_length;
}

template<isig::AtomType_e SignalAtomType>
void LiberaSignalClient<SignalAtomType>::SetAveragingLength(size_t length) {
  std::lock_guard<std::mutex> l(m_data_x);
  m_averaging_length = length;
  m_averaging_number = 0;
  for(int i=0;i<NumColumns();i++)
    m_tlastvals[i].resize(m_averaging_length);
}

/**
 * Convert one atom component (column) from the buffer to spectrum attribute.
 */
template<isig::AtomType_e SignalAtomType>
void LiberaSignalClient<SignalAtomType>::Transpose() {

  istd_FTRC();
  std::lock_guard<std::mutex> l(m_data_x);

  if (m_buf->GetLength() > m_length) {
    istd_TRC(istd::eTrcMed, "Buffer size changed after reading signal from "
            << m_buf->GetLength() << " to " << m_length << ".");
    m_buf->Resize(m_length);
  }

  size_t len = m_buf->GetLength();

  if(ComputeSumRow()) {
    // Compute sum of rows (if asked)
    m_tsumrow.resize(len);
  }

  for (int col = 0; col < NumColumns(); ++col) {

    m_tvals[col].resize(len);

    // Transpose
    for (size_t i(0); i < len; ++i) {
      m_tvals[col][i] = (*m_buf)[i][col];
    }

    // Offset removal
    if( RemoveOffset() && len>20 ) {

      // Compute avg of last 20 samples and remove it
      double sumEnd = 0.0;
      for(size_t i = len-20; i<len; i++) {
        sumEnd += m_tvals[col][i];
      }
      sumEnd = sumEnd / 20.0;
      for (size_t i(0); i < len; ++i) {
        m_tvals[col][i] -= (int)(sumEnd);
      }

    }


    // Compute sum of columns (if asked)
    if (ComputeSum()) {

      size_t start = 0;
      size_t stop = len;

      if (hasStartStopIndex()) {
        start = (size_t) GetStartIndex();
        SATURATE(start, len);
        stop = (size_t) GetStopIndex();
        SATURATE(stop, len);
      }

      m_tsum[col] = 0;
      for (size_t i = start; i < stop; ++i) {
        m_tsum[col] += m_tvals[col][i];
      }

    }

  }

  if(ComputeSumRow()) {
    // Compute sum of rows (if asked)
    for (size_t i(0); i < len; ++i) {
      m_tsumrow[i] = 0;
      for (int col = 0; col < NumColumns(); ++col) {
        m_tsumrow[i] += m_tvals[col][i];
      }
    }
  }

  // Recompute position using polynomial transform (if needed)
  if( IsBPM() ) {

    if (GetCalculationAlgorithm() == POLY_ALGORITHM) {

      // Recompute position according to polynomial transform
      // Expected structure of BPM node
      // {Va,Vb,Vc,Vd,Sum,Incoherency,HPosition,VPosition}

      //time_t t0 = get_ticks();
      unsigned int maxOrder = (hOrder > vOrder) ? hOrder : vOrder;
      //cout << "#H(DoS) V(DoS) H(Poly) V(Poly)" << endl;

      for (size_t i(0); i < len; ++i) {

        double a = (double) (m_tvals[0][i]) * kA;
        double b = (double) (m_tvals[1][i]) * kB;
        double c = (double) (m_tvals[2][i]) * kC;
        double d = (double) (m_tvals[3][i]) * kD;

        double iS = 1.0 / (a + b + c + d);
        double X = (a - b - c + d) * iS;
        double Y = (a + b - c - d) * iS;
        double sum;

        double powx = 1.0;
        double powy = 1.0;
        for (size_t j(0); j < maxOrder; j++) {
          px[j] = powx;
          py[j] = powy;
          powx *= X;
          powy *= Y;
        }

        //cout << i << " " << m_tvals[6][i] << " " << m_tvals[7][i];

        sum = 0.0;
        unsigned int cf = 0;
        for (size_t x(0); x < hOrder; x++) {
          for (size_t y(0); y < hOrder - x; y++) {
            sum += px[x] * py[y] * hPolyCoefs[cf];
            cf++;
          }
        }
        m_tvals[6][i] = (TangoType) (sum / 1e9) + hOffset;

        sum = 0.0;
        cf = 0;
        for (size_t x(0); x < vOrder; x++) {
          for (size_t y(0); y < vOrder - x; y++) {
            sum += px[x] * py[y] * vPolyCoefs[cf];
            cf++;
          }
        }
        m_tvals[7][i] = (TangoType) (sum / 1e9) + vOffset;

        //cout << " " << m_tvals[6][i] << " " << m_tvals[7][i] << endl;

      }

      //time_t t1 = get_ticks();
      //cout << len << " => " << (t1-t0) << " ms" << endl;

    } else {

      // Dos
      // Convert to meters
      for (size_t i(0); i < len; ++i) {
        m_tvals[6][i] /= 1e9;
        m_tvals[7][i] /= 1e9;
      }


    }

    // Rotate
    for (size_t i(0); i < len; ++i) {
      double rx = r11 * m_tvals[6][i] + r12 * m_tvals[7][i];
      double rz = r21 * m_tvals[6][i] + r22 * m_tvals[7][i];
      m_tvals[6][i] = rx;
      m_tvals[7][i] = rz;
    }

  }

  // Compute averaging (eAccessStream only -> m_length=1)

  if( m_averaging ) {

    const std::map<std::string, _ATTR_T> &names = GetNames();

    // Update averaging buffers
    for (auto i = names.begin(); i != names.end(); ++i) {
      _ATTR_T att = i->second;
      if(att.hasAverage) {
        if(m_averaging_number >= m_averaging_length) {
          for(size_t i=1;i<m_averaging_length;i++)
            m_tlastvals[att.column][i-1] = m_tlastvals[att.column][i];
          m_tlastvals[att.column][m_averaging_number-1] = m_tvals[att.column][0];
        } else {
          m_tlastvals[att.column][m_averaging_number] = m_tvals[att.column][0];
        }
      }
    }

    // Update averaging counter
    if(m_averaging_number < m_averaging_length)
      m_averaging_number++;

    // Compute averages
    for (auto i = names.begin(); i != names.end(); ++i) {
      _ATTR_T att = i->second;
      if(att.hasAverage) {
        double sum = 0;
        for(size_t i=0;i<m_averaging_number;i++)
          sum += m_tlastvals[att.column][i];
        m_tvals[att.column][0] = (TangoType) ( sum/(double)m_averaging_number );
      }
    }

  }

  m_updated = true;

}

template<isig::AtomType_e SignalAtomType>
void LiberaSignalClient<SignalAtomType>::DoRead(int a_column, Tango::Attribute &a_attr)
{
    istd_FTRC();
    if (!m_signal) {
        istd_LOGe("Signal doesn't exist!");
        return;
    }
    if ((a_column < -2) || (a_column >= NumColumns())) {
        istd_LOGe("Wrong column number: " << a_column);
        return;
    }
    if (!m_updated) {
       istd_TRC(istd::eTrcMed, "no data for component: " << a_column
                                                         << " of signal: " << LM().IregName());
       return;
    }
    std::lock_guard<std::mutex> l(m_data_x);

    if( a_attr.get_data_type() == Tango::DEV_DOUBLE ) {

      if (a_column == -1) {
        // Sum
        Tango::DevDouble *sum = new Tango::DevDouble[m_tsum.size()];
        for(int j=0;j<(int)m_tsum.size();j++)
          sum[j] = (Tango::DevDouble)m_tsum[j];
        a_attr.set_value(sum,m_tsum.size(), 0, true);
      } if (a_column == -2) {
        // SumRow
        Tango::DevDouble *sum = new Tango::DevDouble[m_tsumrow.size()];
        for(int j=0;j<(int)m_tsumrow.size();j++)
          sum[j] = (Tango::DevDouble)m_tsumrow[j];
        a_attr.set_value(sum,m_tsumrow.size(), 0, true);
      } else {
        Tango::DevDouble *vals = new Tango::DevDouble[m_tvals[a_column].size()];
        for(int j=0;j<(int)m_tvals[a_column].size();j++)
          vals[j] = (Tango::DevDouble)m_tvals[a_column][j];
        a_attr.set_value(vals,m_tvals[a_column].size(), 0, true);
      }

    } else {

      if (a_column == -1) {
        // Sum
        a_attr.set_value(reinterpret_cast<TangoType *>(&m_tsum[0]),
                         m_tsum.size(), 0, false);
      } else if (a_column == -2) {
        // SumRow
        a_attr.set_value(reinterpret_cast<TangoType *>(&m_tsumrow[0]),
                         m_tsumrow.size(), 0, false);
      } else {

        a_attr.set_value(reinterpret_cast<TangoType *>(&m_tvals[a_column][0]),
                         m_tvals[a_column].size(), 0, false);
      }

    }

    istd_TRC(istd::eTrcHigh, "Data copied, buffer size: "
        << m_tvals[a_column].size());
}

template<isig::AtomType_e SignalAtomType>
void LiberaSignalClient<SignalAtomType>::SetOffset(int32_t a_offset)
{
    istd_FTRC();
    if (!m_enabled) {
        m_offset = a_offset;
    }
}

template<isig::AtomType_e SignalAtomType>
int32_t LiberaSignalClient<SignalAtomType>::GetOffset()
{
    istd_FTRC();
    return m_offset;
}

template<isig::AtomType_e SignalAtomType>
void LiberaSignalClient<SignalAtomType>::SetLength(size_t a_length)
{
    istd_FTRC();
    if (!m_enabled) {
        m_length = a_length;
    } else {
        Terminate();
        m_length = a_length;
        Initialize();
    }
}

template<isig::AtomType_e SignalAtomType>
size_t LiberaSignalClient<SignalAtomType>::GetLength()
{
    istd_FTRC();
    return m_length;
}

template<isig::AtomType_e SignalAtomType>
uint64_t LiberaSignalClient<SignalAtomType>::GetMT()
{
    istd_FTRC();
    return m_mt;
}

/**
 * Explicit template instantiations for supported classes so these can be
 * used in other compilation units.
 */
template class LiberaSignalClient<isig::eTypeDouble>;
template class LiberaSignalClient<isig::eTypeFloat>;
template class LiberaSignalClient<isig::eTypeUInt64>;
template class LiberaSignalClient<isig::eTypeInt64>;
template class LiberaSignalClient<isig::eTypeUInt32>;
template class LiberaSignalClient<isig::eTypeInt32>;
template class LiberaSignalClient<isig::eTypeUInt16>;
template class LiberaSignalClient<isig::eTypeInt16>;
template class LiberaSignalClient<isig::eTypeUInt8>;
template class LiberaSignalClient<isig::eTypeInt8>;
