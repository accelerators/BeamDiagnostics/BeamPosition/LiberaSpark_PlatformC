/*
 * Copyright (c) 2014 Instrumentation Technologies
 * All Rights Reserved.
 *
 * $Id: libera_device_class.cpp 20559 2014-06-02 07:57:53Z tomaz.beltram $
 */

#include <istd/log.h>

#include "libera_control.h"
#include "libera_device_server.h"

/**
 * Control attribute for signal enabling.
 */
template<>
LiberaControlAttr<eCEnable>::LiberaControlAttr(const LiberaMapping& a_mapping)
    : Tango::Attr(a_mapping.TangoName(), Tango::DEV_BOOLEAN, Tango::READ_WRITE),
      m_mapping(a_mapping)
{
    istd_FTRC();
    istd_TRC(istd::eTrcMed, "Created ControlAttr for signal enable: "
        << m_mapping.IregName() << ", " << m_mapping.NodeValType()
        << ", " << m_mapping.TangoName()
        << ", " << m_mapping.ControlType()
        << ", " << m_mapping.GetSize());
}

/**
 * Generic template class method implementation.
 */
template<Control_e ControlType>
bool LiberaControlAttr<ControlType>::is_allowed(Tango::DeviceImpl *a_dev, Tango::AttReqType a_type)
{
  istd_FTRC();
  return true;
}

/**
 * Write default value (if any)
 */
template<>
void LiberaControlAttr<eCEnable>::writeDefaultValue(Tango::DeviceImpl *a_dev)
{
    istd_FTRC();
    if(m_mapping.HasDefaultValue()) {
      try {
        string d;
        m_mapping.GetDefaultValue(d);
        Tango::DevBoolean b = d != "0";
        LiberaDeviceServer* dev = dynamic_cast<LiberaDeviceServer *>(a_dev);
        LiberaSignalClientBase* scb = dev->GetSignalClient(m_mapping.IregName());
        if (scb != NULL) {
          istd_TRC(istd::eTrcMed, "(eCEnable) Write default for " << get_name() << ":" << b );
          scb->Enable(b);
        }
      } catch (istd::Exception& ie) {
        Tango::Except::throw_exception(
                "istd::Exception", ie.what(), __PRETTY_FUNCTION__);
      }
    }

}

/**
 * Return the signal enabled state.
 */
template<>
void LiberaControlAttr<eCEnable>::read(Tango::DeviceImpl *a_dev, Tango::Attribute &a_attr)
{
    istd_FTRC();
    LiberaDeviceServer* dev = dynamic_cast<LiberaDeviceServer *>(a_dev);
    LiberaSignalClientBase* scb = dev->GetSignalClient(m_mapping.IregName());
    if (scb != NULL) {
        Tango::DevBoolean b = scb->IsEnabled();
        a_attr.set_value(&b);
    }
}

/**
 * Put the signal to enable or disabled state.
 */
template<>
void LiberaControlAttr<eCEnable>::write(Tango::DeviceImpl *a_dev, Tango::WAttribute &a_attr)
{
    istd_FTRC();
    LiberaDeviceServer* dev = dynamic_cast<LiberaDeviceServer *>(a_dev);
    std::string path = m_mapping.FullName(dev->GetInstanceName());

    try {
        LiberaSignalClientBase* scb = dev->GetSignalClient(m_mapping.IregName());
        if (scb != NULL) {
            Tango::DevBoolean val;
            a_attr.get_write_value(val);
            scb->Enable(val);
        }
    } catch (istd::Exception& ie) {
        Tango::Except::throw_exception(
            "istd::Exception", ie.what(), __PRETTY_FUNCTION__);
    }
}

/**
 * Control attribute for signal mode.
 */
template<>
LiberaControlAttr<eCMode>::LiberaControlAttr(const LiberaMapping& a_mapping)
    : Tango::Attr(a_mapping.TangoName(), Tango::DEV_LONG, Tango::READ_WRITE),
      m_mapping(a_mapping)
{
    istd_FTRC();
    istd_TRC(istd::eTrcMed, "Created ControlAttr for signal mode: "
        << m_mapping.IregName() << ", " << m_mapping.NodeValType()
        << ", " << m_mapping.TangoName()
        << ", " << m_mapping.ControlType()
        << ", " << m_mapping.GetSize());
}

/**
 * Write default value (if any)
 */
template<>
void LiberaControlAttr<eCMode>::writeDefaultValue(Tango::DeviceImpl *a_dev)
{

    istd_FTRC();
    if(m_mapping.HasDefaultValue()) {
      try {
        string d;
        m_mapping.GetDefaultValue(d);
        Tango::DevLong m = std::stol(d);
        LiberaDeviceServer* dev = dynamic_cast<LiberaDeviceServer *>(a_dev);
        LiberaSignalClientBase* scb = dev->GetSignalClient(m_mapping.IregName());
        if (scb != NULL) {
          istd_TRC(istd::eTrcMed, "(eCMode) Write default for " << get_name() << ":" << m );
          scb->SetMode(m);
        }
      } catch (istd::Exception& ie) {
        Tango::Except::throw_exception(
                "istd::Exception", ie.what(), __PRETTY_FUNCTION__);
      }
    }

}

/**
 * Return the signal mode setting.
 */
template<>
void LiberaControlAttr<eCMode>::read(Tango::DeviceImpl *a_dev, Tango::Attribute &a_attr)
{
    istd_FTRC();
    LiberaDeviceServer* dev = dynamic_cast<LiberaDeviceServer *>(a_dev);
    LiberaSignalClientBase* scb = dev->GetSignalClient(m_mapping.IregName());
    if (scb != NULL) {
        Tango::DevLong val = scb->GetMode();
        a_attr.set_value(&val);
    }
}

/**
 * Set the signal mode setting.
 */
template<>
void LiberaControlAttr<eCMode>::write(Tango::DeviceImpl *a_dev, Tango::WAttribute &a_attr)
{
    istd_FTRC();
    LiberaDeviceServer* dev = dynamic_cast<LiberaDeviceServer *>(a_dev);
    std::string path = m_mapping.FullName(dev->GetInstanceName());

    try {
        LiberaSignalClientBase* scb = dev->GetSignalClient(m_mapping.IregName());
        if (scb != NULL) {
            Tango::DevLong val;
            a_attr.get_write_value(val);
            scb->SetMode(val);
        }
    } catch (istd::Exception& ie) {
        Tango::Except::throw_exception(
            "istd::Exception", ie.what(), __PRETTY_FUNCTION__);
    }
}

/**
 * Control attribute for signal offset.
 */
template<>
LiberaControlAttr<eCOffset>::LiberaControlAttr(const LiberaMapping& a_mapping)
    : Tango::Attr(a_mapping.TangoName(), Tango::DEV_LONG, Tango::READ_WRITE),
      m_mapping(a_mapping)
{
    istd_FTRC();
    istd_TRC(istd::eTrcMed, "Created ControlAttr for signal offset: "
        << m_mapping.IregName() << ", " << m_mapping.NodeValType()
        << ", " << m_mapping.TangoName()
        << ", " << m_mapping.ControlType()
        << ", " << m_mapping.GetSize());
}

/**
 * Write default value (if any)
 */
template<>
void LiberaControlAttr<eCOffset>::writeDefaultValue(Tango::DeviceImpl *a_dev)
{

    istd_FTRC();
    if(m_mapping.HasDefaultValue()) {
      try {
        string d;
        m_mapping.GetDefaultValue(d);
        Tango::DevLong o = std::stol(d);
        LiberaDeviceServer* dev = dynamic_cast<LiberaDeviceServer *>(a_dev);
        LiberaSignalClientBase* scb = dev->GetSignalClient(m_mapping.IregName());
        if (scb != NULL) {
          istd_TRC(istd::eTrcMed, "(eCOffset) Write default for " << get_name() << ":" << o );
          scb->SetOffset(o);
        }
      } catch (istd::Exception& ie) {
        Tango::Except::throw_exception(
                "istd::Exception", ie.what(), __PRETTY_FUNCTION__);
      }
    }

}

/**
 * Return the signal offset setting.
 */
template<>
void LiberaControlAttr<eCOffset>::read(Tango::DeviceImpl *a_dev, Tango::Attribute &a_attr)
{
    istd_FTRC();
    LiberaDeviceServer* dev = dynamic_cast<LiberaDeviceServer *>(a_dev);
    LiberaSignalClientBase* scb = dev->GetSignalClient(m_mapping.IregName());
    if (scb != NULL) {
        Tango::DevLong val = scb->GetOffset();
        a_attr.set_value(&val);
    }
}

/**
 * Set the signal offset setting.
 */
template<>
void LiberaControlAttr<eCOffset>::write(Tango::DeviceImpl *a_dev, Tango::WAttribute &a_attr)
{
    istd_FTRC();
    LiberaDeviceServer* dev = dynamic_cast<LiberaDeviceServer *>(a_dev);
    std::string path = m_mapping.FullName(dev->GetInstanceName());

    try {
        LiberaSignalClientBase* scb = dev->GetSignalClient(m_mapping.IregName());
        if (scb != NULL) {
            Tango::DevLong val;
            a_attr.get_write_value(val);
            scb->SetOffset(val);
        }
    } catch (istd::Exception& ie) {
        Tango::Except::throw_exception(
            "istd::Exception", ie.what(), __PRETTY_FUNCTION__);
    }
}

/**
 * Control attribute for signal length.
 */
template<>
LiberaControlAttr<eCLength>::LiberaControlAttr(const LiberaMapping& a_mapping)
    : Tango::Attr(a_mapping.TangoName(), Tango::DEV_ULONG, Tango::READ_WRITE),
      m_mapping(a_mapping)
{
    istd_FTRC();
    istd_TRC(istd::eTrcMed, "Created ControlAttr for signal length: "
        << m_mapping.IregName() << ", " << m_mapping.NodeValType()
        << ", " << m_mapping.TangoName()
        << ", " << m_mapping.ControlType()
        << ", " << m_mapping.GetSize());
}

/**
 * Write default value (if any)
 */
template<>
void LiberaControlAttr<eCLength>::writeDefaultValue(Tango::DeviceImpl *a_dev)
{

    istd_FTRC();
    if(m_mapping.HasDefaultValue()) {
      try {
        string d;
        m_mapping.GetDefaultValue(d);
        Tango::DevLong l = std::stol(d);
        LiberaDeviceServer* dev = dynamic_cast<LiberaDeviceServer *>(a_dev);
        LiberaSignalClientBase* scb = dev->GetSignalClient(m_mapping.IregName());
        if (scb != NULL) {
          istd_TRC(istd::eTrcMed, "(eCLength) Write default for " << get_name() << ":" << l );
          scb->SetLength(l);
        }
      } catch (istd::Exception& ie) {
        Tango::Except::throw_exception(
                "istd::Exception", ie.what(), __PRETTY_FUNCTION__);
      }
    }

}

/**
 * Return the signal length setting.
 */
template<>
void LiberaControlAttr<eCLength>::read(Tango::DeviceImpl *a_dev, Tango::Attribute &a_attr)
{
    istd_FTRC();
    LiberaDeviceServer* dev = dynamic_cast<LiberaDeviceServer *>(a_dev);
    LiberaSignalClientBase* scb = dev->GetSignalClient(m_mapping.IregName());
    if (scb != NULL) {
        Tango::DevULong val = scb->GetLength();
        a_attr.set_value(&val);
    }
}

/**
 * Set the signal length setting.
 */
template<>
void LiberaControlAttr<eCLength>::write(Tango::DeviceImpl *a_dev, Tango::WAttribute &a_attr)
{
    istd_FTRC();
    LiberaDeviceServer* dev = dynamic_cast<LiberaDeviceServer *>(a_dev);
    std::string path = m_mapping.FullName(dev->GetInstanceName());

    try {
        LiberaSignalClientBase* scb = dev->GetSignalClient(m_mapping.IregName());
        if (scb != NULL) {
            Tango::DevULong val;
            a_attr.get_write_value(val);
            scb->SetLength(val);
        }
    } catch (istd::Exception& ie) {
        Tango::Except::throw_exception(
            "istd::Exception", ie.what(), __PRETTY_FUNCTION__);
    }
}

/**
 * Control attribute for signal Machine Time meta data.
 */
template<>
LiberaControlAttr<eCMT>::LiberaControlAttr(const LiberaMapping& a_mapping)
    : Tango::Attr(a_mapping.TangoName(), Tango::DEV_ULONG, Tango::READ_WRITE),
      m_mapping(a_mapping)
{
    istd_FTRC();
    istd_TRC(istd::eTrcMed, "Created ControlAttr for signal MT: "
        << m_mapping.IregName() << ", " << m_mapping.NodeValType()
        << ", " << m_mapping.TangoName()
        << ", " << m_mapping.ControlType()
        << ", " << m_mapping.GetSize());
}

/**
 * Write default value (if any)
 */
template<>
void LiberaControlAttr<eCMT>::writeDefaultValue(Tango::DeviceImpl *a_dev)
{

    istd_FTRC();
    if(m_mapping.HasDefaultValue()) {
      istd_TRC(istd::eTrcMed, "(eCMT) Write default for " << get_name() << ": Not supported " );
    }

}

/**
 * Return the signal MT data.
 */
template<>
void LiberaControlAttr<eCMT>::read(Tango::DeviceImpl *a_dev, Tango::Attribute &a_attr)
{
    istd_FTRC();
    LiberaDeviceServer* dev = dynamic_cast<LiberaDeviceServer *>(a_dev);
    LiberaSignalClientBase* scb = dev->GetSignalClient(m_mapping.IregName());
    if (scb != NULL) {
        Tango::DevULong val = scb->GetMT();
        a_attr.set_value(&val);
    }
}

/**
 * Set the signal MT data.
 */
template<>
void LiberaControlAttr<eCMT>::write(Tango::DeviceImpl *a_dev, Tango::WAttribute &a_attr)
{
    istd_FTRC();
    LiberaDeviceServer* dev = dynamic_cast<LiberaDeviceServer *>(a_dev);
    std::string path = m_mapping.FullName(dev->GetInstanceName());

    try {
        LiberaSignalClientBase* scb = dev->GetSignalClient(m_mapping.IregName());
        if (scb != NULL) {
            Tango::DevULong val;
            a_attr.get_write_value(val);
            // not possible: scb->SetMT(val);
        }
    } catch (istd::Exception& ie) {
        Tango::Except::throw_exception(
            "istd::Exception", ie.what(), __PRETTY_FUNCTION__);
    }
}


/**
 * Control attribute for averaging enabling.
 */
template<>
LiberaControlAttr<eCAvgEnable>::LiberaControlAttr(const LiberaMapping& a_mapping)
        : Tango::Attr(a_mapping.TangoName(), Tango::DEV_BOOLEAN, Tango::READ_WRITE),
          m_mapping(a_mapping)
{
  istd_FTRC();
  istd_TRC(istd::eTrcMed, "Created ControlAttr for signal enable: "
          << m_mapping.IregName() << ", " << m_mapping.NodeValType()
          << ", " << m_mapping.TangoName()
          << ", " << m_mapping.ControlType()
          << ", " << m_mapping.GetSize());
}

/**
 * Write default value (if any)
 */
template<>
void LiberaControlAttr<eCAvgEnable>::writeDefaultValue(Tango::DeviceImpl *a_dev)
{
  istd_FTRC();
  if(m_mapping.HasDefaultValue()) {
    try {
      string d;
      m_mapping.GetDefaultValue(d);
      Tango::DevBoolean b = d != "0";
      LiberaDeviceServer* dev = dynamic_cast<LiberaDeviceServer *>(a_dev);
      LiberaSignalClientBase* scb = dev->GetSignalClient(m_mapping.IregName());
      if (scb != NULL) {
        istd_TRC(istd::eTrcMed, "(eCEnable) Write default for " << get_name() << ":" << b );
        scb->SetAveraging(b);
      }
    } catch (istd::Exception& ie) {
      Tango::Except::throw_exception(
              "istd::Exception", ie.what(), __PRETTY_FUNCTION__);
    }
  }

}

/**
 * Return the signal averaging enabled state.
 */
template<>
void LiberaControlAttr<eCAvgEnable>::read(Tango::DeviceImpl *a_dev, Tango::Attribute &a_attr)
{
  istd_FTRC();
  LiberaDeviceServer* dev = dynamic_cast<LiberaDeviceServer *>(a_dev);
  LiberaSignalClientBase* scb = dev->GetSignalClient(m_mapping.IregName());
  if (scb != NULL) {
    Tango::DevBoolean b = scb->IsAveraging();
    a_attr.set_value(&b);
  }
}

/**
 * Put the signal to enable or disable averaging.
 */
template<>
void LiberaControlAttr<eCAvgEnable>::write(Tango::DeviceImpl *a_dev, Tango::WAttribute &a_attr)
{
  istd_FTRC();
  LiberaDeviceServer* dev = dynamic_cast<LiberaDeviceServer *>(a_dev);
  std::string path = m_mapping.FullName(dev->GetInstanceName());

  try {
    LiberaSignalClientBase* scb = dev->GetSignalClient(m_mapping.IregName());
    if (scb != NULL) {
      Tango::DevBoolean val;
      a_attr.get_write_value(val);
      scb->SetAveraging(val);
    }
  } catch (istd::Exception& ie) {
    Tango::Except::throw_exception(
            "istd::Exception", ie.what(), __PRETTY_FUNCTION__);
  }
}

/**
 * Control attribute for averaging length.
 */
template<>
LiberaControlAttr<eCAvgLength>::LiberaControlAttr(const LiberaMapping& a_mapping)
        : Tango::Attr(a_mapping.TangoName(), Tango::DEV_ULONG, Tango::READ_WRITE),
          m_mapping(a_mapping)
{
  istd_FTRC();
  istd_TRC(istd::eTrcMed, "Created ControlAttr for signal length: "
          << m_mapping.IregName() << ", " << m_mapping.NodeValType()
          << ", " << m_mapping.TangoName()
          << ", " << m_mapping.ControlType()
          << ", " << m_mapping.GetSize());
}

/**
 * Write default value (if any)
 */
template<>
void LiberaControlAttr<eCAvgLength>::writeDefaultValue(Tango::DeviceImpl *a_dev)
{

  istd_FTRC();
  if(m_mapping.HasDefaultValue()) {
    try {
      string d;
      m_mapping.GetDefaultValue(d);
      Tango::DevLong l = std::stol(d);
      LiberaDeviceServer* dev = dynamic_cast<LiberaDeviceServer *>(a_dev);
      LiberaSignalClientBase* scb = dev->GetSignalClient(m_mapping.IregName());
      if (scb != NULL) {
        istd_TRC(istd::eTrcMed, "(eCLength) Write default for " << get_name() << ":" << l );
        scb->SetAveragingLength(l);
      }
    } catch (istd::Exception& ie) {
      Tango::Except::throw_exception(
              "istd::Exception", ie.what(), __PRETTY_FUNCTION__);
    }
  }

}

/**
 * Return the signal length setting.
 */
template<>
void LiberaControlAttr<eCAvgLength>::read(Tango::DeviceImpl *a_dev, Tango::Attribute &a_attr)
{
  istd_FTRC();
  LiberaDeviceServer* dev = dynamic_cast<LiberaDeviceServer *>(a_dev);
  LiberaSignalClientBase* scb = dev->GetSignalClient(m_mapping.IregName());
  if (scb != NULL) {
    Tango::DevULong val = scb->GetAveragingLength();
    a_attr.set_value(&val);
  }
}

/**
 * Set the averaging length.
 */
template<>
void LiberaControlAttr<eCAvgLength>::write(Tango::DeviceImpl *a_dev, Tango::WAttribute &a_attr)
{
  istd_FTRC();
  LiberaDeviceServer* dev = dynamic_cast<LiberaDeviceServer *>(a_dev);
  std::string path = m_mapping.FullName(dev->GetInstanceName());

  try {
    LiberaSignalClientBase* scb = dev->GetSignalClient(m_mapping.IregName());
    if (scb != NULL) {
      Tango::DevULong val;
      a_attr.get_write_value(val);
      scb->SetAveragingLength(val);
    }
  } catch (istd::Exception& ie) {
    Tango::Except::throw_exception(
            "istd::Exception", ie.what(), __PRETTY_FUNCTION__);
  }
}
