/*
 * Copyright (c) 2014 Instrumentation Technologies
 * All Rights Reserved.
 *
 * $Id: libera_device_server.cpp 21334 2014-12-24 09:51:16Z tomaz.beltram $
 */

#include <istd/log.h>

#include "libera_mapping.h"

#include "libera_device_server.h"

const char *c_localHost = "127.0.0.1";
const size_t c_defaultSignalLength(100);

using mci::operator<<;

/**
 * Constructor calls base device class and initialization.
 */
LiberaDeviceServer::LiberaDeviceServer(Tango::DeviceClass *cl, const char *s,LiberaConfig& a_cfg)
    : Device_5Impl(cl, s),
      m_connected(false),
      m_callback(&LiberaDeviceServer::CallbackNotification, this),
      m_client(m_callback)
{
    istd_FTRC();

    m_typeEventMap.insert(NodeValTypePair<mci::eNvBool>());
    m_typeEventMap.insert(NodeValTypePair<mci::eNvLong>());
    m_typeEventMap.insert(NodeValTypePair<mci::eNvULong>());
    m_typeEventMap.insert(NodeValTypePair<mci::eNvLongLong>());
    m_typeEventMap.insert(NodeValTypePair<mci::eNvULongLong>());
    m_typeEventMap.insert(NodeValTypePair<mci::eNvDouble>());
    m_typeEventMap.insert(NodeValTypePair<mci::eNvFloat>());
    m_typeEventMap.insert(NodeValTypePair<mci::eNvString>());
    m_typeEventMap.insert(NodeValTypePair<mci::eNvEnumeration>());

    TypeConstructorMap tm;
    tm.insert(SignalAtomTypePair<isig::eTypeDouble>());
    tm.insert(SignalAtomTypePair<isig::eTypeFloat>());
    tm.insert(SignalAtomTypePair<isig::eTypeUInt64>());
    tm.insert(SignalAtomTypePair<isig::eTypeInt64>());
    tm.insert(SignalAtomTypePair<isig::eTypeUInt32>());
    tm.insert(SignalAtomTypePair<isig::eTypeInt32>());
    tm.insert(SignalAtomTypePair<isig::eTypeUInt16>());
    tm.insert(SignalAtomTypePair<isig::eTypeInt16>());
    tm.insert(SignalAtomTypePair<isig::eTypeUInt8>());
    tm.insert(SignalAtomTypePair<isig::eTypeInt8>());

    bpmNodes = a_cfg.GetBPMNodes();

    // Store attributes that need listening.
    for (auto i = a_cfg.begin(); i != a_cfg.end(); ++i) {
        const LiberaMapping& lm(*i);
        if (lm.IsSignal()) {
            if( lm.IsSum() && lm.IsSumRow() ) {
              // Fatal error: Incorrect configuration cannot be both sum and sumRow
              cerr << "Incorrect configuration for " << lm.TangoName() << " cannot be both sum and sumRow";
              exit(0);
            }
            // find or create new signal client object
            if (m_signalMap.find(lm.IregName()) == m_signalMap.end()) {
                istd_TRC(istd::eTrcMed, "new signal map: " << lm.IregName());
                m_signalMap[lm.IregName()] = tm[lm.SignalAtomType()](this, lm);
            }
            istd_TRC(istd::eTrcMed, "add signal component: " << lm.TangoName());
            m_signalMap[lm.IregName()]->AddAttr(lm.TangoName(),lm.HasEvent(),lm.IsSum(),lm.IsSumRow(),
                        lm.HasStartStopIndex(),lm.HasAverage(),lm.MapToDouble(),lm.RemoveOffset());
        }
        else if (lm.IsListen()) {
            m_listen.push_back(lm);
        }
    }

    istd_TRC(istd::eTrcLow, "Initialize device: " << s);
    init_device();


}

/*
 * Return connected root node reference.
 */
mci::Node& LiberaDeviceServer::GetRoot(const bool a_platform)
{
    istd_FTRC();
    if (a_platform) {
        return m_platform;
    }
    return m_root;
}

/*
 * Return the board name served by this instance.
 */
const std::string& LiberaDeviceServer::GetInstanceName()
{
    istd_FTRC();
    return m_instance;
}

/**
 * Register all notifications.
 */
void LiberaDeviceServer::RegisterNotifications()
{
    istd_FTRC();
    // TODO check if connected, move after connect?

    for (auto i = m_listen.begin(); i != m_listen.end(); ++i) {
        // Register notification callbacks for nodes in the list.
        std::string path_str = i->FullName(m_instance);
        mci::Node& root = GetRoot(i->IsPlatform());
        mci::Node node = root.GetNode(mci::Tokenize(path_str));
        m_client.Register(node);
        istd_TRC(istd::eTrcLow, "Registered notifications for: " << path_str);
        if (i->IsInstance()) {
            istd_TRC(istd::eTrcLow, "... on " << m_instance);
        }

        // Store attribute name for sending event.
        std::ostringstream path;
        path << node.GetFullPath(true); // skip root
        m_notificationMap[path.str()] = i->TangoName();
        istd_TRC(istd::eTrcLow, "Stored notifications for: " << path.str()
            << ", " << i->TangoName());
    }
}

/**
 * Create signals on remote nodes.
 */
void LiberaDeviceServer::CreateSignals()
{
    istd_FTRC();
    for (auto i = m_signalMap.begin(); i != m_signalMap.end(); ++i) {
        LiberaSignalClientBase* pSig = i->second;
        const LiberaMapping& lm = pSig->LM();
        std::string path_str = lm.FullName(m_instance);
        istd_TRC(istd::eTrcLow, "Create signal for: " << path_str);
        if (lm.IsInstance()) {
            istd_TRC(istd::eTrcLow, "... on " << m_instance);
        }
        mci::Node& root = GetRoot(lm.IsPlatform());
        try {
            mci::Node n = root.GetNode(mci::Tokenize(path_str));
            pSig->Create(n,m_refreshPeriod);
        } catch (istd::Exception& e) {
            istd_TRC(istd::eTrcLow, "Skipped creation of nonexisting signal: "
                << path_str << ", because of " << e.what());
        }
    }
}

template <mci::NodeValType_e NodeValType>
void LiberaDeviceServer::PushChangeEventFunc(LiberaDeviceServer* a_dev,
    const std::string& a_name, const istd::Any& a_val, const bool a_isArray)
{
    istd_FTRC();

    typedef typename NodeValTraits<NodeValType>::LType LiberaType;
    typedef typename NodeValTraits<NodeValType>::TType TangoType;
    typedef typename std::vector<LiberaType> LiberaTypeVec;
    typedef typename std_vector<TangoType>::Type TangoTypeVec;

    if (a_isArray) {
        LiberaTypeVec lval = a_val;
        size_t len = a_val.Size();
        istd_TRC(istd::eTrcDetail, "array event " << a_name << ", " << len);
        TangoTypeVec tval(len);
        for (size_t i(0); i < len; ++i) {
            tval[i] = lval[i];
            istd_TRC(istd::eTrcDetail, "  " << lval[i] << ", " << tval[i]);
        }
        a_dev->push_change_event(a_name, reinterpret_cast<TangoType*>(&tval[0]), len);
    }
    else {
        // declare Tango type value
        TangoType tval;

        // get value by type
        tval = LiberaType(a_val);

        // send event with changed value
        a_dev->push_change_event(a_name, &tval);
    }
}

template <>
void LiberaDeviceServer::PushChangeEventFunc<mci::eNvString>(
    LiberaDeviceServer* a_dev, const std::string& a_name,
    const istd::Any& a_val, const bool a_isArray)
{
    istd_FTRC();

    if (a_isArray) {
        istd_LOGe("Unsupported string array ireg node.");
        return;
    }

    // get value by type
    NodeValTraits<mci::eNvString>::LType lval = a_val;
    // Cast away constness and hope Tango will not abuse it.
    NodeValTraits<mci::eNvString>::TType tval =
        const_cast<NodeValTraits<mci::eNvString>::TType>(lval.c_str());

    // send event with changed value
    a_dev->push_change_event(a_name, &tval);
}

/**
 * Called on registered node value change.
 */
void LiberaDeviceServer::CallbackNotification(const mci::NotificationData& a_nft)
{
    istd_FTRC();

    // Find Tango attribute name for this notification.
    std::ostringstream path;
    path << a_nft.m_node.GetFullPath(true); // skip root
    //TODO: if (m_notificationMap.find() == m_notificationMap.end()) return;
    std::string name = m_notificationMap[path.str()];

    mci::NodeValType_e t = a_nft.m_node.GetValueType();
    if (m_typeEventMap.find(t) == m_typeEventMap.end()) {
        istd_LOGe("Unsupported ireg node value type (" << t
            << ") in notification data conversion!");
    }
    else {
        m_typeEventMap[t](this, name, a_nft.m_data, a_nft.m_node.IsArray());
        istd_TRC(istd::eTrcDetail, "Send event for notification of: "
            << path.str() << ", " << name << ", "
            << a_nft.m_data);
    }
}

template<isig::AtomType_e SignalAtomType>
LiberaSignalClientBase* LiberaDeviceServer::SignalClientConstructor(
    Tango::DeviceImpl* a_dev, const LiberaMapping& a_lm)
{
    istd_FTRC();
    return new LiberaSignalClient<SignalAtomType>(a_dev, c_defaultSignalLength, a_lm);
}

LiberaSignalClientBase* LiberaDeviceServer::GetSignalClient(const std::string& a_node)
{
    istd_FTRC();
    if (m_signalMap.find(a_node) == m_signalMap.end()) {
        istd_TRC(istd::eTrcMed, "read signal node not found: " << a_node);
        return NULL;
    }
    return m_signalMap[a_node];
}

/**
 * Delete method called befor object is destroyed.
 */
void LiberaDeviceServer::delete_device()
{
    istd_FTRC();
    Disconnect();
    delete self;
    self = NULL;

}

/**
 * Connects to LBASE application via MCI.
 */
void LiberaDeviceServer::init_device() {

  istd_FTRC();

  //  Read device properties from database.
  Tango::DbData dev_prop;
  dev_prop.push_back(Tango::DbDatum("LiberaBoard"));
  dev_prop.push_back(Tango::DbDatum("RefreshPeriod"));
  dev_prop.push_back(Tango::DbDatum("BPMCoefSetName"));
  dev_prop.push_back(Tango::DbDatum("Ka"));
  dev_prop.push_back(Tango::DbDatum("Kb"));
  dev_prop.push_back(Tango::DbDatum("Kc"));
  dev_prop.push_back(Tango::DbDatum("Kd"));
  dev_prop.push_back(Tango::DbDatum("AGCMinADC"));
  dev_prop.push_back(Tango::DbDatum("AGCMaxADC"));

  get_db_device()->get_property(dev_prop);
  if (dev_prop[0].is_empty() == false) {
    dev_prop[0] >> m_instance;
    m_instance = "boards." + m_instance + ".";
    istd_TRC(istd::eTrcLow, "Board instance: " << m_instance);
  } else {
    istd_LOGe("LiberaBoard property not found in database!");
    Tango::Except::throw_exception("Libera DS",
                                   "Missing mandatory LiberaBoard property", __PRETTY_FUNCTION__);
  }

  m_refreshPeriod = 100; // 100 msec
  if (dev_prop[1].is_empty() == false) {
    dev_prop[1] >> m_refreshPeriod;
    istd_TRC(istd::eTrcLow, "Refresh period (ms): " << m_refreshPeriod);
  }

  double ka = 1;
  if (dev_prop[3].is_empty() == false)
    dev_prop[3] >> ka;

  double kb = 1;
  if (dev_prop[4].is_empty() == false)
    dev_prop[4] >> kb;

  double kc = 1;
  if (dev_prop[5].is_empty() == false)
    dev_prop[5] >> kc;

  double kd = 1;
  if (dev_prop[6].is_empty() == false)
    dev_prop[6] >> kd;

  if (dev_prop[2].is_empty() == false) {

    string hBpmSetName;
    string vBpmSetName;
    string bpmSetName;
    dev_prop[2] >> bpmSetName;
    hBpmSetName = "H_" + bpmSetName;
    vBpmSetName = "V_" + bpmSetName;

    // Retreive BPM coef from the class
    Tango::Database *db = Tango::Util::instance()->get_database();
    Tango::DbData db_data;
    db_data.push_back(Tango::DbDatum(hBpmSetName));
    db_data.push_back(Tango::DbDatum(vBpmSetName));
    db->get_class_property("LiberaDeviceClass", db_data);
    if (db_data[0].is_empty()) {
      Tango::Except::throw_exception(
              "ErrorInit",
              hBpmSetName + " not defined for class LiberaDeviceClass",
              "LiberaDeviceServer::init_device"
      );
    }
    if (db_data[1].is_empty()) {
      Tango::Except::throw_exception(
              "ErrorInit",
              vBpmSetName + "not defined for class LiberaDeviceClass",
              "LiberaDeviceServer::init_device"
      );
    }
    vector<double> hBpmCoefs;
    vector<double> vBpmCoefs;
    db_data[0] >> hBpmCoefs;
    db_data[1] >> vBpmCoefs;

    // Sets the polynomial coeficient on all concerned node
    for (int i = 0; i < (int) bpmNodes.size(); i++) {

      LiberaSignalClientBase *clnt = m_signalMap[bpmNodes[i]];
      if (clnt == NULL) {
        Tango::Except::throw_exception(
                "ErrorInit",
                "BPM node: " + bpmNodes[i] + " unknown ireg node",
                "LiberaDeviceServer::init_device"
        );
      }

      // Set this node as a BPM node
      clnt->SetBPM();

      if (!clnt->SetBPMPolynomial(hBpmCoefs, vBpmCoefs, ka, kb, kc, kd)) {
        Tango::Except::throw_exception(
                "ErrorInit",
                "BPM node: " + bpmNodes[i] + " error while setting polynamial coefficents",
                "LiberaDeviceServer::init_device"
        );
      }


    }

  }

  agcMinADC = 3000;
  if (dev_prop[7].is_empty() == false)
    dev_prop[7] >> agcMinADC;

  agcMaxADC = 5000;
  if (dev_prop[8].is_empty() == false)
    dev_prop[8] >> agcMaxADC;

  incoherencyRef = 0.0;
  hOffset = 0.0;
  vOffset = 0.0;
  hOffsetFine = 0.0;
  vOffsetFine = 0.0;
  tiltAngle = 0.0;
  agcEnable = false;

  if (Connect()) {
    RegisterNotifications();
    CreateSignals();
    set_state(Tango::ON);
    set_status("Device is OK\nAGC: Off");
  }

  self = new Tango::DeviceProxy(get_name());

}

Tango::DevState LiberaDeviceServer::dev_state() {

  // Auto Gain Control

  if( agcEnable ) {

    set_state(Tango::RUNNING);
    set_status("Device is OK\nAGC: On");

    Tango::DevULong maxADC;
    Tango::DevULong att;

    try {

      self->read_attribute("MaxADC") >> maxADC;
      self->read_attribute("Attenuation") >> att;

      int newAtt = (int)att;
      if( maxADC>(Tango::DevULong)agcMaxADC )
        newAtt = newAtt + 1;
      if( maxADC<(Tango::DevULong)agcMinADC )
        newAtt = newAtt - 3;

      if( newAtt>31 ) {
        newAtt = 31;
        set_state(Tango::ALARM);
        set_status("Device is Alarm\nAGC: Alarm, maximum attenuation reached");
      }

      if( newAtt<0 ) {
        newAtt = 0;
        set_state(Tango::ALARM);
        set_status("Device is Alarm\nAGC: Alarm, minimum attenuation reached");
      }

      att = (Tango::DevULong)newAtt;
      Tango::DeviceAttribute da("Attenuation",att);
      self->write_attribute(da);

    } catch (Tango::DevFailed &e) {
      set_state(Tango::ALARM);
      set_status("Device is Alarm\nAGC: Error " + string(e.errors[0].desc.in()));
    }

  } else {

    set_state(Tango::ON);
    set_status("Device is OK\nAGC: Off");

  }

  if (get_state()!=Tango::ALARM)
    Tango::DeviceImpl::dev_state();
	return get_state();

}

void LiberaDeviceServer::SetCalculationAlgorithm(Tango::DevShort algo) {

  // Apply calculation algotirhm on all BPM nodes
  for (int i = 0; i < (int) bpmNodes.size(); i++) {
    m_signalMap[bpmNodes[i]]->SetCalculationAlgorithm(algo);
  }

}

void LiberaDeviceServer::SetHOffset(Tango::DevDouble offset) {

  hOffset = offset;
  UpdateHOffset();

}

void LiberaDeviceServer::SetVOffset(Tango::DevDouble offset) {

  vOffset = offset;
  UpdateVOffset();

}

void LiberaDeviceServer::SetHOffsetFine(Tango::DevDouble offset) {

  hOffsetFine = offset;
  UpdateHOffset();

}

void LiberaDeviceServer::SetVOffsetFine(Tango::DevDouble offset) {

  vOffsetFine = offset;
  UpdateVOffset();

}

Tango::DevDouble LiberaDeviceServer::GetHOffset() {
  return hOffset;
}
Tango::DevDouble LiberaDeviceServer::GetHOffsetFine() {
  return hOffsetFine;
}
Tango::DevDouble LiberaDeviceServer::GetVOffset() {
  return vOffset;
}
Tango::DevDouble LiberaDeviceServer::GetVOffsetFine() {
  return vOffsetFine;
}

void LiberaDeviceServer::SetTiltAngle(Tango::DevDouble angle) {

  tiltAngle = angle;
  // Apply h offset on all BPM nodes
  for (int i = 0; i < (int) bpmNodes.size(); i++) {
    m_signalMap[bpmNodes[i]]->SetTiltAngle(tiltAngle);
  }

}

Tango::DevDouble LiberaDeviceServer::GetTiltAngle() {
  return tiltAngle;
}

void LiberaDeviceServer::UpdateHOffset() {

  Tango::DevDouble hOffsetTotal = hOffset + hOffsetFine;
  NodeValTraits<mci::eNvLong>::LType hOffsetTotalnm = (long)(hOffsetTotal * 1e9);

  // Send offset to the MCI (in nm)
  try {

    std::string path_str = "application.fa_dsp.position.off_x";
    mci::Node node = m_root.GetNode(mci::Tokenize(path_str));
    node.Set(hOffsetTotalnm);

  } catch (istd::Exception& ie) {
    Tango::Except::throw_exception(
            "istd::Exception",
            string("MCI error:") + ie.what(),
            "LiberaDeviceServer::UpdateHOffset");
  }

  // Apply h offset on all BPM nodes
  for (int i = 0; i < (int) bpmNodes.size(); i++) {
    m_signalMap[bpmNodes[i]]->SetHOffset(hOffsetTotal);
  }

}

void LiberaDeviceServer::UpdateVOffset() {

  Tango::DevDouble vOffsetTotal = vOffset + vOffsetFine;
  NodeValTraits<mci::eNvLong>::LType vOffsetTotalnm = (long)(vOffsetTotal * 1e9);

  // Send offset to the MCI (in nm)
  try {

    std::string path_str = "application.fa_dsp.position.off_y";
    mci::Node node = m_root.GetNode(mci::Tokenize(path_str));
    node.Set(vOffsetTotalnm);

  } catch (istd::Exception& ie) {
    Tango::Except::throw_exception(
            "istd::Exception",
            string("MCI error:") + ie.what(),
            "LiberaDeviceServer::UpdateVOffset");
  }

  // Apply v offset on all BPM nodes
  for (int i = 0; i < (int) bpmNodes.size(); i++) {
    m_signalMap[bpmNodes[i]]->SetVOffset(vOffsetTotal);
  }

}

void LiberaDeviceServer::SetIncoherencyRef(Tango::DevDouble ref) {

  incoherencyRef = ref;

}

void LiberaDeviceServer::SetIncoherencyRefFromReading() {

  // Update incoherency ref
  Tango::DevLong i;
  self->read_attribute("SA_Incoherency") >> i;
  incoherencyRef = i;

  // Save to DB
  Tango::Util *tg = Tango::Util::instance();
  Tango::Database *db = tg->get_database();
  Tango::DbDatum Attr("SA_Incoherency_Ref");
  Tango::DbDatum Value("__value");
  Attr << (short)1;
  Value << incoherencyRef;
  Tango::DbData db_val;
  db_val.push_back(Attr);
  db_val.push_back(Value);
  tg->get_database()->put_device_attribute_property(get_name(), db_val);

}

Tango::DevDouble LiberaDeviceServer::GetIncoherencyRef() {

  return incoherencyRef;

}

Tango::DevDouble LiberaDeviceServer::GetIncoherencyDrift() {

  Tango::DevLong i;
  self->read_attribute("SA_Incoherency") >> i;
  return incoherencyRef - i;

}

void LiberaDeviceServer::SetAGCEnable(bool enable) {

  agcEnable = enable;

}

void LiberaDeviceServer::always_executed_hook()
{
    istd_FTRC();
}

/**
  * Destroy allocated structures.
  */
LiberaDeviceServer::~LiberaDeviceServer()
{
    istd_FTRC();
    //TODO delete m_signalMap members
    delete_device();
}

/**
 * Connection handling method.
 */
void LiberaDeviceServer::Connect(mci::Node &a_root, mci::Root a_type)
{
    if (a_root.IsValid()) {
        // The root node was connected by some other device instance.
        istd_TRC(istd::eTrcLow, "Already connected root node!");
        return;
    }

    // make new connection
    try {
        a_root = mci::Connect(c_localHost, a_type);
    }
    catch (istd::Exception e)
    {
        istd_TRC(istd::eTrcLow, "Exception thrown while connecting root node!");
        istd_TRC(istd::eTrcLow, e.what());
    }
}

/**
 * Connect to application and platform daemons.
 */
bool LiberaDeviceServer::Connect()
{
    istd_FTRC();

    m_connected = false;

    Connect(m_root, mci::Root::Application);
    Connect(m_platform, mci::Root::Platform);

    // update attributes for the first time
    if (m_root.IsValid()) { // && m_platform.IsValid()) {
        // TODO: set root node connection for signals
        m_connected = true;
        istd_TRC(istd::eTrcLow,
            "Connection to platform and application succeeded.");
    }
    else {
        istd_TRC(istd::eTrcLow,
            "Connection to platform or application failed.");
    }
    return m_connected;
}

void LiberaDeviceServer::Disconnect(mci::Node &a_root, mci::Root a_type)
{
    // destroy root node to force disconnect
    try {
        a_root.Destroy();
    }
    catch (istd::Exception e)
    {
        istd_TRC(istd::eTrcLow, "Exception thrown while destroying root node!");
        istd_TRC(istd::eTrcLow, e.what());
    }
    a_root = mci::Node();

    // disconnect if connected
    mci::Disconnect(c_localHost, a_type);
}

void LiberaDeviceServer::Disconnect()
{
    istd_FTRC();

    // stop attribute update loop
    m_connected = false;

    Disconnect(m_root, mci::Root::Application);
    Disconnect(m_platform, mci::Root::Platform);
}
