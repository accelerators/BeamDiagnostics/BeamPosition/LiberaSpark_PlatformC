# app file

The app configuration file of the libera-ds Tango server is located in: */opt/libera/sbin/cfg/tango/*. It consists of a list of Tango attribute definitions. The order within a node must follow the ireg node structure.

`ireg_name` `tango_att` `access` `data_type` `size` `flags` [min max] [default] [startIdxName] [stopIdxName]

## ireg_name

Name of the ireg node

## tango_att

Name of the Tango attribute

## access

  when >0

    eNfReadable     = (1ULL << 1),   2
    eNfWritable     = (1ULL << 2),   4
    eNfPersistent   = (1ULL << 3),   8
    eNfUnlockedRead = (1ULL << 4),   16
    eNfConstant     = (1ULL << 5),   32
    eNfHidden       = (1ULL << 6),   64
    eNfExecutable   = (1ULL << 7),   128
    eNfAttribute    = (1ULL << 8),   256
    eNfArray        = (1ULL << 9),   512
    eNfSignal       = (1ULL << 10),  1024
    eNfPluggable    = (1ULL << 11),  2048
    eNfDefault      = (Flags_e::eNfReadable | Flags_e::eNfWritable)

  when <0 (control type)

    eCUndefined = 0
    eCEnable = -1        // Enable/disable ireg node
    eCMode   = -2        // Set access mode
    eCOffset = -3        // Set reading offset
    eCLength = -4        // Buffer length
    eCMT     = -5        // Time meta data
    eCAvgEnable = -6     // Enable/disable averaging of averaged items of this ireg node (Stream only)
    eCAvgLength = -7     // Set the averaging length in number of sample to average (Stream only)

## data_type (ignored if control type)

###For eNfSignal:

    eTypeDouble     = 1
    eTypeFloat      = 2
    eTypeUInt64     = 3
    eTypeInt64      = 4
    eTypeUInt32     = 5
    eTypeInt32      = 6
    eTypeUInt16     = 7
    eTypeInt16      = 8
    eTypeUInt8      = 9
    eTypeInt8       = 10
        
###For other node:

    eNvUndefined = 0
    eNvBool = 1
    eNvLong = 2
    eNvULong = 3
    eNvLongLong = 4
    eNvULongLong = 5
    eNvDouble = 6
    eNvFloat = 7
    eNvString = 8
    eNvEnumeration = 9  

## size (eNfArray node only, ignored otherwise)

Size of the ireg node

## Flags

Flags is a combination of the follwing bits:

    ScalarFlag      1      Build tango attribute as scalar
    MinMaxFlag      2      Define a min and max value
    ExpertViewFlag  4      Put the attribute in expert view
    ParamMemorised  8      Set the attribute as memorized
    DefaultValue    16     Set the default value when the server starts
    HasEvent        32     Allow events
    ComputeSum      64     Compute the sum of the column of a reg node (this flag should be used on an 
                           extra item added at the end of the ireg-node)
    SumStartStopIdx 128    To be used together with ComputeSum. Creates 2 attributes 
                           [startIdxName] and [atopIdxName]. The sum is calculated between minIdx and maxIdx.
    HasAverage      256    Average this item when averaging is enabled for the ireg node (Stream only)
    MapToDouble     512    Convert node to double
    IsSumRow       1024    Compure the sum of the row of a ireg node (this flag should be used on an
                           extra item added at the end of the ireg-node)
    RemoveOffset   2048    Compute an avergage of last 20 samples and substract it from the node data
    
# bpm-ireg file

The bpm-ireg configuration file of the libera-ds Tango server is located in: */opt/libera/sbin/cfg/tango/*.
It contains the list of ireg node which are a BPM. Polynamial calculation can be enabled only on theses nodes.
Structure of a BPM ireg node must be {Va,Vb,Vc,Vd,Sum,Incoherency,HPosition,VPosition}.

# ds-features file

This files contains a list enabled features.

    ZyncTemp            Display ZyncTemp attribute (Temperature of chip)
    AGC                 Auto gain control
    SAIncoherencyDrift  SA stream Incoherency drift calculation

# Polynomial configuration

Polynomial coeficients are defined by a set of 2 class properties of the class LiberaDeviceClass starting with H_ and V_ followed by an arbitraty suffix. The coeficient are given in the following order:

i<sub>0</sub> * X<sup>0</sup>Y<sup>0</sup> + i<sub>1</sub> * X<sup>0</sup>Y<sup>1</sup> + ... + i<sub>n</sub> * X<sup>0</sup>Y<sup>n</sup> + i<sub>n+1</sub> * X<sup>1</sup>Y<sup>0</sup> + i<sub>n+2</sub> * X<sup>1</sup>Y<sup>1</sup> + ... + i<sub>2n</sub> * X<sup>1</sup>Y<sup>n-1</sup> +  i<sub>2n+1</sub> * X<sup>2</sup>Y<sup>0</sup> + ...

For a n<sup>th</sup> order polynomial, the number of coeficient must be equal to:</br>

n*(n+1)/2 + (n+1)

When CalcAlgorithm attribute is set to 1, all positions within a BPM node (see bpm-ireg) are recomputed using the following forumla:

  a = V<sub>a</sub>*kA<br/>
  b = V<sub>b</sub>*kB<br/>
  c = V<sub>c</sub>*kC<br/>
  d = V<sub>d</sub>*kD<br/>
  X = (a-b-c+d)/(a+b+c+d)<br/>
  Y = (a+b-c-d)/(a+b+c+d)<br/>
  X<sub>pos</sub> = H_poly(X,Y) + H<sub>offset</sub><br/>
  Y<sub>pos</sub> = V_poly(X,Y) + V<sub>offset</sub><br/>

Where H<sub>offset</sub> and V<sub>offset</sub> are the same offsets as defined for the defaut DoS calculation.


```  
#---------------------------------------------------------
# CLASS LiberaDeviceClass properties 
#---------------------------------------------------------

# Exemple with a set of 2 7th order polynomial

CLASS/LiberaDeviceClass->H_BPM_Coef_Set1: 0.050497,\ 
                                          0,\ 
                                          -0.13711,\ 
                                          0,\ 
                                          0.131497,\ 
                                          0,\ 
                                          -0.20355,\ 
                                          0,\ 
                                          15.35155,\ 
                                          0,\ 
                                          -27.6858,\ 
                                          0,\ 
                                          5.257479,\ 
                                          0,\ 
                                          -0.04602,\ 
                                          0.086645,\ 
                                          0,\ 
                                          0.439082,\ 
                                          0,\ 
                                          -0.20366,\ 
                                          0,\ 
                                          13.91676,\ 
                                          0,\ 
                                          35.9299,\ 
                                          0,\ 
                                          15.26707,\ 
                                          -0.267,\ 
                                          0,\ 
                                          -1.69893,\ 
                                          0,\ 
                                          -21.1792,\ 
                                          0,\ 
                                          -123.094,\ 
                                          1.483509,\ 
                                          0,\ 
                                          92.71761

CLASS/LiberaDeviceClass->V_BPM_Coef_Set1: 0,\ 
                                          14.43176,\ 
                                          0,\ 
                                          3.78652,\ 
                                          0,\ 
                                          2.359179,\ 
                                          0,\ 
                                          -0.23795,\ 
                                          0,\ 
                                          -0.12099,\ 
                                          0,\ 
                                          0.117987,\ 
                                          0,\ 
                                          0.113516,\ 
                                          0,\ 
                                          0,\ 
                                          -10.0604,\ 
                                          0,\ 
                                          14.747,\ 
                                          0,\ 
                                          21.68874,\ 
                                          0,\ 
                                          0.126257,\ 
                                          0,\ 
                                          -0.44056,\ 
                                          0,\ 
                                          0,\ 
                                          -2.50319,\ 
                                          0,\ 
                                          -81.0382,\ 
                                          0,\ 
                                          0.144465,\ 
                                          0,\ 
                                          0,\ 
                                          45.61492,\ 
                                          0

```  

At the device level, you have to define Ka,Kb,Kc and Kd coeficients and the polynomial used (by using the suffix):

```
sr/d-bpm/c07-2->BPMCoefSetName: BPM_Coef_Set1
sr/d-bpm/c07-2->Ka: 1
sr/d-bpm/c07-2->Kb: 1
sr/d-bpm/c07-2->Kc: 1
sr/d-bpm/c07-2->Kd: 1
```

