package SparkConfig;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;


/* A specific table for Jive */
public class SparkTable extends JTable {

  final String[] colNames = {"IREG node","Tango Att","Access","Type","Size","Flas","Min","Max","Default","StartIdxName","StopIdxName"};

  BitEditor accessEditor;
  BitEditor flagsEditor;
  JPopupMenu treeMenu;
  JMenuItem addMenu;
  JMenuItem removeMenu;
  JMenuItem editMenu;
  final EditFrame editFrame;
  int selectedRow;
  int selectedCol;
  final DefaultTableModel _model;

  public SparkTable(DefaultTableModel model) {

    super(model);
    _model = model;

    selectedRow = -1;
    selectedCol = -1;

    accessEditor = new BitEditor(Access.Names,Access.Idx);
    flagsEditor = new BitEditor(Flags.Names,Flags.Idx);
    setDefaultRenderer(Access.class, accessEditor);
    setDefaultRenderer(Flags.class, flagsEditor);

    editFrame = new EditFrame(this);

    treeMenu = new JPopupMenu();
    addMenu = new JMenuItem("Add");
    removeMenu = new JMenuItem("Remove");
    editMenu = new JMenuItem("Edit");
    treeMenu.add(editMenu);
    treeMenu.add(removeMenu);
    //treeMenu.add(addMenu);

    editMenu.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        editFrame.setValues(_model,selectedRow);
        editFrame.setVisible(true);
      }
    });

    removeMenu.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {

        _model.removeRow(selectedRow);
        selectedRow = -1;
        selectedCol = -1;

      }
    });


  }

  protected void processEvent(AWTEvent e) {

    if( e instanceof MouseEvent) {
      MouseEvent me = (MouseEvent)e;
      if(me.getButton()==3 && me.getID()==MouseEvent.MOUSE_PRESSED) {
        selectedCol = getColumnForLocation(me.getX());
        selectedRow = getRowForLocation(me.getY());
        treeMenu.show(this,me.getX(),me.getY());
      }
      if(me.getButton()==1 && me.getClickCount()==2) {
        selectedCol = getColumnForLocation(me.getX());
        selectedRow = getRowForLocation(me.getY());
        editFrame.setValues(_model,selectedRow);
        editFrame.setVisible(true);
      }
    }
    super.processEvent(e);

  }

  private int cellHeight(int row, int col) {
    return getDefaultRenderer(getColumnClass(col)).getTableCellRendererComponent(this,
           getModel().getValueAt(row, col), false, false, row, col).getPreferredSize().height + 1;
  }

  void updateRow(int row) {
    int maxHeight = 0;
    for (int j = 0; j < getColumnCount(); j++) {
      int ch;
      if ((ch = cellHeight(row, j)) > maxHeight) {
        maxHeight = ch;
      }
    }
    setRowHeight(row, maxHeight);
  }

  public void updateRows() {
    for (int i = 0; i < getRowCount(); i++) {
      updateRow(i);
    }
  }


  public int getRowForLocation(int y) {

    boolean found = false;
    int i = 0;
    int h = 0;

    while(i<getModel().getRowCount() && !found) {
      found = (y>=h && y<=h+getRowHeight(i));
      if(!found) {
        h+=getRowHeight(i);
        i++;
      }
    }

    if(found) {
      return i;
    } else {
      return -1;
    }

  }

  public void setValue(Object[][] values) {

    selectedRow = -1;
    selectedCol = -1;

    _model.setDataVector(values, colNames);
    getColumnModel().getColumn(0).setPreferredWidth(100);
    getColumnModel().getColumn(1).setPreferredWidth(120);
    getColumnModel().getColumn(2).setPreferredWidth(accessEditor.getPreferredWidth());
    getColumnModel().getColumn(3).setPreferredWidth(40);
    getColumnModel().getColumn(4).setPreferredWidth(50);
    getColumnModel().getColumn(5).setPreferredWidth(flagsEditor.getPreferredWidth());
    getColumnModel().getColumn(6).setPreferredWidth(50);
    getColumnModel().getColumn(7).setPreferredWidth(50);
    getColumnModel().getColumn(8).setPreferredWidth(50);
    getColumnModel().getColumn(9).setPreferredWidth(100);
    getColumnModel().getColumn(10).setPreferredWidth(100);

    updateRows();

  }

  public int getColumnForLocation(int x) {

    boolean found = false;
    int i = 0;
    int w = 0;

    while(i<getModel().getColumnCount() && !found) {
      int cWidth = getColumnModel().getColumn(i).getWidth();
      found = (x>=w && x<=w+cWidth);
      if(!found) {
        w+=cWidth;
        i++;
      }
    }

    if(found) {
      return i;
    } else {
      return -1;
    }

  }

}
