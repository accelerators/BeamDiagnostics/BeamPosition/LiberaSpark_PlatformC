package SparkConfig;

public class Flags extends BitValue {

  final static String[] Names = {"Scalar","MinMax","Expert","Memorized","Default","Event","Sum","Start/Stop","Avg","Double","SumRow","RemoveOffset"};
  final static int[] Idx = {0,1,2,3,4,5,6,7,8,9,10,11};

  public Flags(int value) {
    this.value = value;
  }

  public boolean hasMinMax() {
    return (value&2)!=0;
  }

  public boolean hasDefault() {
    return (value&16)!=0;
  }

  public boolean hasStartStop() {
    return (value&128)!=0;
  }

}
