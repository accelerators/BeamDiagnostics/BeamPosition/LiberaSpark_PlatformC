package SparkConfig;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Vector;

/**
 * A panel to edit app file
 */
public class MainPanel extends JFrame {

  JScrollPane tableView;
  SparkTable sparkTable;
  private DefaultTableModel dm;
  private final String _appFile;

  MainPanel(String appFile) {

    _appFile = appFile;
    getContentPane().setLayout(new BorderLayout());
    getContentPane().setPreferredSize(new Dimension(1400,600));

    // Table model
    dm = new DefaultTableModel() {

      public Class getColumnClass(int columnIndex) {
        switch (columnIndex) {
          case 2:
            return Access.class;
          case 4:
            return Integer.class;
          case 5:
            return Flags.class;
          case 6:
          case 7:
          case 8:
            return Double.class;
          default:
            return String.class;
        }
      }

      public boolean isCellEditable(int row, int column) {
        return false;
      }

      public void setValueAt(Object aValue, int row, int column) {
        super.setValueAt(aValue,row,column);
      }

    };

    // Table initialisation
    sparkTable = new SparkTable(dm);
    tableView = new JScrollPane(sparkTable);
    add(tableView, BorderLayout.CENTER);


    try {
      loadFile(appFile);
    } catch (IOException e) {
      JOptionPane.showMessageDialog(this,"Cannot load " + _appFile + "\n" + e.getMessage(),"Error",JOptionPane.ERROR_MESSAGE);
    }

    JPanel btnPanel = new JPanel();
    btnPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
    getContentPane().add(btnPanel,BorderLayout.SOUTH);

    JButton saveBtn = new JButton("Save");
    saveBtn.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        try {
          saveFile();
          JOptionPane.showMessageDialog(null, _appFile + "\nSuccesfully saved", "Information", JOptionPane.INFORMATION_MESSAGE);
        } catch (IOException ex) {
          JOptionPane.showMessageDialog(null,"Cannot save " + _appFile + "\n" + ex.getMessage(),"Error",JOptionPane.ERROR_MESSAGE);
        }
      }
    });
    btnPanel.add(saveBtn);

    JButton closeBtn = new JButton("Exit");
    closeBtn.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        System.exit(0);
      }
    });
    btnPanel.add(closeBtn);

    setTitle("Spark App File editor 1.1 [" + appFile + "]");
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    pack();
    setLocationRelativeTo(null);
    setVisible(true);

  }

  public String[] readLine(FileReader fr) throws IOException {

    Vector<String> v = new Vector<String>();
    boolean eof = false;
    boolean eol = false;
    int c=0;

    while (!eol && !eof) {

      StringBuffer w = new StringBuffer();

      // Jump space
      while (!eof && !eol && c <= 32) {
        c = fr.read();
        eof = (c < 0);
        eol = (c=='\n');
      }

      if (!eof && !eol)
        w.append((char) c);

      boolean eow = false;
      while (!eof && !eow && !eol) {
        c = fr.read();
        eof = (c < 0);
        eol = (c=='\n');
        if (!eof) {
          eow = c <= 32;
          if (!eow)
            w.append((char) c);
        }
      }

      if(w.length()>0)
        v.add(w.toString());

    }

    String[] ret = new String[v.size()];
    for(int i=0;i<v.size();i++)
      ret[i] = v.get(i);
    return ret;

  }

  public int parseInt(String fieldName,String word) throws IOException {
    try {
      return Integer.parseInt(word);
    } catch (NumberFormatException e) {
      throw new IOException("Wrong " + fieldName + "\n" + e.getMessage());
    }
  }

  public double parseDouble(String fieldName,String word) throws IOException {
    try {
      return Double.parseDouble(word);
    } catch (NumberFormatException e) {
      throw new IOException("Wrong " + fieldName + "\n" + e.getMessage());
    }
  }

  public void loadFile(String appFile) throws IOException {

    FileReader fr = new FileReader(appFile);
    boolean eof = false;
    Vector<Object[]> v = new Vector<Object[]>();

    while(!eof) {

      String[] words = readLine(fr);
      eof = (words.length==0);
      if( !eof && words.length<5 ) {
        throw new IOException(words[0]+" incorrect number of parameters");
      }
      if(!eof) {

        Object[] item = new Object[11];

        item[0] = words[0];  // IREG node
        item[1] = words[1]; // Tango Att
        Access A = new Access(parseInt("Access",words[2]));
        item[2] = A;
        item[3] = new DataType(parseInt("Type",words[3]),A.value); //Type
        item[4] = parseInt("Size",words[4]); // Size

        Flags f = new Flags(0);
        int extIdx = 5;
        if( words.length>extIdx ) {
          f.value = parseInt("Flags",words[extIdx]);
          extIdx++;
        }

        item[5] = f; // Flags
        item[6] = null;
        item[7] = null;
        item[8] = null;
        item[9] = null;
        item[10] = null;

        if(f.hasMinMax()) {
          if( words.length<extIdx+2 ) {
            throw new IOException(words[0]+" incorrect number of parameters");
          }
          item[6] = parseDouble("Min value", words[extIdx]);
          item[7] =  parseDouble("Max value", words[extIdx + 1]);
          extIdx+=2;
        }

        if(f.hasDefault()) {
          if( words.length<extIdx+1 ) {
            throw new IOException(words[0]+" incorrect number of parameters");
          }
          item[8] = parseDouble("Default value",words[extIdx]);
          extIdx++;
        }

        if(f.hasStartStop()) {
          if( words.length<extIdx+2 ) {
            throw new IOException(words[0]+" incorrect number of parameters");
          }
          item[9] = words[extIdx];
          item[10] = words[extIdx+1];
        }

        v.add(item);
      }

    }


    Object[][] prop = new Object[v.size()][];
    for(int i=0;i<v.size();i++)
      prop[i] = v.get(i);

    sparkTable.setValue(prop);

  }

  public void saveFile() throws IOException {

    FileWriter f = new FileWriter(_appFile);
    TableModel model = sparkTable.getModel();
    int nb = model.getRowCount();

    for (int i = 0; i < nb; i++) {

      StringBuffer str = new StringBuffer();

      String iRegName = (String) model.getValueAt(i, 0);
      String attName = (String) model.getValueAt(i, 1);
      Access access = (Access) model.getValueAt(i, 2);
      DataType type = (DataType) model.getValueAt(i, 3);
      int size = (Integer) model.getValueAt(i, 4);
      Flags flags = (Flags) model.getValueAt(i, 5);
      Double min = (Double) model.getValueAt(i, 6);
      Double max = (Double) model.getValueAt(i, 7);
      Double defaultValue = (Double) model.getValueAt(i, 8);
      String startName = (String) model.getValueAt(i, 9);
      String stopName = (String) model.getValueAt(i, 10);

      str.append(iRegName);
      str.append(" ");
      str.append(attName);
      str.append(" ");
      str.append(access.value);
      str.append(" ");
      str.append(type.type);
      str.append(" ");
      str.append(size);
      str.append(" ");

      if (flags.value != 0) {

        str.append(flags.value);
        str.append(" ");

        if (flags.hasMinMax()) {

          if (type.isInteger()) {
            str.append((long) min.doubleValue());
            str.append(" ");
            str.append((long) max.doubleValue());
            str.append(" ");
          } else {
            str.append(min);
            str.append(" ");
            str.append(max);
            str.append(" ");
          }


        }

        if (flags.hasDefault()) {
          if (type.isInteger()) {
            str.append((long) defaultValue.doubleValue());
            str.append(" ");
          } else {
            str.append(defaultValue);
            str.append(" ");
          }

        }

        if (flags.hasStartStop()) {
          str.append(startName);
          str.append(" ");
          str.append(stopName);
          str.append(" ");
        }

      }

      f.write(str.toString().trim());
      if(i<nb-1) f.write("\n");

    }

    f.close();

  }

  public static void main(String args[]) {
    if(args.length!=1) {
      System.out.println("jsparkconfig app-filename");
      System.exit(0);
    }
    new MainPanel(args[0]);
  }

}
