package SparkConfig;

public class Access extends BitValue {

  final static String[] Names = {"Read","Write","Pers.","Unlck","Const","Hidden","Exec","Attr","Array","Signal","Plug"};
  final static int[] Idx = {1,2,3,4,5,6,7,8,9,10,11};

  public Access(int value) {
    this.value = value;
  }

  public static boolean isSignal(int value) {
    return (value&(1<<10))!=0;
  }

}
