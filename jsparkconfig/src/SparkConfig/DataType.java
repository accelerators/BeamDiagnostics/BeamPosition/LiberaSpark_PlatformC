package SparkConfig;

public class DataType {

  final static String[] nodeTypeNames = {"Undef","Bool","Long","ULong","Long64","ULong64","Double","Float","String","Enum"};
  final static String[] sigTypeNames =  {"Undef","Double","Float","UInt64","Int64","UInt32","Int32","UInt16","Int16","UInt8","Int8"};
  final static String[] ctrlTypeNames = {"Undef","Bool","Long","Long","ULong","ULong","Bool","ULong"};

  int type;
  int access;

  public DataType(int type,int access) {
    this.type = type;
    this.access = access;
    if(access<0)
      this.type = 0;
  }

  public boolean isInteger() {
    String typeStr = toString();
    if( typeStr.equalsIgnoreCase("Float") || typeStr.equalsIgnoreCase("Double"))
      return false;
    else
      return true;
  }

  public String toString() {
    if( access<0 ) {
      return ctrlTypeNames[-access];
    } else if( Access.isSignal(access) ) {
      return sigTypeNames[type];
    } else {
      return nodeTypeNames[type];
    }

  }

}
