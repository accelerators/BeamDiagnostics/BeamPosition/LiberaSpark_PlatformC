package SparkConfig;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.table.TableCellRenderer;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.font.FontRenderContext;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.util.Vector;

/**
 * A components to edit bit of an integer
 */
public class BitEditor extends JComponent implements MouseListener,TableCellRenderer,ActionListener {

  final static String[] controlNames = {"Undefined","Ctrl:Enable","Ctrl:Mode","Ctrl:Offset","Ctrl:Length","Ctrl:CMT","Ctrl:AvgEnable","Ctrl:AvgLength"};

  static Font labelFont = new Font("Dialog",Font.PLAIN,9);
  static Font textFont = new Font("Dialog",Font.PLAIN,12);
  static Color selectedColor = new Color(184,207,229);
  static BufferedImage toggleON = null;
  static BufferedImage toggleOFF = null;
  static BufferedImage dummy = null;
  static FontRenderContext frc = null;
  static int MARGIN = 2;

  static {

    if( toggleON==null ) {
      try {
        toggleON = ImageIO.read(BitEditor.class.getResourceAsStream("/SparkConfig/toggle_on.png"));
      } catch (IOException e) {
        System.out.println("Cannot load /SparkConfig/toggle_on.png");
      }
    }

    if( toggleOFF==null ) {
      try {
        toggleOFF = ImageIO.read(BitEditor.class.getResourceAsStream("/SparkConfig/toggle_off.png"));
      } catch (IOException e) {
        System.out.println("Cannot load /SparkConfig/toggle_off.png");
      }
    }

    dummy = new BufferedImage(8,9,BufferedImage.TYPE_INT_RGB);
    frc = ((Graphics2D)dummy.getGraphics()).getFontRenderContext();

  }

  class BitInfo {
    String bitName;
    int bitIndex;
    int width;
    int height;
    BitInfo(String name,int idx) {
      bitName = name;
      bitIndex = idx;
      Rectangle2D r = labelFont.getStringBounds(bitName,frc);
      width = (int)r.getWidth();
      height = (int)r.getHeight();
    }
  }

  private Vector<BitInfo> bitInfos;
  private int value;
  private boolean editable = false;
  JPopupMenu controlMenu;
  JMenuItem[] controlItems;

  public BitEditor(boolean editable) {

    bitInfos = new Vector<BitInfo>();
    value = 0;
    this.editable = editable;
    if(editable) addMouseListener(this);
    controlMenu = new JPopupMenu();
    controlItems = new JMenuItem[controlNames.length+1];
    for(int i=1;i<controlNames.length;i++) {
      controlItems[i] = new JMenuItem(controlNames[i]);
      controlMenu.add(controlItems[i]);
      controlItems[i].addActionListener(this);
    }

  }

  public BitEditor() {
    this(false);
  }

  public BitEditor(String[] names,int[] idx,boolean editable) {
    this(editable);
    if(names.length!= idx.length)
      throw new IllegalStateException("BitEditor(): Wrong array dimension.");

    for(int i=0;i<names.length;i++)
      addBit(names[i],idx[i]);
  }

  public BitEditor(String[] names,int[] idx) {
    this(names,idx,false);
  }

  public void addBit(String name,int bitIndex) {
    BitInfo bi =new BitInfo(name,bitIndex);
    bitInfos.add(bi);
  }

  public void setValue(int value) {
    this.value = value;
  }

  public int getValue() {
    return value;
  }

  public void paintToggle(Graphics g,int x,int y,boolean state) {
    g.drawImage(state?toggleON:toggleOFF,x,y,null);
  }

  public int getPreferredWidth() {
    return getPreferredSize().width;
  }

  public Dimension getPreferredSize() {
    int W = MARGIN;
    int H = 0;
    for(int i=0;i<bitInfos.size();i++) {
      W += bitInfos.get(i).width + MARGIN;
      if( bitInfos.get(i).height>H ) H = bitInfos.get(i).height;
    }
    W += MARGIN;
    return new Dimension(W,10+H);
  }

  public void paint(Graphics g) {

    // Background
    Dimension d = getSize();
    g.setColor(getBackground());
    g.fillRect(0,0,d.width,d.height);

    if(value<0) {
      // We have a control signal
      int idx = -value;
      g.setFont(textFont);
      g.setColor(Color.BLACK);
      g.drawString(controlNames[idx],MARGIN,g.getFontMetrics().getAscent() + 3);
      return;
    }

    //g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
    //    RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
    //g2.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS,
    //    RenderingHints.VALUE_FRACTIONALMETRICS_ON);


    int xPos = MARGIN;
    g.setFont(labelFont);
    g.setColor(Color.BLACK);
    int a = g.getFontMetrics().getAscent();

    for(int i=0;i<bitInfos.size();i++) {

      BitInfo bi = bitInfos.get(i);
      int mask = (int)1 << bi.bitIndex;
      paintToggle(g,xPos+(bi.width-8)/2,2,(value&mask)!=0);
      g.drawString(bi.bitName,xPos,10+a);
      xPos += bi.width + MARGIN;

    }

  }

  @Override
  public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {

    if( value instanceof BitValue )
      setValue(((BitValue)value).value);
    if( isSelected )
      setBackground(selectedColor);
    else
      setBackground(Color.WHITE);
    return this;

  }

  @Override
  public void actionPerformed(ActionEvent e) {

    Object src = e.getSource();
    boolean found = false;
    int i=0;
    while(i<controlItems.length && !found) {
      found = controlItems[i] == src;
      if(!found) i++;
    }

    if(found) {
      value = -(i+1);
    }

    repaint();

  }

  @Override
  public void mouseClicked(MouseEvent e) {

    int X = e.getX();
    int Y = e.getY();

    if( value<0 ) {
      controlMenu.show(this,X,Y);
      return;
    }

    boolean found = false;
    int i = 0;
    int xpos = MARGIN;
    while(!found && i<bitInfos.size()) {
      found = X>xpos && X<xpos+bitInfos.get(i).width;
      if(!found) {
        xpos+=bitInfos.get(i).width+MARGIN;
        i++;
      }
    }
    if(found) {

      int mask = 0x1 << bitInfos.get(i).bitIndex;
      if((mask&value)==0) {
        value |= mask;
      } else {
        value &= ~mask;
      }
      repaint();

    }


  }

  @Override
  public void mousePressed(MouseEvent e) {  }

  @Override
  public void mouseReleased(MouseEvent e) {  }

  @Override
  public void mouseEntered(MouseEvent e) {  }

  @Override
  public void mouseExited(MouseEvent e) {  }


}
