package SparkConfig;

import javax.swing.*;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

public class EditFrame extends JFrame implements ActionListener {

  JTextField iregNodeText;
  JTextField attNameText;
  BitEditor accessEditor;
  JComboBox typeCombo;
  JTextField sizeText;
  BitEditor flagsEditor;
  JTextField minText;
  JTextField maxText;
  JTextField defaultText;
  JTextField startIdxNameText;
  JTextField stopIdxNameText;
  JButton applyButton;
  JButton cancelButton;
  TableModel editModel;
  int editRow;

  private void populateCombo(JComboBox combo,String[] items) {
    combo.removeAllItems();
    for(int i=0;i<items.length;i++)
      combo.addItem(items[i]);
  }

  public EditFrame(JComponent parent) {

    JPanel innerPanel = new JPanel();
    innerPanel.setLayout(null);
    innerPanel.setPreferredSize(new Dimension(635,370));


    int lWidth = 120;
    int tWidth = 500;
    int yPos = 5;

    JLabel l1 = new JLabel("IREG Node");
    l1.setBounds(10,yPos,lWidth,25);
    innerPanel.add(l1);
    iregNodeText = new JTextField();
    iregNodeText.setEditable(true);
    iregNodeText.setBounds(lWidth + 10, yPos, tWidth, 25);
    innerPanel.add(iregNodeText);
    yPos += 30;

    JLabel l2 = new JLabel("Attribute");
    l2.setBounds(10,yPos,lWidth,25);
    innerPanel.add(l2);
    attNameText = new JTextField();
    attNameText.setEditable(true);
    attNameText.setBounds(lWidth + 10, yPos, tWidth, 25);
    innerPanel.add(attNameText);
    yPos += 30;

    JLabel l3 = new JLabel("Access");
    l3.setBounds(10,yPos,lWidth,25);
    innerPanel.add(l3);
    accessEditor = new BitEditor(Access.Names,Access.Idx,true);
    accessEditor.setBounds(lWidth + 10, yPos, tWidth, 25);
    innerPanel.add(accessEditor);
    yPos += 30;

    JLabel l4 = new JLabel("Type");
    l4.setBounds(10,yPos,lWidth,25);
    innerPanel.add(l4);
    typeCombo = new JComboBox();
    typeCombo.setBounds(lWidth + 10, yPos, tWidth, 25);
    innerPanel.add(typeCombo);
    yPos += 30;

    JLabel l5 = new JLabel("Size");
    l5.setBounds(10,yPos,lWidth,25);
    innerPanel.add(l5);
    sizeText = new JTextField();
    sizeText.setBounds(lWidth + 10, yPos, tWidth, 25);
    innerPanel.add(sizeText);
    yPos += 30;

    JLabel l6 = new JLabel("Flags");
    l6.setBounds(10,yPos,lWidth,25);
    innerPanel.add(l6);
    flagsEditor = new BitEditor(Flags.Names,Flags.Idx,true);
    flagsEditor.setBounds(lWidth + 10, yPos, tWidth, 25);
    innerPanel.add(flagsEditor);
    yPos += 30;

    JLabel l7 = new JLabel("Minimum");
    l7.setBounds(10,yPos,lWidth,25);
    innerPanel.add(l7);
    minText = new JTextField();
    minText.setEditable(true);
    minText.setBounds(lWidth + 10, yPos, tWidth, 25);
    innerPanel.add(minText);
    yPos += 30;

    JLabel l8 = new JLabel("Maximum");
    l8.setBounds(10,yPos,lWidth,25);
    innerPanel.add(l8);
    maxText = new JTextField();
    maxText.setEditable(true);
    maxText.setBounds(lWidth + 10, yPos, tWidth, 25);
    innerPanel.add(maxText);
    yPos += 30;

    JLabel l9 = new JLabel("Default");
    l9.setBounds(10,yPos,lWidth,25);
    innerPanel.add(l9);
    defaultText = new JTextField();
    defaultText.setEditable(true);
    defaultText.setBounds(lWidth + 10, yPos, tWidth, 25);
    innerPanel.add(defaultText);
    yPos += 30;

    JLabel l10 = new JLabel("StartIdx AttName");
    l10.setBounds(10,yPos,lWidth,25);
    innerPanel.add(l10);
    startIdxNameText = new JTextField();
    startIdxNameText.setEditable(true);
    startIdxNameText.setBounds(lWidth + 10, yPos, tWidth, 25);
    innerPanel.add(startIdxNameText);
    yPos += 30;

    JLabel l11 = new JLabel("StopIdx AttName");
    l11.setBounds(10, yPos, lWidth, 25);
    innerPanel.add(l11);
    stopIdxNameText = new JTextField();
    stopIdxNameText.setEditable(true);
    stopIdxNameText.setBounds(lWidth + 10, yPos, tWidth, 25);
    innerPanel.add(stopIdxNameText);
    yPos += 30;

    applyButton = new JButton("Apply");
    applyButton.addActionListener(this);
    applyButton.setBounds(lWidth + tWidth - 90,yPos,100,25);
    innerPanel.add(applyButton);

    cancelButton = new JButton("Cancel");
    cancelButton.addActionListener(this);
    cancelButton.setBounds(lWidth + tWidth - 200,yPos,100,25);
    innerPanel.add(cancelButton);

    setContentPane(innerPanel);
    setTitle("Item Editor");
    setLocationRelativeTo(parent);
    setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
    pack();

  }


  public void setValues(TableModel model,int selectedRow) {

    editModel = model;
    editRow = selectedRow;

    String iRegName = (String)editModel.getValueAt(selectedRow,0);
    String attName = (String)editModel.getValueAt(selectedRow,1);
    Access access = (Access)editModel.getValueAt(selectedRow,2);
    DataType type = (DataType)editModel.getValueAt(selectedRow,3);
    int size = (Integer)editModel.getValueAt(selectedRow,4);
    Flags flags = (Flags)editModel.getValueAt(selectedRow,5);
    Double min = (Double)editModel.getValueAt(selectedRow,6);
    Double max = (Double)editModel.getValueAt(selectedRow,7);
    Double defaultValue = (Double)editModel.getValueAt(selectedRow,8);
    String startName = (String)editModel.getValueAt(selectedRow,9);
    String stopName = (String)editModel.getValueAt(selectedRow, 10);

    iregNodeText.setText(iRegName);
    attNameText.setText(attName);
    accessEditor.setValue(access.value);
    if( access.value<0 ) {
      populateCombo(typeCombo,DataType.ctrlTypeNames);
      typeCombo.setSelectedIndex(-access.value);
    } else if( Access.isSignal(access.value) ) {
      populateCombo(typeCombo,DataType.sigTypeNames);
      typeCombo.setSelectedIndex(type.type);
    } else {
      populateCombo(typeCombo,DataType.nodeTypeNames);
      typeCombo.setSelectedIndex(type.type);
    }
    sizeText.setText(Integer.toString(size));
    flagsEditor.setValue(flags.value);
    if(min!=null) minText.setText(Double.toString(min));
    else          minText.setText("");
    if(max!=null) maxText.setText(Double.toString(max));
    else          maxText.setText("");
    if(defaultValue!=null) defaultText.setText(Double.toString(defaultValue));
    else          defaultText.setText("");
    if(startName!=null) startIdxNameText.setName(startName);
    else startIdxNameText.setText("");
    if(stopName!=null) stopIdxNameText.setName(stopName);
    else stopIdxNameText.setText("");

  }

  public Integer parseInt(String fieldName,String word) {
    try {
      return Integer.parseInt(word);
    } catch (NumberFormatException e) {
      JOptionPane.showMessageDialog(this,"Wrong " + fieldName + "\n" + e.getMessage(),"Error",JOptionPane.ERROR_MESSAGE);
    }
    return null;
  }

  public Double parseDouble(String fieldName,String word) {
    try {
      return Double.parseDouble(word);
    } catch (NumberFormatException e) {
      JOptionPane.showMessageDialog(this,"Wrong " + fieldName + "\n" + e.getMessage(),"Error",JOptionPane.ERROR_MESSAGE);
    }
    return null;
  }

  @Override
  public void actionPerformed(ActionEvent e) {

    Object src = e.getSource();

    if( src==cancelButton ) {

      setVisible(false);

    } else if ( src==applyButton ) {

      String iRegName = iregNodeText.getText();
      String attName = attNameText.getText();
      Access access = new Access(accessEditor.getValue());
      DataType type = new DataType(typeCombo.getSelectedIndex(),access.value);
      int size = parseInt("Size",sizeText.getText());
      Flags flags = new Flags(flagsEditor.getValue());
      Double min=null;
      Double max=null;
      Double defaultValue=null;
      String startName=null;
      String stopName=null;
      if(flags.hasMinMax()) {
        min = parseDouble("Minimum",minText.getText());
        max = parseDouble("Maximum",maxText.getText());
      }
      if(flags.hasDefault()) {
        defaultValue = parseDouble("Default Value",defaultText.getText());
      }
      if(flags.hasStartStop()) {
        startName = startIdxNameText.getText();
        stopName = stopIdxNameText.getText();
      }

      editModel.setValueAt(iRegName,editRow,0);
      editModel.setValueAt(attName,editRow,1);
      editModel.setValueAt(access,editRow,2);
      editModel.setValueAt(type,editRow,3);
      editModel.setValueAt(size,editRow,4);
      editModel.setValueAt(flags,editRow,5);
      editModel.setValueAt(min,editRow,6);
      editModel.setValueAt(max,editRow,7);
      editModel.setValueAt(defaultValue,editRow,8);
      editModel.setValueAt(startName,editRow,9);
      editModel.setValueAt(stopName,editRow,10);

      setVisible(false);

    }

  }

}
