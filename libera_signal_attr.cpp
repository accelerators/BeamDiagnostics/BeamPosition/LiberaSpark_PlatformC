/*
 * Copyright (c) 2014 Instrumentation Technologies
 * All Rights Reserved.
 *
 * $Id: libera_signal_attr.cpp 22552 2015-10-27 14:54:34Z luka.rahne $
 */

#include "libera_signal_attr.h"

#include "libera_device_server.h"


template<isig::AtomType_e SignalAtomType,class Base>
void LiberaSignalAttr<SignalAtomType,Base>::read(Tango::DeviceImpl *a_dev,
    Tango::Attribute &a_attr)
{
    istd_FTRC();
    LiberaDeviceServer* dev = dynamic_cast<LiberaDeviceServer *>(a_dev);
    std::string path = m_mapping.FullName(dev->GetInstanceName());

    try {
        LiberaSignalClientBase* scb = dev->GetSignalClient(m_mapping.IregName());
        if (scb != NULL) {
            mci::Node n = dev->GetRoot(m_mapping.IsPlatform()).GetNode(mci::Tokenize(path));
            istd_TRC(istd::eTrcHigh, "Get signal data: " << m_mapping.TangoName());
            scb->Read(m_mapping.TangoName(), a_attr);
        }
    } catch (istd::Exception& ie) {
        Tango::Except::throw_exception(
            "istd::Exception", ie.what(), __PRETTY_FUNCTION__);
    }
}

template<isig::AtomType_e SignalAtomType,class Base>
void LiberaSignalAttr<SignalAtomType,Base>::write(Tango::DeviceImpl *a_dev,
    Tango::WAttribute &a_attr)
{
    istd_FTRC();
}

template<isig::AtomType_e SignalAtomType,class Base>
bool LiberaSignalAttr<SignalAtomType,Base>::is_allowed(Tango::DeviceImpl *a_dev,
    Tango::AttReqType a_type)
{
    istd_FTRC();
    return true;
}

/**
 * Explicit template instantiations for supported classes so these can be
 * used in other compilation units.
 */
template class LiberaSignalAttr<isig::eTypeDouble,Tango::SpectrumAttr>;
template class LiberaSignalAttr<isig::eTypeFloat,Tango::SpectrumAttr>;
template class LiberaSignalAttr<isig::eTypeUInt64,Tango::SpectrumAttr>;
template class LiberaSignalAttr<isig::eTypeInt64,Tango::SpectrumAttr>;
template class LiberaSignalAttr<isig::eTypeUInt32,Tango::SpectrumAttr>;
template class LiberaSignalAttr<isig::eTypeInt32,Tango::SpectrumAttr>;
template class LiberaSignalAttr<isig::eTypeUInt16,Tango::SpectrumAttr>;
template class LiberaSignalAttr<isig::eTypeInt16,Tango::SpectrumAttr>;
template class LiberaSignalAttr<isig::eTypeUInt8,Tango::SpectrumAttr>;
template class LiberaSignalAttr<isig::eTypeInt8,Tango::SpectrumAttr>;

template class LiberaSignalAttr<isig::eTypeDouble,Tango::Attr>;
template class LiberaSignalAttr<isig::eTypeFloat,Tango::Attr>;
template class LiberaSignalAttr<isig::eTypeUInt64,Tango::Attr>;
template class LiberaSignalAttr<isig::eTypeInt64,Tango::Attr>;
template class LiberaSignalAttr<isig::eTypeUInt32,Tango::Attr>;
template class LiberaSignalAttr<isig::eTypeInt32,Tango::Attr>;
template class LiberaSignalAttr<isig::eTypeUInt16,Tango::Attr>;
template class LiberaSignalAttr<isig::eTypeInt16,Tango::Attr>;
template class LiberaSignalAttr<isig::eTypeUInt8,Tango::Attr>;
template class LiberaSignalAttr<isig::eTypeInt8,Tango::Attr>;
