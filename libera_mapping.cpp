/*
 * Copyright (c) 2014 Instrumentation Technologies
 * All Rights Reserved.
 *
 * $Id: libera_mapping.cpp 22552 2015-10-27 14:54:34Z luka.rahne $
 */

#include "libera_mapping.h"
#include <boost/lexical_cast.hpp>
#include "istd/assert.h"

const char* NodeValTraits<mci::eNvBool>::format = "%1.0f";
const char* NodeValTraits<mci::eNvLong>::format = "%10.0f";
const char* NodeValTraits<mci::eNvULong>::format = "%10.0f";
const char* NodeValTraits<mci::eNvLongLong>::format = "%20.0f";
const char* NodeValTraits<mci::eNvULongLong>::format = "%20.0f";
const char* NodeValTraits<mci::eNvDouble>::format = "%15.6f";
const char* NodeValTraits<mci::eNvFloat>::format = "%6.2f";
const char* NodeValTraits<mci::eNvString>::format = "%s";
const char* NodeValTraits<mci::eNvEnumeration>::format = "%2.0f";


static const uint32_t default_extra_parameter_value = 0;
// flag bits (index 0 in m_ep)
static const uint32_t ExtraParamScalarFlag = (1 << 0);
static const uint32_t ExtraParamMinMaxFlag = (1 << 1);
static const uint32_t ExtraParamExpertViewFlag = (1 << 2);
static const uint32_t ExtraParamMemorised = (1 << 3);
static const uint32_t ExtraParamHasDefault = (1 << 4);
static const uint32_t ExtraParamHasEvent = (1 << 5);
static const uint32_t ExtraParamIsSum = (1 << 6);
static const uint32_t ExtraParamHasStartStopIndex = (1 << 7);
static const uint32_t ExtraParamHasAverage = (1 << 8);
static const uint32_t ExtraParamMapToDouble = (1 << 9);
static const uint32_t ExtraParamIsSumRow = (1 << 10);
static const uint32_t ExtraParamRemoveOffset = (1 << 11);

// indexes in vector m_ep
static const size_t ExtraParamFlagIndex = 0;
static const size_t ExtraParamMinIndex = 1;
static const size_t ExtraParamMaxIndex = 2;


static void splitString(std::vector<std::string>& vec, std::string str) {
    using namespace std;
    istringstream iss(str);
    copy(istream_iterator<string>(iss),
         istream_iterator<string>(),
         back_inserter(vec));
}


/**
 * Parse the configuration line and construct the maping object from it.
 * The line has five fields as follows:
 * <ireg_name> <tango_name> <node_flags>|-<control_type> <value_type> <size>
 */
LiberaMapping::LiberaMapping(const std::string &a_line,
                             const std::string &a_ireg_prefix,
                             const std::string &a_tango_prefix,
                             const bool a_platform, const bool a_instance)
    : m_platform(a_platform), m_instance(a_instance), m_listen(false)
{
    std::stringstream sstr(a_line);
    int i;

    sstr >> m_ireg_name;
    sstr.ignore();

    sstr >> m_tango_name;
    sstr.ignore();

    sstr >> i;
    if (i > 0) {
        m_access = static_cast<mci::Flags_e>(i);
        m_control = eCUndefined;
    } else {
        m_access = static_cast<mci::Flags_e>(0);
        m_control = static_cast<Control_e>(-i);
    }
    sstr.ignore();

    sstr >> i;
    m_data_type = i;

    sstr >> m_size;

    // store remining data in vector<std::string> separated by spaces (or
    // multiple)
    std::string rem;
    std::getline(sstr, rem); // reimaing string
    splitString(m_extraparams, rem); 
    
    if ((m_extraparams.size() == 1) && (m_extraparams[0].size() == 0)) {
        m_extraparams.clear(); // remove if we have single empty string
    }

    if (m_ireg_name == ".") {
        m_ireg_name = a_ireg_prefix.substr(0, a_ireg_prefix.length() - 1);
        m_tango_name = a_tango_prefix;
    }
    else {
        m_ireg_name = a_ireg_prefix + m_ireg_name;
        m_tango_name = a_tango_prefix + m_tango_name;
    }
}

const char* LiberaMapping::IregName() const
{
    return m_ireg_name.c_str();
}

const char* LiberaMapping::TangoName() const
{
    return m_tango_name.c_str();
}

bool LiberaMapping::IsReadable() const
{
    return (m_access & mci::eNfReadable) != 0;
}

bool LiberaMapping::IsWritable() const
{
    return (m_access & mci::eNfWritable) != 0;
}

Tango::AttrWriteType LiberaMapping::TangoAccess() const
{
    Tango::AttrWriteType access = Tango::READ_WRITE;

    if (IsReadable() && !IsWritable()) {
        access = Tango::READ;
    }
    else if (!IsReadable() && IsWritable()) {
        access = Tango::WRITE;
    }
    return access;
}

bool LiberaMapping::IsArray() const
{
    return (m_access & mci::eNfArray) != 0;
}

bool LiberaMapping::IsSignal() const
{
    return (m_access & mci::eNfSignal) != 0;
}

bool LiberaMapping::IsControl() const
{
    return m_control != eCUndefined;
}

Control_e LiberaMapping::ControlType() const
{
    return m_control;
}

mci::NodeValType_e LiberaMapping::NodeValType() const
{
    //TODO: assert !IsSignal
    return static_cast<mci::NodeValType_e>(m_data_type);
}

isig::AtomType_e LiberaMapping::SignalAtomType() const
{
    //TODO: assert IsSignal
    return static_cast<isig::AtomType_e>(m_data_type);
}

bool LiberaMapping::IsPlatform() const
{
    return m_platform;
}

bool LiberaMapping::IsInstance() const
{
    return m_instance;
}

void LiberaMapping::SetListen()
{
    m_listen = true;
}

bool LiberaMapping::IsListen() const
{
    return m_listen;
}

std::string LiberaMapping::FullName(const std::string& a_instance) const
{
    if (m_instance) {
        return a_instance + m_ireg_name;
    }
    return m_ireg_name;
}

size_t LiberaMapping::GetSize() const
{
    return m_size;
}



uint32_t LiberaMapping::GetExtraParam() const
{
    if (m_extraparams.size() >= 1) {
        return boost::lexical_cast<uint32_t>(m_extraparams[ExtraParamFlagIndex]);
    }
    else {
        return default_extra_parameter_value;
    }
}

/******************************************************************************/
bool LiberaMapping::IsScalar() const
{
    return GetExtraParam() & ExtraParamScalarFlag;
}

/******************************************************************************/
bool LiberaMapping::IsMinMaxEnabled() const
{
    return GetExtraParam() & ExtraParamMinMaxFlag;
}

/******************************************************************************/
void LiberaMapping::GetMinMax(std::string &a_min, std::string &a_max) const
{
    // min max are arguments 1,2 in m_ep, enabled by flag ExtraParamMinMaxFlag
    istd_ASSERT_MSG(IsMinMaxEnabled(), "flag for MinMax is not set");
    istd_ASSERT_MSG(m_extraparams.size() >= (ExtraParamMaxIndex + 1),
                    "arguments for MinMax are missing");
    a_min = m_extraparams[ExtraParamMinIndex];
    a_max = m_extraparams[ExtraParamMaxIndex];
}

/******************************************************************************/
void LiberaMapping::GetDefaultValue(std::string &defaultValue) const
{
    size_t  ExtraParamDefaultIndex = 1;
    if(IsMinMaxEnabled())
        ExtraParamDefaultIndex += 2;

    istd_ASSERT_MSG(HasDefaultValue(), "flag for default value is not set");
    istd_ASSERT_MSG(m_extraparams.size() >= (ExtraParamDefaultIndex + 1),
                    "argument for default value is missing");
    defaultValue = m_extraparams[ExtraParamDefaultIndex];
}

/******************************************************************************/
bool LiberaMapping::HasDefaultValue() const
{
    return GetExtraParam() & ExtraParamHasDefault;
}

/******************************************************************************/
bool LiberaMapping::HasEvent() const
{
    return GetExtraParam() & ExtraParamHasEvent;
}

/******************************************************************************/
bool LiberaMapping::HasAverage() const
{
    return GetExtraParam() & ExtraParamHasAverage;
}

/******************************************************************************/
bool LiberaMapping::MapToDouble() const
{
  return GetExtraParam() & ExtraParamMapToDouble;
}

/******************************************************************************/
bool LiberaMapping::HasStartStopIndex() const
{
    return GetExtraParam() & ExtraParamHasStartStopIndex;
}

/******************************************************************************/
void LiberaMapping::GetStartStopIndex(std::string &a_start, std::string &a_stop) const
{
    istd_ASSERT_MSG(HasStartStopIndex(), "flag for SumStartStopIdx is not set");

    size_t  ExtraParamStartIndex = 1;
    if(IsMinMaxEnabled())
        ExtraParamStartIndex += 2;
    if(HasDefaultValue())
        ExtraParamStartIndex += 1;

    istd_ASSERT_MSG(m_extraparams.size() >= (ExtraParamStartIndex + 2),
                    "arguments for SumStartStopIdx are missing");
    a_start = m_extraparams[ExtraParamStartIndex];
    a_stop = m_extraparams[ExtraParamStartIndex+1];
}

/******************************************************************************/
bool LiberaMapping::IsSum() const
{
    return GetExtraParam() & ExtraParamIsSum;
}

/******************************************************************************/
bool LiberaMapping::IsSumRow() const
{
  return GetExtraParam() & ExtraParamIsSumRow;
}

/******************************************************************************/
bool LiberaMapping::RemoveOffset() const
{
  return GetExtraParam() & ExtraParamRemoveOffset;
}

/******************************************************************************/
bool LiberaMapping::IsExpertView() const
{
    return GetExtraParam() & ExtraParamExpertViewFlag;
}

/******************************************************************************/
bool LiberaMapping::IsMemorisedOnServer() const
{
    return GetExtraParam() & ExtraParamMemorised;
}

