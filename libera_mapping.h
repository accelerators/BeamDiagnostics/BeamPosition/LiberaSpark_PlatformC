/*
 * Copyright (c) 2014 Instrumentation Technologies
 * All Rights Reserved.
 *
 * $Id: libera_mapping.h 22552 2015-10-27 14:54:34Z luka.rahne $
 */

#pragma once

#include <string>
#include <vector>

#include <mci/mci.h>
#include <isig/declarations.h>
#include <isig/signal_traits.h>

#include <tango.h>

/**
 * Type mapping traits for node value types.
 */
template<mci::NodeValType_e NodeValType>
struct NodeValTraits;

template<>
struct NodeValTraits<mci::eNvBool> {
    typedef bool LType;
    typedef Tango::DevBoolean TType;
    typedef Tango::DevBoolean TCType;
    enum TTypeEnum {TTypeEnumVal=Tango::DEV_BOOLEAN};
    static const char* format;
};

template<>
struct NodeValTraits<mci::eNvLong> {
    typedef int32_t LType;
    typedef Tango::DevLong TType;
    typedef Tango::DevLong TCType;
    enum TTypeEnum {TTypeEnumVal=Tango::DEV_LONG};
    static const char* format;
};

template<>
struct NodeValTraits<mci::eNvULong> {
    typedef uint32_t LType;
    typedef Tango::DevULong TType;
    typedef Tango::DevULong TCType;
    enum TTypeEnum {TTypeEnumVal=Tango::DEV_ULONG};
    static const char* format;
};

template<>
struct NodeValTraits<mci::eNvLongLong> {
    typedef int64_t LType;
    typedef Tango::DevLong64 TType;
    typedef Tango::DevLong64 TCType;
    enum TTypeEnum {TTypeEnumVal=Tango::DEV_LONG64};
    static const char* format;
};

template<>
struct NodeValTraits<mci::eNvULongLong> {
    typedef uint64_t LType;
    typedef Tango::DevULong64 TType;
    typedef Tango::DevULong64 TCType;
    enum TTypeEnum {TTypeEnumVal=Tango::DEV_ULONG64};
    static const char* format;
};

template<>
struct NodeValTraits<mci::eNvDouble> {
    typedef double LType;
    typedef Tango::DevDouble TType;
    typedef Tango::DevDouble TCType;
    enum TTypeEnum {TTypeEnumVal=Tango::DEV_DOUBLE};
    static const char* format;
};

template<>
struct NodeValTraits<mci::eNvFloat> {
    typedef float LType;
    typedef Tango::DevFloat TType;
    typedef Tango::DevFloat TCType;
    enum TTypeEnum {TTypeEnumVal=Tango::DEV_FLOAT};
    static const char* format;
};

template<>
struct NodeValTraits<mci::eNvString> {
    typedef std::string LType;
    typedef Tango::DevString TType;
    typedef Tango::ConstDevString TCType;
    enum TTypeEnum {TTypeEnumVal=Tango::DEV_STRING};
    static const char* format;
};

template<>
struct NodeValTraits<mci::eNvEnumeration> {
    typedef int64_t LType;
    typedef Tango::DevLong64 TType;
    typedef Tango::DevLong64 TCType;
    enum TTypeEnum {TTypeEnumVal=Tango::DEV_LONG64};
    static const char* format;
};

/**
 * Type mapping traits for signal atom types.
 */
template<isig::AtomType_e SignalAtomType>
struct SignalAtomTraits;

template<>
struct SignalAtomTraits<isig::eTypeDouble> {
    typedef double LType;
    typedef isig::SignalTraitsVarDouble Traits;
    typedef Tango::DevDouble TType;
    enum TTypeEnum {TTypeEnumVal=Tango::DEV_DOUBLE};
};

template<>
struct SignalAtomTraits<isig::eTypeFloat> {
    typedef float LType;
    typedef isig::SignalTraitsVarFloat Traits;
    typedef Tango::DevFloat TType;
    enum TTypeEnum {TTypeEnumVal=Tango::DEV_FLOAT};
};

template<>
struct SignalAtomTraits<isig::eTypeUInt64> {
    typedef int64_t LType;
    typedef isig::SignalTraitsVarUint64 Traits;
    typedef Tango::DevULong64 TType;
    enum TTypeEnum {TTypeEnumVal=Tango::DEV_ULONG64};
};

template<>
struct SignalAtomTraits<isig::eTypeInt64> {
    typedef uint64_t LType;
    typedef isig::SignalTraitsVarInt64 Traits;
    typedef Tango::DevLong64 TType;
    enum TTypeEnum {TTypeEnumVal=Tango::DEV_LONG64};
};

template<>
struct SignalAtomTraits<isig::eTypeUInt32> {
    typedef uint32_t LType;
    typedef isig::SignalTraitsVarUint32 Traits;
    typedef Tango::DevULong TType;
    enum TTypeEnum {TTypeEnumVal=Tango::DEV_ULONG};
};

template<>
struct SignalAtomTraits<isig::eTypeInt32> {
    typedef int32_t LType;
    typedef isig::SignalTraitsVarInt32 Traits;
    typedef Tango::DevLong TType;
    enum TTypeEnum {TTypeEnumVal=Tango::DEV_LONG};
};

template<>
struct SignalAtomTraits<isig::eTypeUInt16> {
    typedef uint16_t LType;
    typedef isig::SignalTraitsVarUint16 Traits;
    typedef Tango::DevUShort TType;
    enum TTypeEnum {TTypeEnumVal=Tango::DEV_USHORT};
};

template<>
struct SignalAtomTraits<isig::eTypeInt16> {
    typedef int16_t LType;
    typedef isig::SignalTraitsVarInt16 Traits;
    typedef Tango::DevShort TType;
    enum TTypeEnum {TTypeEnumVal=Tango::DEV_SHORT};
};

template<>
struct SignalAtomTraits<isig::eTypeUInt8> {
    typedef uint8_t LType;
    typedef isig::SignalTraitsVarUint8 Traits;
    typedef Tango::DevUChar TType;
    enum TTypeEnum {TTypeEnumVal=Tango::DEV_UCHAR};
};

template<>
struct SignalAtomTraits<isig::eTypeInt8> {
    typedef int8_t LType;
    typedef isig::SignalTraitsVarInt8 Traits;
    typedef Tango::DevUChar TType;
    enum TTypeEnum {TTypeEnumVal=Tango::DEV_UCHAR};
};

enum Control_e {
    eCUndefined = 0,
    eCEnable = 1,
    eCMode   = 2,
    eCOffset = 3,
    eCLength = 4,
    eCMT     = 5,
    eCAvgEnable = 6,
    eCAvgLength = 7
};

class LiberaMapping {
public:
    LiberaMapping(const std::string& a_line,
        const std::string& a_ireg_prefix, const std::string& a_tango_prefix,
        const bool a_platform, const bool a_instance);
    const char* IregName() const;
    const char* TangoName() const;
    bool IsReadable() const;
    bool IsWritable() const;
    Tango::AttrWriteType TangoAccess() const;
    bool IsArray() const;
    bool IsSignal() const;
    bool IsControl() const;
    Control_e ControlType() const;
    mci::NodeValType_e NodeValType() const;
    isig::AtomType_e SignalAtomType() const;
    bool IsPlatform() const;
    bool IsInstance() const;
    void SetListen();
    bool IsListen() const;
    std::string FullName(const std::string& a_instance) const;
    size_t GetSize() const;
    bool IsScalar() const;
    bool IsMinMaxEnabled() const;
    void GetMinMax(std::string &a_min, std::string &a_max) const;
    void GetDefaultValue(std::string &defaultValue) const;
    bool HasDefaultValue() const;
    bool HasEvent() const;
    bool IsSum() const;
    bool IsSumRow() const;
    bool RemoveOffset() const;
    bool HasStartStopIndex() const;
    bool HasAverage() const;
    void GetStartStopIndex(std::string &a_start, std::string &a_stop) const;
    bool IsExpertView() const;
    bool IsMemorisedOnServer() const;
    bool MapToDouble() const;

private:
    std::string m_ireg_name;
    std::string m_tango_name;
    mci::Flags_e m_access;
    int32_t m_data_type;
    bool m_platform;
    bool m_instance;
    bool m_listen;
    size_t m_size;
    Control_e m_control;
    std::vector<std::string> m_extraparams;
    uint32_t GetExtraParam() const;
};

// Workaround STD committee vector<bool> specialization mistake.
template<typename T>
struct std_vector {
    typedef std::vector<T> Type;
};

template<>
struct std_vector<bool> {
    typedef std::vector<unsigned char> Type;
};

