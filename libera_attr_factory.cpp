/*
 * Copyright (c) 2014 Instrumentation Technologies
 * All Rights Reserved.
 *
 * $Id: libera_attr_factory.cpp 22552 2015-10-27 14:54:34Z luka.rahne $
 */

#include <istd/log.h>
#include <mci/mci.h>

#include "libera_attr_factory.h"

#include "libera_attr.h"
#include "libera_array_attr.h"
#include "libera_signal_attr.h"
#include "libera_control.h"
#include "libera_config.h"


static void SetTangoAttributesFromConfig(Tango::UserDefaultAttrProp &prop,
                                         Tango::Attr *p,
                                         const LiberaMapping &a_mapping)
{
    if (a_mapping.IsMinMaxEnabled()) {
        std::string max, min;
        a_mapping.GetMinMax(min, max);
        prop.set_max_value(max.c_str());
        prop.set_min_value(min.c_str());
    }
    if (a_mapping.IsExpertView()) {
        p->set_disp_level(Tango::EXPERT);
    }
    else {
        p->set_disp_level(Tango::OPERATOR);
    }
    if (a_mapping.IsMemorisedOnServer()) {
        p->set_memorized();
        p->set_memorized_init(true);
    }
    p->set_default_properties(prop);
}


/**
 * Helper function to work around constructor address restriction.
 */
template<mci::NodeValType_e NodeValType>
Tango::Attr* LiberaAttrFactory::Creator(const LiberaMapping& a_mapping)
{
    istd_FTRC();
    Tango::Attr* p = NULL;
    try {

        if (a_mapping.IsArray()) {
          cout << "LiberaAttrFactory(" << a_mapping.IregName() << "): Create LiberaAttrArray: " << a_mapping.TangoName() << endl;
          p = new LiberaArrayAttr<NodeValType>(a_mapping);
        } else {
          cout << "LiberaAttrFactory(" << a_mapping.IregName() << "): Create LiberaAttr: " << a_mapping.TangoName() << endl;
          p = new LiberaAttr<NodeValType>(a_mapping);
        }

        Tango::UserDefaultAttrProp prop;
        prop.set_format(NodeValTraits<NodeValType>::format);
        SetTangoAttributesFromConfig(prop,p,a_mapping);

    } catch (Tango::DevFailed& e) {
        istd_LOGe("Tango exception while creating attribute for: "
            << a_mapping.IregName() << e.errors[0].desc);
    }
    return p;
}

/**
 * Helper function to work around constructor address restriction.
 */
template<isig::AtomType_e SignalAtomType>
Tango::Attr* LiberaAttrFactory::Creator(const LiberaMapping& a_mapping)
{
    istd_FTRC();
    Tango::Attr* p = NULL;
    try {
        if(!a_mapping.IsScalar()) {
          cout << "LiberaAttrFactory(" << a_mapping.IregName() << "): Create LiberaSignalAttr(" << SignalAtomTraits<SignalAtomType>::TTypeEnumVal <<  ",Spectrum): " << a_mapping.TangoName() << endl;
          p = new LiberaSignalAttr<SignalAtomType,Tango::SpectrumAttr>(a_mapping);
        } else {
          cout << "LiberaAttrFactory(" << a_mapping.IregName() << "): Create LiberaSignalAttr(" << SignalAtomTraits<SignalAtomType>::TTypeEnumVal <<  ",Scalar): " << a_mapping.TangoName() << endl;
          p = new LiberaSignalAttr<SignalAtomType,Tango::Attr>(a_mapping);
        }
        Tango::UserDefaultAttrProp prop;
        SetTangoAttributesFromConfig(prop,p,a_mapping);
    }
    catch (Tango::DevFailed& e) {
        istd_LOGe("Tango exception while creating attribute for: "
            << a_mapping.IregName());
    }
    return p;
}

/**
 * Initialization of NodeValType mappings.
 */
LiberaAttrFactory::NodeValTypeMapping::NodeValTypeMapping()
{
    istd_FTRC();

    insert(NodeValTypePair<mci::eNvBool>());
    insert(NodeValTypePair<mci::eNvLong>());
    insert(NodeValTypePair<mci::eNvULong>());
    insert(NodeValTypePair<mci::eNvLongLong>());
    insert(NodeValTypePair<mci::eNvULongLong>());
    insert(NodeValTypePair<mci::eNvDouble>());
    insert(NodeValTypePair<mci::eNvFloat>());
    insert(NodeValTypePair<mci::eNvString>());
    insert(NodeValTypePair<mci::eNvEnumeration>());
}

/**
 * Initialization of SignalAtomType mappings.
 */
LiberaAttrFactory::SignalAtomTypeMapping::SignalAtomTypeMapping()
{
    istd_FTRC();

    insert(SignalAtomTypePair<isig::eTypeDouble>());
    insert(SignalAtomTypePair<isig::eTypeFloat>());
    insert(SignalAtomTypePair<isig::eTypeUInt64>());
    insert(SignalAtomTypePair<isig::eTypeInt64>());
    insert(SignalAtomTypePair<isig::eTypeUInt32>());
    insert(SignalAtomTypePair<isig::eTypeInt32>());
    insert(SignalAtomTypePair<isig::eTypeUInt16>());
    insert(SignalAtomTypePair<isig::eTypeInt16>());
    insert(SignalAtomTypePair<isig::eTypeUInt8>());
    insert(SignalAtomTypePair<isig::eTypeInt8>());
}

/**
 * Class Factory function uses NodeValType mapping for LiberaAttr creation.
 */
Tango::Attr* LiberaAttrFactory::Create(const LiberaMapping& a_mapping)
{
    istd_FTRC();

    static NodeValTypeMapping parameterCreator;
    static SignalAtomTypeMapping signalCreator;

    if (a_mapping.IsSignal()) {

        if (signalCreator.find(a_mapping.SignalAtomType()) == signalCreator.end()) {
            istd_LOGe("Unsupported ireg node value type (" << a_mapping.SignalAtomType()
                << ") in configuration!");
            return NULL;
        }
        if( a_mapping.MapToDouble() ) {
          return signalCreator[isig::eTypeDouble](a_mapping);
        } else {
          return signalCreator[a_mapping.SignalAtomType()](a_mapping);
        }
    }

    if (a_mapping.IsControl()) {
        // add signal controls
        Tango::Attr* p = NULL;
        cout << "LiberaAttrFactory(" << a_mapping.IregName() << "): Create Control: " << a_mapping.TangoName() << endl;
        switch (a_mapping.ControlType()) {
        case eCEnable:
            p = new LiberaControlAttr<eCEnable>(a_mapping);
            break;
        case eCMode:
            p =  new LiberaControlAttr<eCMode>(a_mapping);
            break;
        case eCOffset:
            p =  new LiberaControlAttr<eCOffset>(a_mapping);
            break;
        case eCLength:
            p =  new LiberaControlAttr<eCLength>(a_mapping);
            break;
        case eCMT:
            p =  new LiberaControlAttr<eCMT>(a_mapping);
            break;
          case eCAvgEnable:
            p =  new LiberaControlAttr<eCAvgEnable>(a_mapping);
            break;
          case eCAvgLength:
            p =  new LiberaControlAttr<eCAvgLength>(a_mapping);
            break;
        default:
            istd_LOGe("Unsupported control type (" << a_mapping.ControlType()
                << ") in configuration!");
            return NULL;
        }
        Tango::UserDefaultAttrProp prop;
        SetTangoAttributesFromConfig(prop,p,a_mapping);
        return p;
    }

    if (parameterCreator.find(a_mapping.NodeValType()) == parameterCreator.end()) {
        istd_LOGe("Unsupported ireg node value type (" << a_mapping.NodeValType()
            << ") in configuration!");
        return NULL;
    }

    return parameterCreator[a_mapping.NodeValType()](a_mapping);
}
