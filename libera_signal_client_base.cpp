/*
 * Copyright (c) 2014 Instrumentation Technologies
 * All Rights Reserved.
 *
 * $Id: libera_signal_client_base.cpp 21027 2014-09-09 12:33:18Z tomaz.beltram $
 */

#include <istd/trace.h>
#include <mci/mci_util.h>

#include "libera_signal_client_base.h"

LiberaSignalClientBase::LiberaSignalClientBase(const LiberaMapping& a_lm)
  : m_columns(0),m_computeSum(false),m_computeSumRow(false),m_hasStartStopIndex(false),m_calculationAlgorithm(DOS_ALGORITHM),
    hOrder(0),vOrder(0),hOffset(0),vOffset(0),m_removeOffset(false),
    m_lm(a_lm),isBPM(false)
{
  // Rotation matrix
  r11 = 1.0; r12 = 0.0;
  r21 = 0.0; r22 = 1.0;
  istd_FTRC();
}

LiberaSignalClientBase::~LiberaSignalClientBase()
{
    istd_FTRC();
}

void LiberaSignalClientBase::AddAttr(const std::string& a_name,bool hasEvent,bool isSum,bool isSumRow,bool hasStartStopIndex,bool hasAverage,bool mapToDouble,bool removeOffset)
{
    istd_FTRC();
    _ATTR_T att;
    if(!isSum && !isSumRow) {
      att.column = m_columns++;
    } else if(isSum) {
      att.column = -1; // -1 for Sum
    } else if(isSumRow) {
      att.column = -2; // -2 for SumRow
    }
    att.hasEvent = hasEvent;
    att.isSum = isSum;
    att.isSumRow = isSumRow;
    att.hasAverage = hasAverage;
    m_names[a_name] = att;
    // Ask to compute sum
    if(isSum) m_computeSum = true;
    if(isSumRow) m_computeSumRow = true;
    if(hasStartStopIndex) m_hasStartStopIndex = true;
    if(removeOffset) m_removeOffset = true;
}

void LiberaSignalClientBase::SetHOffset(double offset) {
  hOffset = offset;
}

void LiberaSignalClientBase::SetVOffset(double offset) {
  vOffset = offset;
}

void LiberaSignalClientBase::SetBPM() {
  isBPM = true;
}

bool LiberaSignalClientBase::IsBPM() {
  return isBPM;
}

bool LiberaSignalClientBase::SetBPMPolynomial(vector<double> &hCoefs,vector<double> &vCoefs,double kA,double kB,double kC,double kD) {

  hPolyCoefs = hCoefs;
  vPolyCoefs = vCoefs;

  this->kA = kA;
  this->kB = kB;
  this->kC = kC;
  this->kD = kD;

  // Compute polynomial orders n (v and h orders are n+1)
  // i[0] * X^0Y^0 + i[1] * X^0Y^1 + ... + i[n] * X0Y^n + i[n+1] * X^1Y^0 + ... + i[2n] * X^1Y^n +  i[2n+1] * X^2Y^0 + ...

  double delta;
  delta = 1 + 4.0 * 2.0 * (double)hPolyCoefs.size();
  hOrder = (unsigned int)((sqrt(delta)-1) / 2.0);
  delta = 1 + 4.0 * 2.0 * (double)vPolyCoefs.size();
  vOrder = (unsigned int)((sqrt(delta)-1) / 2.0);

  // Check order

  if( hOrder > MAX_ORDER ) {
    cerr << "Error: Polynomial order to large for h" << endl;
    return false;
  }
  if( vOrder > MAX_ORDER ) {
    cerr << "Error: Polynomial order to large for h" << endl;
    return false;
  }

  if( (((hOrder-1)*hOrder)/2 + hOrder ) != hPolyCoefs.size() ) {
    cerr << "Error: Wrong coefficient number for h polynomial" << endl;
    return false;
  }
  if( (((vOrder-1)*vOrder)/2 + vOrder ) != vPolyCoefs.size() ) {
    cerr << "Error: Wrong coefficient number for v polynomial" << endl;
    return false;
  }

  return true;

}

void LiberaSignalClientBase::SetTiltAngle(double angle) {

  r11 = cos(angle);r12 = -sin(angle);
  r21 = sin(angle);r22 = cos(angle);

}

void LiberaSignalClientBase::SetCalculationAlgorithm(int algo) {
  m_calculationAlgorithm = algo;
}

bool LiberaSignalClientBase::GetCalculationAlgorithm() {
   return m_calculationAlgorithm;
}

bool LiberaSignalClientBase::hasStartStopIndex() {
    return m_hasStartStopIndex;
}

void LiberaSignalClientBase::SetStartIndex(int startIdx) {
    m_startIdx = startIdx;
}

void LiberaSignalClientBase::SetStopIndex(int stopIdx) {
    m_stopIdx = stopIdx;
}

int LiberaSignalClientBase::GetStartIndex() {
  return m_startIdx;
}

int LiberaSignalClientBase::GetStopIndex() {
  return m_stopIdx;
}

bool LiberaSignalClientBase::ComputeSum() {
    return m_computeSum;
}

bool LiberaSignalClientBase::ComputeSumRow() {
  return m_computeSumRow;
}

bool LiberaSignalClientBase::RemoveOffset() {
  return m_removeOffset;
}

const LiberaMapping& LiberaSignalClientBase::LM()
{
    istd_FTRC();
    return m_lm;
}

void LiberaSignalClientBase::Enable(bool a_enable)
{
    istd_FTRC();
    if (a_enable) {
        Initialize();
    } else {
        Terminate();
    }
}

const std::map<std::string, _ATTR_T>& LiberaSignalClientBase::GetNames()
{
    return m_names;
}

int LiberaSignalClientBase::NumColumns()
{
    return m_columns;
}

void LiberaSignalClientBase::Read(const std::string& a_name,
    Tango::Attribute &a_attr)
{
    istd_FTRC();
    auto i = m_names.find(a_name);
    if (i != m_names.end()) {
        istd_TRC(istd::eTrcLow,"Read signal" << a_name);
        DoRead(i->second.column, a_attr);
    }
    // column name not found
}
