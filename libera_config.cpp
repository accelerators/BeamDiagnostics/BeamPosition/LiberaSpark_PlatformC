/*
 * Copyright (c) 2014 Instrumentation Technologies
 * All Rights Reserved.
 *
 * $Id: libera_config.cpp 20924 2014-08-14 11:52:46Z tomaz.beltram $
 */

#include <fstream>

#include <istd/log.h>

#include "libera_config.h"

const std::string LiberaConfig::m_cfg_dir_name("/opt/libera/sbin/cfg/tango/");
const std::string LiberaConfig::m_cfg_file_name("ds-top");
const std::string LiberaConfig::m_cfg_pm_name("pm-top");
const std::string LiberaConfig::m_cfg_bpm_ireg_name("bpm-ireg");
const std::string LiberaConfig::m_cfg_features_name("ds-features");

const std::string c_instanceStr("$INSTANCE");

/**
 * Load mappings from config file.
 */
LiberaConfig::LiberaConfig()
{
    istd_FTRC();
}

void LiberaConfig::Init()
{
    istd_FTRC();
    hasZyncTemp = false;
    hasAGC = false;
    hasSAIncoherencyDrift = false;
    ReadTopFile(m_cfg_dir_name + m_cfg_file_name, false);
    ReadTopFile(m_cfg_dir_name + m_cfg_pm_name, true);
    ReadBpmFile(m_cfg_dir_name + m_cfg_bpm_ireg_name);
    ReadFeaturesFile(m_cfg_dir_name + m_cfg_features_name);
}

vector<std::string> LiberaConfig::GetBPMNodes() {
    return bpmNodes;
}


bool LiberaConfig::HasBpm() {
  return bpmNodes.size()>0;
}

void LiberaConfig::ReadBpmFile(const std::string& a_fname)
{
    std::ifstream cfg_file(a_fname);
    std::string line;

    istd_TRC(istd::eTrcLow, "Read BPM file: " << a_fname);

    if (cfg_file.is_open()) {

        while (getline (cfg_file, line)) {
            bpmNodes.push_back(line);
        }

    } else {
        istd_LOGe("Unable to open bpm file: " << a_fname);
    }
}

void LiberaConfig::ReadFeaturesFile(const std::string& a_fname)
{
    std::ifstream cfg_file(a_fname);
    std::string line;

    istd_TRC(istd::eTrcLow, "Read features file: " << a_fname);

    if (cfg_file.is_open()) {

        while (getline (cfg_file, line)) {
            string field;
            std::stringstream sstr(line);
            sstr >> field;
            if(field.compare("ZyncTemp")==0) hasZyncTemp = true;
            if(field.compare("AGC")==0) hasAGC = true;
            if(field.compare("SAIncoherencyDrift")==0) hasSAIncoherencyDrift = true;
        }
        cfg_file.close();

    } else {
        istd_LOGe("Unable to open features file: " << a_fname);
    }
}

void LiberaConfig::ReadTopFile(const std::string& a_fname, const bool a_platform)
{
    std::ifstream cfg_file(a_fname);
    std::string line;

    istd_TRC(istd::eTrcLow, "Read top mapping file: " << a_fname);

    if (cfg_file.is_open()) {

        // Read first level mapping and detect $INSTANCE marker.
        // <file_name> <ireg_prefix> <tango_prefix>
        while (getline (cfg_file, line)) {
            std::stringstream sstr(line);
            std::string fname;
            std::string ireg_prefix;
            std::string tango_prefix;
            bool instance = false;

            sstr >> fname;
            sstr.ignore();

            sstr >> ireg_prefix;
            sstr.ignore();

            sstr >> tango_prefix;

            fname = m_cfg_dir_name + fname;

            istd_TRC(istd::eTrcLow, "Map file " << fname << ", "
                << ireg_prefix << ", " << tango_prefix);

            if (ireg_prefix == c_instanceStr) {
                ireg_prefix = "";
                instance = true;
            }
            ReadFile(fname, ireg_prefix, tango_prefix, a_platform, instance);
        }
        std::string fname = a_fname + "-listen";
        ReadListenFile(fname, a_platform);
    }
    else {
        istd_LOGe("Unable to open top mapping file: " << a_fname);
    }
}

void LiberaConfig::ReadFile(const std::string& a_fname,
    const std::string& a_ireg_prefix, const std::string& a_tango_prefix,
    const bool a_platform, const bool a_instance)
{
    istd_FTRC();

    std::ifstream cfg_file(a_fname);
    std::string line;

    istd_TRC(istd::eTrcLow, "Read mapping file: " << a_fname);

    if (cfg_file.is_open()) {
        while (getline (cfg_file, line)) {
            istd_TRC(istd::eTrcDetail, "Read mapping: " << line);
            //TODO: check if valid line
            push_back(
                LiberaMapping(line, a_ireg_prefix, a_tango_prefix,
                    a_platform, a_instance));
        }
        cfg_file.close();
    }

    else {
        istd_LOGe("Unable to open mapping file: " << a_fname);
    }
}

void LiberaConfig::ReadListenFile(const std::string& a_fname,
    const bool a_platform)
{
    std::ifstream cfg_file(a_fname);
    std::string line;

    istd_TRC(istd::eTrcLow, "Read listen file: " << a_fname);

    if (cfg_file.is_open()) {
        while (getline (cfg_file, line)) {
            bool instance = false;
            if (line.compare(0, c_instanceStr.length(), c_instanceStr) == 0) {
                line = line.substr(c_instanceStr.length());
                instance = true;
            }
            istd_TRC(istd::eTrcLow, "Listen node: " << line << ", " << instance);
            for (auto i = begin(); i != end(); ++i) {
                if ((i->IsInstance() == instance) && (i->IregName() == line)) {
                    i->SetListen();
                    istd_TRC(istd::eTrcLow, "Listen attr: " << i->TangoName());
                }
            }
        }
    }
    else {
        istd_LOGe("Unable to open listen file: " << a_fname);
    }
}
