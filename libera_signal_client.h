/*
 * Copyright (c) 2014 Instrumentation Technologies
 * All Rights Reserved.
 *
 * $Id: libera_signal_client.h 21345 2015-01-07 10:01:47Z tomaz.beltram $
 */

#pragma once

#include <isig/remote_stream.h>
#include <isig/data_on_demand_remote_source.h>

// ignore warning turned on in included inet/corba_util.h
#pragma GCC diagnostic ignored "-Wold-style-cast"

#include "libera_mapping.h"
#include "libera_signal_client_base.h"

template<isig::AtomType_e SignalAtomType>
class LiberaSignalClient : public LiberaSignalClientBase
{
public:
    typedef typename SignalAtomTraits<SignalAtomType>::Traits Traits;
    typedef isig::Array<Traits>                    ClientBuffer;
    typedef isig::RemoteStream<Traits>             RStream;
    typedef isig::DataOnDemandRemoteSource<Traits> RSource;
    typedef typename RStream::Client               StreamClient;
    typedef typename RSource::Client               DodClient;
    typedef typename SignalAtomTraits<SignalAtomType>::TType  TangoType;
    typedef typename std_vector<TangoType>::Type   TangoTypeVec;

    LiberaSignalClient(Tango::DeviceImpl* a_dev, const size_t a_length, const LiberaMapping& a_lm);
    ~LiberaSignalClient();
    void operator ()();
    void Create(mci::Node &a_node,uint32_t refresh_period);
    virtual int32_t GetAccessType();

    // Control signals
    virtual bool IsEnabled();
    virtual void SetOffset(int32_t a_offset);
    virtual int32_t GetOffset();
    virtual void SetLength(size_t a_length);
    virtual size_t GetLength();
    virtual void SetMode(int32_t a_mode);
    virtual int32_t GetMode();
    virtual uint64_t GetMT();
    virtual bool IsAveraging();
    virtual bool SetAveraging(bool averaging);
    virtual size_t GetAveragingLength();
    virtual void SetAveragingLength(size_t length);

private:
    void OpenStream();
    void OpenDoD();
    virtual void Initialize();
    virtual void Terminate();
    virtual void Update();
    void Transpose();
    virtual void DoRead(int a_column, Tango::Attribute &a_attr);

    Tango::DeviceImpl* m_dev;
    size_t             m_length;
    int32_t            m_offset;
    uint64_t           m_mt;
    uint32_t           m_refreshPeriod;
    bool               m_enabled;
    bool               m_idle;
    bool               m_updated;
    bool               m_averaging;
    size_t             m_averaging_length;
    size_t             m_averaging_number;
    isig::AccessMode_e m_mode;
    isig::SignalSourceSharedPtr   m_signal;
    std::shared_ptr<RStream>      m_stream;
    std::shared_ptr<StreamClient> m_streamClient;
    std::shared_ptr<RSource>      m_dod;
    std::shared_ptr<DodClient>    m_dodClient;
    std::mutex                    m_data_x; // protects buffer access
    std::shared_ptr<ClientBuffer> m_buf;
    std::vector<TangoTypeVec>     m_tvals;
    std::vector<TangoTypeVec>     m_tlastvals; // For averaging (only eAccessStream)
    TangoTypeVec                  m_tsum;
    TangoTypeVec                  m_tsumrow;
    bool m_running;
    std::thread m_thread;
};
