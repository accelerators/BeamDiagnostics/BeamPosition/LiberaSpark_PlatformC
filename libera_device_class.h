/*
 * Copyright (c) 2014 Instrumentation Technologies
 * All Rights Reserved.
 *
 * $Id: libera_device_class.h 20729 2014-06-26 12:00:56Z tomaz.beltram $
 */

#pragma once

#include <tango.h>

#include "libera_config.h"
#include "libera_device_server.h"

/**
 *  Libera device class implemented as singletone.
 */

class LiberaDeviceClass : public Tango::DeviceClass {
public:
    static LiberaDeviceClass * Instance();

private:
    void command_factory();

    void attribute_factory(vector<Tango::Attr *> &attributes);

    LiberaDeviceClass(std::string& name);

    ~LiberaDeviceClass();

    void device_factory(const Tango::DevVarStringArray *devices);

    static LiberaDeviceClass* m_instance;

    LiberaConfig m_cfg;

};



// Zync temperature attribute
class ZyncTempAttrib: public Tango::Attr {

public:

    double A0;
    double A1;
    double Temp;

    ZyncTempAttrib():Attr("ZyncTemp",Tango::DEV_DOUBLE, Tango::READ) {
      // Initialise scaling coeficient
      A0 = readFromFile("/sys/bus/iio/devices/iio:device0/in_temp0_offset");
      A1 = readFromFile("/sys/bus/iio/devices/iio:device0/in_temp0_scale");
    };

    ~ZyncTempAttrib() {};

    virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att) {
      double r = readFromFile("/sys/bus/iio/devices/iio:device0/in_temp0_raw");
      Temp = ((r+A0)*A1)/1000.0;
      att.set_value(&Temp);
    }

    virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
    {return true;}

    double readFromFile(string fileName) {

      double v = 0.0;
      FILE *f = fopen(fileName.c_str(),"r");
      if(f!=NULL) {
        fscanf(f,"%lf",&v);
        fclose(f);
      }
      return v;

    }

};

// StartIndex attribute (used for sum attribute)
class StartIndexAttrib: public Tango::Attr {

public:

    string iregName;
    Tango::DevLong readValue = 0;

    StartIndexAttrib(string attName,string iregName):Attr(attName.c_str(),Tango::DEV_LONG, Tango::READ_WRITE) {
      this->iregName = iregName;
    };

    ~StartIndexAttrib() {};

    virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att) {
      readValue = (static_cast<LiberaDeviceServer *>(dev))->GetSignalClient(iregName)->GetStartIndex();
      att.set_value(&readValue);
    }

    virtual void write(Tango::DeviceImpl *dev,Tango::WAttribute &att) {
      Tango::DevLong w_val;
      att.get_write_value(w_val);
      (static_cast<LiberaDeviceServer *>(dev))->GetSignalClient(iregName)->SetStartIndex(w_val);
    }

    virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
    {return true;}

};

// StopIndex attribute (used for sum attribute)
class StopIndexAttrib: public Tango::Attr {

public:

    string iregName;
    Tango::DevLong readValue = 0;

    StopIndexAttrib(string attName,string iregName):Attr(attName.c_str(),Tango::DEV_LONG, Tango::READ_WRITE) {
      this->iregName = iregName;
    };

    ~StopIndexAttrib() {};

    virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att) {
      readValue = (static_cast<LiberaDeviceServer *>(dev))->GetSignalClient(iregName)->GetStopIndex();
      att.set_value(&readValue);
    }

    virtual void write(Tango::DeviceImpl *dev,Tango::WAttribute &att) {
      Tango::DevLong w_val;
      att.get_write_value(w_val);
      (static_cast<LiberaDeviceServer *>(dev))->GetSignalClient(iregName)->SetStopIndex(w_val);
    }

    virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
    {return true;}

};

enum _CalcAlgorithmEnum {
    _DOS,
    _POLYNOMIAL,
} ;
typedef _CalcAlgorithmEnum CalcAlgorithmEnum;

// CalcAlgorithm attribute (used for BPM calculation algorithm)
class CalcAlgorithmAttrib: public Tango::Attr {

public:

    CalcAlgorithmEnum calcMode = _DOS;

    CalcAlgorithmAttrib():Attr("CalcAlgorithm", Tango::DEV_ENUM, Tango::READ_WRITE) {};
    ~CalcAlgorithmAttrib() {};

    virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
    {
      att.set_value(&calcMode);
    }

    virtual void write(Tango::DeviceImpl *dev,Tango::WAttribute &att)
    {
      att.get_write_value(calcMode);
      (static_cast<LiberaDeviceServer *>(dev))->SetCalculationAlgorithm((Tango::DevShort)calcMode);
    }

    virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
    {return true;}

    virtual bool same_type(const type_info &in_type) {return typeid(CalcAlgorithmEnum) == in_type;}
    virtual string get_enum_type() {return string("CalcAlgorithmEnum");}

};

// AGCEnable attribute (used for AGC control)
class AGCEnableAttrib: public Tango::Attr {

public:

    Tango::DevBoolean agcEnable = false;

    AGCEnableAttrib():Attr("AGC_Enable", Tango::DEV_BOOLEAN, Tango::READ_WRITE) {};
    ~AGCEnableAttrib() {};

    virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
    {
      att.set_value(&agcEnable);
    }

    virtual void write(Tango::DeviceImpl *dev,Tango::WAttribute &att)
    {
      att.get_write_value(agcEnable);
      (static_cast<LiberaDeviceServer *>(dev))->SetAGCEnable(agcEnable);
    }

    virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
    {return true;}
};

// SA_Incoherency_Ref (Incoherency reference)
class SAIncoherencyRefAttrib: public Tango::Attr {

public:

    Tango::DevDouble incoherencyRef = 0;

    SAIncoherencyRefAttrib():Attr("SA_Incoherency_Ref", Tango::DEV_DOUBLE, Tango::READ_WRITE) {};
    ~SAIncoherencyRefAttrib() {};

    virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
    {
      incoherencyRef = (static_cast<LiberaDeviceServer *>(dev))->GetIncoherencyRef();
      att.set_value(&incoherencyRef);
    }

    virtual void write(Tango::DeviceImpl *dev,Tango::WAttribute &att)
    {
      Tango::DevDouble setValue;
      att.get_write_value(setValue);
      (static_cast<LiberaDeviceServer *>(dev))->SetIncoherencyRef(setValue);
    }

    virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
    {return true;}
};

// SA_Incoherency_Drift (Incoherency drift)
class SAIncoherencyDriftAttrib: public Tango::Attr {

public:

    Tango::DevDouble incoherencyDrift = 0;

    SAIncoherencyDriftAttrib():Attr("SA_Incoherency_Drift", Tango::DEV_DOUBLE, Tango::READ) {};
    ~SAIncoherencyDriftAttrib() {};

    virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
    {
      incoherencyDrift = (static_cast<LiberaDeviceServer *>(dev))->GetIncoherencyDrift();
      att.set_value(&incoherencyDrift);
    }

    virtual void write(Tango::DeviceImpl *dev,Tango::WAttribute &att)
    {
    }

    virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
    {return true;}
};

// HOffset
class HOffsetAttrib: public Tango::Attr {

public:

    Tango::DevDouble val = 0;

    HOffsetAttrib():Attr("HOffset", Tango::DEV_DOUBLE, Tango::READ_WRITE) {};
    ~HOffsetAttrib() {};

    virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
    {
      val = (static_cast<LiberaDeviceServer *>(dev))->GetHOffset();
      att.set_value(&val);
    }

    virtual void write(Tango::DeviceImpl *dev,Tango::WAttribute &att)
    {
      Tango::DevDouble setValue;
      att.get_write_value(setValue);
      (static_cast<LiberaDeviceServer *>(dev))->SetHOffset(setValue);
    }

    virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
    {return true;}
};

// VOffset
class VOffsetAttrib: public Tango::Attr {

public:

    Tango::DevDouble val = 0;

    VOffsetAttrib():Attr("VOffset", Tango::DEV_DOUBLE, Tango::READ_WRITE) {};
    ~VOffsetAttrib() {};

    virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
    {
      val = (static_cast<LiberaDeviceServer *>(dev))->GetVOffset();
      att.set_value(&val);
    }

    virtual void write(Tango::DeviceImpl *dev,Tango::WAttribute &att)
    {
      Tango::DevDouble setValue;
      att.get_write_value(setValue);
      (static_cast<LiberaDeviceServer *>(dev))->SetVOffset(setValue);
    }

    virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
    {return true;}
};

// HOffsetFine
class HOffsetFineAttrib: public Tango::Attr {

public:

    Tango::DevDouble val = 0;

    HOffsetFineAttrib():Attr("HOffsetFine", Tango::DEV_DOUBLE, Tango::READ_WRITE) {};
    ~HOffsetFineAttrib() {};

    virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
    {
      val = (static_cast<LiberaDeviceServer *>(dev))->GetHOffsetFine();
      att.set_value(&val);
    }

    virtual void write(Tango::DeviceImpl *dev,Tango::WAttribute &att)
    {
      Tango::DevDouble setValue;
      att.get_write_value(setValue);
      (static_cast<LiberaDeviceServer *>(dev))->SetHOffsetFine(setValue);
    }

    virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
    {return true;}
};

// VOffset
class VOffsetFineAttrib: public Tango::Attr {

public:

    Tango::DevDouble val = 0;

    VOffsetFineAttrib():Attr("VOffsetFine", Tango::DEV_DOUBLE, Tango::READ_WRITE) {};
    ~VOffsetFineAttrib() {};

    virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
    {
      val = (static_cast<LiberaDeviceServer *>(dev))->GetVOffsetFine();
      att.set_value(&val);
    }

    virtual void write(Tango::DeviceImpl *dev,Tango::WAttribute &att)
    {
      Tango::DevDouble setValue;
      att.get_write_value(setValue);
      (static_cast<LiberaDeviceServer *>(dev))->SetVOffsetFine(setValue);
    }

    virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
    {return true;}
};

// Tilt_Angle
class Tilt_AngleAttrib: public Tango::Attr {

public:

    Tango::DevDouble val = 0;

    Tilt_AngleAttrib():Attr("Tilt_Angle", Tango::DEV_DOUBLE, Tango::READ_WRITE) {};
    ~Tilt_AngleAttrib() {};

    virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
    {
      val = (static_cast<LiberaDeviceServer *>(dev))->GetTiltAngle();
      att.set_value(&val);
    }

    virtual void write(Tango::DeviceImpl *dev,Tango::WAttribute &att)
    {
      Tango::DevDouble setValue;
      att.get_write_value(setValue);
      (static_cast<LiberaDeviceServer *>(dev))->SetTiltAngle(setValue);
    }

    virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
    {return true;}
};

//	Command Set_Incoherency_Ref
class SetIncoherencyRefCommand : public Tango::Command
{
public:
    SetIncoherencyRefCommand(const char   *name,
               Tango::CmdArgType in,
               Tango::CmdArgType out,
               const char        *in_desc,
               const char        *out_desc,
               Tango::DispLevel  level)
            :Command(name,in,out,in_desc,out_desc, level)	{};

    SetIncoherencyRefCommand(const char   *name,
               Tango::CmdArgType in,
               Tango::CmdArgType out)
            :Command(name,in,out)	{};

    ~SetIncoherencyRefCommand() {};

    virtual CORBA::Any *execute (Tango::DeviceImpl *dev, TANGO_UNUSED(const CORBA::Any &in_any)) {
      (static_cast<LiberaDeviceServer *>(dev))->SetIncoherencyRefFromReading();
      return new CORBA::Any();
    }
    virtual bool is_allowed (Tango::DeviceImpl *dev, TANGO_UNUSED(const CORBA::Any &in_any)) {
      return true;
    }
};
