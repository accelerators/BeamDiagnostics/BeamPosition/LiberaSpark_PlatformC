/*
 * Copyright (c) 2014 Instrumentation Technologies
 * All Rights Reserved.
 *
 * $Id: libera_device_server.h 21001 2014-09-03 12:53:46Z tomaz.beltram $
 */

#pragma once

#include <mci/mci.h>
#include <mci/node.h>
#include <mci/callback.h>
#include <mci/notification_client.h>
#include <mci/notification_data.h>
#include <isig/declarations.h>

#include "libera_config.h"
#include "libera_signal_client.h"

#include <tango.h>

class LiberaDeviceServer : public Tango::Device_5Impl
{
public:

    LiberaDeviceServer(Tango::DeviceClass *cl, const char *s,LiberaConfig& a_cfg);
    mci::Node& GetRoot(const bool a_platform);
    const std::string& GetInstanceName();
    LiberaSignalClientBase* GetSignalClient(const std::string& a_node);
    void AddNotification(const LiberaMapping& a_lm);
    void CallbackNotification(const mci::NotificationData& a_data);
    void SetCalculationAlgorithm(Tango::DevShort algo);
    void SetAGCEnable(bool enable);
    void SetHOffset(Tango::DevDouble offset);
    void SetHOffsetFine(Tango::DevDouble offset);
    void SetVOffset(Tango::DevDouble offset);
    void SetVOffsetFine(Tango::DevDouble offset);
    Tango::DevDouble GetHOffset();
    Tango::DevDouble GetHOffsetFine();
    Tango::DevDouble GetVOffset();
    Tango::DevDouble GetVOffsetFine();
    void UpdateHOffset();
    void UpdateVOffset();
    void SetTiltAngle(Tango::DevDouble angle);
    Tango::DevDouble GetTiltAngle();
    void SetIncoherencyRef(Tango::DevDouble ref);
    void SetIncoherencyRefFromReading();
    Tango::DevDouble GetIncoherencyRef();
    Tango::DevDouble GetIncoherencyDrift();

private:
    ~LiberaDeviceServer();

    template <mci::NodeValType_e NodeValType>
    static void PushChangeEventFunc(LiberaDeviceServer* a_dev,
        const std::string& a_name, const istd::Any& a_val, const bool a_isArray);

    void delete_device();
    virtual void init_device();
    virtual void always_executed_hook();
    virtual Tango::DevState dev_state();

    bool Connect();
    void Connect(mci::Node &a_root, mci::Root a_type);

    void Disconnect();
    void Disconnect(mci::Node &a_root, mci::Root a_type);

    void RegisterNotifications();
    void CreateSignals();

    template<isig::AtomType_e SignalAtomType>
    static LiberaSignalClientBase* SignalClientConstructor(Tango::DeviceImpl* a_dev, const LiberaMapping& a_lm);

    // Type converter map
    typedef LiberaSignalClientBase* (*TypeConstructorFunc)(Tango::DeviceImpl*, const LiberaMapping& a_lm);
    typedef std::map<isig::AtomType_e, TypeConstructorFunc> TypeConstructorMap;

    /**
     * Helper class for pair construction.
     */
    template <isig::AtomType_e SignalAtomType>
    class SignalAtomTypePair : public std::pair<isig::AtomType_e, TypeConstructorFunc>
    {
    public:
        SignalAtomTypePair() : std::pair<isig::AtomType_e, TypeConstructorFunc>(
                SignalAtomType, &SignalClientConstructor<SignalAtomType>) {}
    };

    typedef void (*TypeEventFunc)(LiberaDeviceServer*,
        const std::string&, const istd::Any&, const bool);
    typedef std::map<mci::NodeValType_e, TypeEventFunc> NodeValTypeEventMap;

    /**
     * Helper class for pair construction.
     */
    template <mci::NodeValType_e NodeValType>
    class NodeValTypePair : public std::pair<mci::NodeValType_e, TypeEventFunc>
    {
    public:
        NodeValTypePair() : std::pair<mci::NodeValType_e, TypeEventFunc>(
            NodeValType, &PushChangeEventFunc<NodeValType>) {}
    };

    NodeValTypeEventMap m_typeEventMap;

    bool      m_connected;

    bool      agcEnable;
    Tango::DevLong agcMaxADC;
    Tango::DevLong agcMinADC;

    Tango::DevDouble incoherencyRef;
    Tango::DevDouble hOffsetFine;
    Tango::DevDouble hOffset;
    Tango::DevDouble vOffsetFine;
    Tango::DevDouble vOffset;
    Tango::DevDouble tiltAngle;

    mci::Node m_root;
    mci::Node m_platform;
    std::string m_instance;
    Tango::DevULong m_refreshPeriod;
    mci::Callback           m_callback;
    mci::NotificationClient m_client;
    std::vector<LiberaMapping> m_listen;
    std::map<std::string, std::string> m_notificationMap;
    std::map<std::string, LiberaSignalClientBase*> m_signalMap;
    vector<std::string> bpmNodes;
    Tango::DeviceProxy *self;

};

template <>
void LiberaDeviceServer::PushChangeEventFunc<mci::eNvString>(
    LiberaDeviceServer* a_dev, const std::string& a_name,
    const istd::Any& a_val, const bool a_isArray);
