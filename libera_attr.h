/*
 * Copyright (c) 2014 Instrumentation Technologies
 * All Rights Reserved.
 *
 * $Id: libera_device_class.cpp 20559 2014-06-02 07:57:53Z tomaz.beltram $
 */

#pragma once

#include <mci/mci.h>

#include "libera_mapping.h"

template<mci::NodeValType_e NodeValType>
class LiberaAttr : public Tango::Attr
{
public:
    LiberaAttr(const LiberaMapping& a_mapping);
    virtual void writeDefaultValue(Tango::DeviceImpl *a_dev);

private:
    typedef typename NodeValTraits<NodeValType>::LType LiberaType;
    typedef typename NodeValTraits<NodeValType>::TType TangoType;

    /**
     * Inherited methods implemented for Libera node access.
     */
    virtual void read(Tango::DeviceImpl *a_dev, Tango::Attribute &a_attr);
    virtual void write(Tango::DeviceImpl *a_dev, Tango::WAttribute &a_attr);
    virtual bool is_allowed(Tango::DeviceImpl *a_dev, Tango::AttReqType a_type);

    /**
     * Default conversion methods.
     */
    void FromLiberaType()
    {
        m_tval = m_lval;
    }

    void ToLiberaType()
    {
        m_lval = m_tval;
    }

    LiberaMapping     m_mapping;
    LiberaType        m_lval;
    TangoType         m_tval;
};

// Disable redundant declaration warning for template specialization
// caused by gcc bug fixed only in 4.7.0 and above.
#pragma GCC diagnostic ignored "-Wredundant-decls"

/**
 * Specialized template class function for std::string to char* conversion.
 */
template<>
void LiberaAttr<mci::eNvString>::FromLiberaType();

#pragma GCC diagnostic warning "-Wredundant-decls"
