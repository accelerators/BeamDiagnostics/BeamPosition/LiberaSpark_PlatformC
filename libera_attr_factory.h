/*
 * Copyright (c) 2014 Instrumentation Technologies
 * All Rights Reserved.
 *
 * $Id: libera_attr_factory.h 20836 2014-07-25 13:31:53Z tomaz.beltram $
 */

#pragma once

#include <map>

#include "libera_mapping.h"

class LiberaAttrFactory {
public:
    static Tango::Attr* Create(const LiberaMapping& a_mapping);

private:
    typedef Tango::Attr* (*CreatorTypeFunc)(const LiberaMapping&);
    typedef std::map<mci::NodeValType_e, CreatorTypeFunc> NodeValTypeMap;
    typedef std::map<isig::AtomType_e, CreatorTypeFunc> SignalAtomTypeMap;

    template<mci::NodeValType_e NodeValType>
    static Tango::Attr* Creator(const LiberaMapping& a_mapping);

    template<isig::AtomType_e SignalAtomType>
    static Tango::Attr* Creator(const LiberaMapping& a_mapping);

    /**
     * Helper class for pair construction.
     */
    template <mci::NodeValType_e NodeValType>
    class NodeValTypePair : public std::pair<mci::NodeValType_e, CreatorTypeFunc>
    {
    public:
        NodeValTypePair() : std::pair<mci::NodeValType_e, CreatorTypeFunc>(
            NodeValType, &Creator<NodeValType>) {}
    };

    struct NodeValTypeMapping : NodeValTypeMap {
        NodeValTypeMapping();
    };

    /**
     * Helper class for pair construction.
     */
    template <isig::AtomType_e SignalAtomType>
    class SignalAtomTypePair : public std::pair<isig::AtomType_e, CreatorTypeFunc>
    {
    public:
        SignalAtomTypePair() : std::pair<isig::AtomType_e, CreatorTypeFunc>(
                SignalAtomType, &Creator<SignalAtomType>) {}
    };

    struct SignalAtomTypeMapping : SignalAtomTypeMap {
        SignalAtomTypeMapping();
    };
};
