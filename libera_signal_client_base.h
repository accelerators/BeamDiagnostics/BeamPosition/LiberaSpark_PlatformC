/*
 * Copyright (c) 2014 Instrumentation Technologies
 * All Rights Reserved.
 *
 * $Id: libera_signal_client_base.h 21345 2015-01-07 10:01:47Z tomaz.beltram $
 */

#pragma once

#include <thread>

#include <mci/mci.h>

#include <tango.h>

#include "libera_mapping.h"

#define DOS_ALGORITHM  0   // Standard position calculation algorithm (Delta Over Sigma)
#define POLY_ALGORITHM 1   // Polynomial based position calculation algorithm
#define MAX_ORDER      15  // Maximum polynomial order

typedef struct {
    int  column;
    bool hasEvent;
    bool isSum;
    bool isSumRow;
    bool hasAverage;
    bool mapToDouble;
} _ATTR_T;

class LiberaSignalClientBase
{
public:
    LiberaSignalClientBase(const LiberaMapping& a_lm);
    virtual ~LiberaSignalClientBase();
    void AddAttr(const std::string& a_name,bool hasEvent,bool isSum,bool isUmRow,bool hasStartStopIndex,bool hasAverage,bool mapToDouble,bool removeOffset);
    const LiberaMapping& LM();

    virtual void Create(mci::Node &a_node,uint32_t refresh_rate) = 0;
    virtual int32_t GetAccessType() = 0;

    // Control signals
    virtual void SetOffset(int32_t a_offset) = 0;
    virtual int32_t GetOffset() = 0;
    virtual void SetLength(size_t a_length) = 0;
    virtual size_t GetLength() = 0;
    void Enable(bool a_enable);
    virtual bool IsEnabled() = 0;
    virtual void SetMode(int32_t a_mode) = 0;
    virtual int32_t GetMode() = 0;
    virtual uint64_t GetMT() = 0;
    virtual bool IsAveraging() = 0;
    virtual bool SetAveraging(bool averaging) = 0;
    virtual size_t GetAveragingLength() = 0;
    virtual void SetAveragingLength(size_t length) = 0;

    void Read(const std::string& a_name, Tango::Attribute &a_attr);
    bool ComputeSum();
    bool ComputeSumRow();
    bool RemoveOffset();
    bool hasStartStopIndex();
    void SetStartIndex(int startIdx);
    void SetStopIndex(int stopIdx);
    int GetStartIndex();
    int GetStopIndex();
    void SetCalculationAlgorithm(int algo);
    bool GetCalculationAlgorithm();
    bool SetBPMPolynomial(vector<double> &hCoefs,vector<double> &vCoefs,double kA,double kB,double kC,double kD);
    void SetHOffset(double offset);
    void SetVOffset(double offset);
    void SetTiltAngle(double angle);
    void SetBPM();
    bool IsBPM();

protected:
    const std::map<std::string, _ATTR_T>& GetNames();
    int NumColumns();

    bool isBPM;

    // Polynomial stuff
    vector<double> hPolyCoefs;
    vector<double> vPolyCoefs;
    double kA;
    double kB;
    double kC;
    double kD;
    unsigned int hOrder;
    unsigned int vOrder;
    double px[MAX_ORDER];
    double py[MAX_ORDER];
    double hOffset;
    double vOffset;

    // Rotation matrix
    double r11,r12,r21,r22;


private:
    virtual void Initialize() = 0;
    virtual void Terminate() = 0;
    virtual void Update() = 0;
    virtual void DoRead(int a_column, Tango::Attribute &a_attr) = 0;

    std::map<std::string, _ATTR_T> m_names;
    int m_columns;
    LiberaMapping m_lm;
    bool m_computeSum;
    bool m_computeSumRow;
    bool m_removeOffset;
    bool m_hasStartStopIndex;
    int  m_startIdx;
    int  m_stopIdx;
    int  m_calculationAlgorithm;

};
