/*
 * Copyright (c) 2014 Instrumentation Technologies
 * All Rights Reserved.
 *
 * $Id: libera_device_class.cpp 20888 2014-08-05 08:47:58Z tomaz.beltram $
 */

#include <string>

#include <istd/log.h>

#include "libera_device_class.h"

#include "libera_attr_factory.h"
#include "libera_device_server.h"
#include "libera_control.h"
#include "libera_attr.h"

LiberaDeviceClass* LiberaDeviceClass::m_instance = NULL;

LiberaDeviceClass* LiberaDeviceClass::Instance()
{
    istd_FTRC();
    static std::string liberaDeviceName("LiberaDeviceClass");
    if (!m_instance) {
        m_instance = new LiberaDeviceClass(liberaDeviceName);
    }
    return m_instance;
}

void LiberaDeviceClass::command_factory()
{

  if( m_cfg.hasSAIncoherencyDrift ) {

    SetIncoherencyRefCommand *setIncoherencyRefCommand =
            new SetIncoherencyRefCommand(
                    "SetIncoherencyRef",
                    Tango::DEV_VOID, Tango::DEV_VOID,
                    "",
                    "",
                    Tango::OPERATOR);
    command_list.push_back(setIncoherencyRefCommand);

  }

  istd_FTRC();
}

void LiberaDeviceClass::attribute_factory(vector<Tango::Attr *> &attributes) {
  istd_FTRC();

  for (auto i = m_cfg.begin(); i != m_cfg.end(); ++i) {

    const LiberaMapping &lm(*i);

    if (!(lm.IsReadable() || lm.IsWritable() || lm.IsControl())) {

      istd_LOGi("Skipped unaccessible attribute for: " << lm.IregName());

    } else {

      Tango::Attr *p = LiberaAttrFactory::Create(lm);
      if (p != NULL) {
        if (lm.HasEvent()) {
          if (lm.IsListen() || lm.IsSignal()) {
            p->set_change_event(true, false);
          }
        }
        attributes.push_back(p);
        if (lm.HasStartStopIndex()) {

          // Create start and stop index attribute
          string startName;
          string stopName;
          lm.GetStartStopIndex(startName, stopName);

          StartIndexAttrib *startAtt = new StartIndexAttrib(startName, lm.IregName());
          Tango::UserDefaultAttrProp start_index_prop;
          start_index_prop.format = "%6d";
          start_index_prop.unit = "";
          start_index_prop.min_value = "0";
          startAtt->set_default_properties(start_index_prop);
          startAtt->set_disp_level(Tango::EXPERT);
          startAtt->set_memorized();
          startAtt->set_memorized_init(true);
          attributes.push_back(startAtt);

          StopIndexAttrib *stopAtt = new StopIndexAttrib(stopName, lm.IregName());
          Tango::UserDefaultAttrProp stop_index_prop;
          stop_index_prop.format = "%6d";
          stop_index_prop.unit = "";
          stop_index_prop.min_value = "0";
          stopAtt->set_default_properties(stop_index_prop);
          stopAtt->set_disp_level(Tango::EXPERT);
          stopAtt->set_memorized();
          stopAtt->set_memorized_init(true);
          attributes.push_back(stopAtt);

        }

      } else {

        istd_LOGi("Skipped attribute for: " << lm.IregName());

      }

    }

  }

  if( m_cfg.hasZyncTemp ) {

    // Add Zync temperature attribute
    ZyncTempAttrib *zyncTemp = new ZyncTempAttrib();
    Tango::UserDefaultAttrProp zyncTemp_prop;
    zyncTemp_prop.format = "%4.1f";
    zyncTemp_prop.unit = "degC";
    zyncTemp->set_default_properties(zyncTemp_prop);
    zyncTemp->set_disp_level(Tango::EXPERT);
    attributes.push_back(zyncTemp);

  }

  if( m_cfg.HasBpm() ) {

    // Add Calculation algorithm attribute
    CalcAlgorithmAttrib *calcAlgorithm = new CalcAlgorithmAttrib();
    Tango::UserDefaultAttrProp calcAlgorithm_prop;
    vector<string> labels;
    labels.push_back("DoS");
    labels.push_back("Polynomial");
    calcAlgorithm_prop.set_enum_labels(labels);
    calcAlgorithm->set_default_properties(calcAlgorithm_prop);
    calcAlgorithm->set_disp_level(Tango::EXPERT);
    calcAlgorithm->set_memorized();
    calcAlgorithm->set_memorized_init(true);
    attributes.push_back(calcAlgorithm);

    // Add HOffset attribute
    HOffsetAttrib *hOffset = new HOffsetAttrib();
    hOffset->set_memorized();
    hOffset->set_memorized_init(true);
    attributes.push_back(hOffset);

    // Add HOffsetFine attribute
    HOffsetFineAttrib *hOffsetFine = new HOffsetFineAttrib();
    hOffsetFine->set_memorized();
    hOffsetFine->set_memorized_init(true);
    attributes.push_back(hOffsetFine);

    // Add VOffset attribute
    VOffsetAttrib *vOffset = new VOffsetAttrib();
    vOffset->set_memorized();
    vOffset->set_memorized_init(true);
    attributes.push_back(vOffset);

    // Add HOffsetFine attribute
    VOffsetFineAttrib *vOffsetFine = new VOffsetFineAttrib();
    vOffsetFine->set_memorized();
    vOffsetFine->set_memorized_init(true);
    attributes.push_back(vOffsetFine);

    // Add Tilt_Angle attribute
    Tilt_AngleAttrib *tiltAngle = new Tilt_AngleAttrib();
    tiltAngle->set_memorized();
    tiltAngle->set_memorized_init(true);
    attributes.push_back(tiltAngle);

  }

  if( m_cfg.hasAGC ) {

    // AGC enable
    AGCEnableAttrib *agcEnable = new AGCEnableAttrib();
    agcEnable->set_disp_level(Tango::EXPERT);
    agcEnable->set_memorized();
    agcEnable->set_memorized_init(true);
    attributes.push_back(agcEnable);

  }


  // SA Incohenrency stuff
  if( m_cfg.hasSAIncoherencyDrift ) {

    // Add SAIncoherencyRef  attribute
    SAIncoherencyRefAttrib *saIncoherencyRef = new SAIncoherencyRefAttrib();
    saIncoherencyRef->set_disp_level(Tango::EXPERT);
    saIncoherencyRef->set_memorized();
    saIncoherencyRef->set_memorized_init(true);
    attributes.push_back(saIncoherencyRef);

    // Add SAIncoherencyRef  attribute
    SAIncoherencyDriftAttrib *saIncoherencyDrift = new SAIncoherencyDriftAttrib();
    attributes.push_back(saIncoherencyDrift);

  }



}

LiberaDeviceClass::LiberaDeviceClass(std::string& name) : DeviceClass(name)
{
    istd_FTRC();
    m_cfg.Init();


}

LiberaDeviceClass::~LiberaDeviceClass()
{
    istd_FTRC();
    m_instance = NULL;
}

void LiberaDeviceClass::device_factory(const Tango::DevVarStringArray *devices)
{
    istd_FTRC();
    if(!devices)
    {
        istd_TRC(istd::eTrcLow, "LiberaDeviceClass::device_factory devices is NULL");
        return;
    }
    
    // this is important to set because otherwise it 
    auto* tu = Tango::Util::instance();
    if(tu->_UseDb)
    {
        auto *db = tu->get_database();
        db->set_access_control(Tango::ACCESS_WRITE);
    }

    vector<Tango::Attr *> &attList = get_class_attr()->get_attr_list();
    
    for (size_t i(0); i < devices->length(); ++i) {
        LiberaDeviceServer *dev = new LiberaDeviceServer(
            this, (*devices)[i], m_cfg);
        istd_TRC(istd::eTrcLow, "LiberaDeviceClass::device_factory made " << dev->get_name());
        device_list.push_back(dev);

        // Initialise default value here
        // Memorized attribute are done after device_factory
        for(size_t i=0;i<attList.size();i++) {

          try {

            LiberaControlAttr<eCEnable> *att1 = dynamic_cast<LiberaControlAttr<eCEnable> *>(attList[i]);
            if (att1 != NULL) att1->writeDefaultValue(dev);

            LiberaControlAttr<eCMode> *att2 = dynamic_cast<LiberaControlAttr<eCMode> *>(attList[i]);
            if (att2 != NULL) att2->writeDefaultValue(dev);

            LiberaControlAttr<eCOffset> *att3 = dynamic_cast<LiberaControlAttr<eCOffset> *>(attList[i]);
            if (att3 != NULL) att3->writeDefaultValue(dev);

            LiberaControlAttr<eCLength> *att4 = dynamic_cast<LiberaControlAttr<eCLength> *>(attList[i]);
            if (att4 != NULL) att4->writeDefaultValue(dev);

            LiberaControlAttr<eCMT> *att5 = dynamic_cast<LiberaControlAttr<eCMT> *>(attList[i]);
            if (att5 != NULL) att5->writeDefaultValue(dev);

            LiberaAttr<mci::eNvBool> *att6 = dynamic_cast<LiberaAttr<mci::eNvBool> *>(attList[i]);
            if (att6 != NULL) att6->writeDefaultValue(dev);

            LiberaAttr<mci::eNvLong> *att7 = dynamic_cast<LiberaAttr<mci::eNvLong> *>(attList[i]);
            if (att7 != NULL) att7->writeDefaultValue(dev);

            LiberaAttr<mci::eNvULong> *att8 = dynamic_cast<LiberaAttr<mci::eNvULong> *>(attList[i]);
            if (att8 != NULL) att8->writeDefaultValue(dev);

            LiberaAttr<mci::eNvLongLong> *att9 = dynamic_cast<LiberaAttr<mci::eNvLongLong> *>(attList[i]);
            if (att9 != NULL) att9->writeDefaultValue(dev);

            LiberaAttr<mci::eNvULongLong> *att10 = dynamic_cast<LiberaAttr<mci::eNvULongLong> *>(attList[i]);
            if (att10 != NULL) att10->writeDefaultValue(dev);

            LiberaAttr<mci::eNvDouble> *att11 = dynamic_cast<LiberaAttr<mci::eNvDouble> *>(attList[i]);
            if (att11 != NULL) att11->writeDefaultValue(dev);

            LiberaAttr<mci::eNvFloat> *att12 = dynamic_cast<LiberaAttr<mci::eNvFloat> *>(attList[i]);
            if (att12 != NULL) att12->writeDefaultValue(dev);

          } catch( Tango::DevFailed &e ) {

            cerr << "Warning: Failed to set default value for " << attList[i]->get_name() << endl;
            cerr << e.errors[0].desc << endl;

          }

        }

        if ((Tango::Util::_UseDb == true) && (Tango::Util::_FileDb == false))
        {
            istd_TRC(istd::eTrcLow, "export_device(1) in");
            export_device(dev);
            istd_TRC(istd::eTrcLow, "export_device(1) out");
        }
        else
        {
            istd_TRC(istd::eTrcLow, "export_device(2) in");
            export_device(dev, dev->get_name().c_str());
            istd_TRC(istd::eTrcLow, "export_device(2) out");
        }
    }
}

